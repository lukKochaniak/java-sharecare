export class User {
  id: number;
}

export enum Role {
  CAREGIVER,
  ELDERLY
}

export class LoginPayload {
  email: string;
  password: string;
}

export class SignUpForm {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  dateOfBirth: Date;
  address: string;
  city: string;
  zipcode: string;
  phoneNumber: string;
  role: Role;
}

export class UserSnapshot {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  age: number;
  address: string;
  city: string;
  zipcode: string;
  dateOfBirth: Date;
  lastLocation: string;
  role: Role;
  locale: string;
}

export class SignInForm {
  email: string;
  password: string;
}

export class ApiResponse {
  message: string;
  result: UserSnapshot;
  status: number;
}

export class GetProfileDataRequest {
  message: string;
  result: ProfileData;
  status: number;
}

export class ProfileData {
  email: string;
  firstName: string;
  lastName: string;
  address: string;
  city: string;
  zipcode: string;
  dateOfBirth: Date;
}

export enum NotificationType {
  ACCOUNT_ASSIGNMENT_OFFER,
  ACCOUNT_ASSIGNMENT_ACCEPTED,
  ACCOUNT_ASSIGNMENT_DELETED,
  SERVICE_REQUESTED,
  SERVICE_REQUEST_ACCEPTED,
  SERVICE_REQUESTED_DECLINED,
  SERVICE_COMPLETED
}

export class OfferData {
  phoneNumber: string;
  fromEmail: string;
}

export class ElderlyAssignment {
  caregiverId: number;
  elderlyId: number;
}

export class NotificationSnapshot {
  id: string;
  fromUserId: number;
  fromUser: string;
  toUserId: number;
  title: string;
  message: string;
  read: boolean;
  dateOfSending: Date;
  type: NotificationType;
}

export class Notifications {
  notifications: NotificationSnapshot[];
}

export class AddContractRequest {
  toId: number;
  amount: number;
  fromId: number;
}
