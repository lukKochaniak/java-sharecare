export enum Title {
  OptionA,
  OptionB,
  OptionC,
  OptionD,
  OptionE
}

export enum Status {
  //Open is when an elderly requests the service.
  OPEN,
  //Caregivers requests to do the task.
  REQUESTED,
  //Elderly accepts caregiver's request.
  ACCEPTED,
  //This will happen when the caregiver checks-in at the elerderlys'
  INPROGRESS,
  //After task is done, and check out has been performed.
  COMPLETED
}

export class CreateServiceForm {
  title: string;
  description: string;
  cost: number;
  duration: number;
  requestedBy: number;
  address: string;
  city: string;
  zipcode: string;

}

export class ServiceSnapshot {
  id: number;
  title: Title;
  description: string;
  cost: number;
  address: string;
  city: string;
  zipcode: string;
  checkindatehour: Date;
  checkoutdatehour: Date;
  duraton: number;
  requestedby: number;
  providedby: number;
  status: Status;
}

export class Service {
  id: number;
  title: Title;
  description: string;
  cost: number;
  address: string;
  city: string;
  zipcode: string;
  checkindatehour: Date;
  checkoutdatehour: Date;
  duration: number;
  requestedby: number;
  providedby: number;
  status: Status;
}

export class RequestServiceRequest {
  serviceId: number;
  userId: number;
}

export class FindOpenServicesRequest {
  userId: number;
}

export class FindPendingServiceRequest {
  userId: number;
}

export class CancelServiceRequest {
  taskId: number;
  requestedby: number;
  providedby: number;
}
