import {RouterModule, Routes} from '@angular/router';
import {NgModule} from "@angular/core";
import {LayoutComponent} from "./layout/layout.component";


const routes: Routes = [
  {
    path: '',
    loadChildren: './frontpage/frontpage.module#FrontpageModule',
    data: {
      title: '',
      description: ''
    }
  },
  {
    path: 'home',
    loadChildren: './layout/layout.module#LayoutModule',
    data: {
      title: '',
      description: ''
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
