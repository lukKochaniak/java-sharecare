import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ApiResponse, Notifications, NotificationSnapshot} from '../../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  baseUrl = 'http://localhost:8080/api/';

  constructor(private http : HttpClient) {}

  getNotifications(id) : Observable<NotificationSnapshot[]> {
    return this.http.get<NotificationSnapshot[]>(this.baseUrl + "notifications/get/" + id);
  }

  markAsRead(id) : Observable<NotificationSnapshot> {
    return this.http.get<NotificationSnapshot>(this.baseUrl + "notifications/read/" + id);
  }

}
