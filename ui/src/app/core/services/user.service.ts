import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GetProfileDataRequest, ApiResponse, SignUpForm, UserSnapshot} from '../../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  baseUrl = 'http://localhost:8080/api/';

  create(signUpForm: SignUpForm): Observable<SignUpForm> {
    return this.http.post<SignUpForm>(this.baseUrl + 'users/add', signUpForm);
  }

  login(loginPayload): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + 'users/login', loginPayload);
  }

  resetPassword(email): Observable<ApiResponse>{
    return this.http.post<ApiResponse>(this.baseUrl + 'users/password/reset', email);
  }

  setNewPassword(newPasswordData): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + "users/password/new/set", newPasswordData);
  }

  updateProfile(profileData) : Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + "users/profile/update", profileData);
  }

  getProfileData(email) : Observable<GetProfileDataRequest> {
    return this.http.post<GetProfileDataRequest>(this.baseUrl + "users/profile/get/data", email);
  }

  getElderly(id) : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + "users/elderly/get/" + id);
  }

  updatePassword(passwordUpdateData) : Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + "users/password/update", passwordUpdateData);
  }
}
