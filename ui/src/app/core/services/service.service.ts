import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {
  CancelServiceRequest,
  CreateServiceForm, FindOpenServicesRequest,
  FindPendingServiceRequest,
  RequestServiceRequest,
  Service,
  ServiceSnapshot
} from '../../model/service.model';
import {ApiResponse} from "../../model/api.response";

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http: HttpClient) {
  }

  baseUrl = 'http://localhost:8080/api/services/';

  //This is for adding a new service. Check ServiceController for backend info.
  createService(createServiceForm: CreateServiceForm): Observable<CreateServiceForm> {
    return this.http.post<CreateServiceForm>(this.baseUrl + 'add', createServiceForm);
  }

  getServices() : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'get');
  }

  getServiceById(id: number): Observable<Service> {
    return this.http.get<Service>(this.baseUrl + id);
  }

  updateService(service: ServiceSnapshot): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(this.baseUrl + 'update', service);
  }

  deleteService(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(this.baseUrl + id);
  }

  getServicesOfUser(id : number) :Observable<Service[]> {
    return this.http.get<Service[]>(this.baseUrl + "servicesOf/" + id);
  }

  getCompletedBy(id: number): Observable<Service[]> {
    return this.http.get<Service[]>(this.baseUrl + 'completedBy/' + id);
  }
  
  getOpenServices(request: FindOpenServicesRequest): Observable<Service[]> {
    return this.http.get<Service[]>(this.baseUrl + "get/open/services/" + request.userId);
  }

  getPendingServices(request : FindPendingServiceRequest): Observable<Service[]> {
    return this.http.get<Service[]>(this.baseUrl + "get/pending/services/" + request.userId);
  }

  requestService(request : RequestServiceRequest): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + "request", request);
  }

  acceptServiceRequest(serviceData) : Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + "accept/request", serviceData);
  }

  declineServiceRequest(serviceData) : Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + "decline/request", serviceData);
  }

  cancelService(taskId) : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + "cancel/" + taskId);
  }

  finishService(serviceData) : Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + "finish", serviceData);
  }

}
