import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ApiResponse} from "../../model/api.response";

@Injectable({
  providedIn: 'root'
})
export class BlockchainService {

  constructor(private http: HttpClient) { }

  baseUrl = 'http://localhost:8080/api/transactions/';

  getBalance(id: number) : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + id);
  }

  pay(paymentData) : Observable<Boolean> {
    return this.http.post<Boolean>(this.baseUrl + "pay", paymentData);
  }

}
