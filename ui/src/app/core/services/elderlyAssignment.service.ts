import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ApiResponse} from "../../model/api.response";

@Injectable({
  providedIn: 'root'
})
export class ElderlyAssignmentService {

  baseUrl = 'http://localhost:8080/api/assignelderly';

  constructor(private http : HttpClient) {}

  acceptElderlyAssignment(elderlyAssignment): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + "/accept", elderlyAssignment);
  }

  declineElderlyAssignment(elderlyAssignment): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + "/decline", elderlyAssignment);
  }

  deleteElderlyAssignment(elderlyAssignment) : Observable<ApiResponse> {
    console.log(elderlyAssignment);
    return this.http.post<ApiResponse>(this.baseUrl + "/delete", elderlyAssignment);
  }

  sendAssignElderlyOffer(offerData): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + "/offer", offerData);
  }

  getAssignedUsers(id): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + "/get/" + id);
  }

}
