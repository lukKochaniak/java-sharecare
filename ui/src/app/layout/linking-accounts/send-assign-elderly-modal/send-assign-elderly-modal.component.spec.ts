import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendAssignElderlyModalComponent } from './send-assign-elderly-modal.component';

describe('SendAssignElderlyModalComponent', () => {
  let component: SendAssignElderlyModalComponent;
  let fixture: ComponentFixture<SendAssignElderlyModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendAssignElderlyModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendAssignElderlyModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
