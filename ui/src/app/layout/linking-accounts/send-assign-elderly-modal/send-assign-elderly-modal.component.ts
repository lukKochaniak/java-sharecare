import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NgbActiveModal, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {validation_messages} from "../../../frontpage/common/validation_messages";
import {SuccessModalComponent} from "../../../frontpage/success-modal/success-modal.component";
import {FailureModalComponent} from "../../../frontpage/failure-modal/failure-modal.component";
import {OfferData} from "../../../model/user.model";
import {ElderlyAssignmentService} from "../../../core/services/elderlyAssignment.service";

@Component({
  selector: 'app-send-assign-elderly-modal',
  templateUrl: './send-assign-elderly-modal.component.html',
  styleUrls: ['./send-assign-elderly-modal.component.css']
})
export class SendAssignElderlyModalComponent implements OnInit {
  form : FormGroup;
  validation_messages = validation_messages;
  userRole: string;

  constructor(private fb: FormBuilder,
              private activeModal: NgbActiveModal,
              private elderlyAssignmentService: ElderlyAssignmentService,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.getUserRole();
    this.buildForm();
  }

  getUserRole() {
    this.userRole = localStorage.getItem('role');
  }

  private buildForm(): void {
    this.form = this.fb.group({
      phoneNumber: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])]
    });
  }

  checkIfPhoneNumberCorrect(checkItem: string): boolean {
    let hasError = false;
    let array;

    if(checkItem === 'phoneNumber') {
      array = this.validation_messages.phoneNumber;
    }

    for (let validation of array) {
      if (this.form.get(checkItem).hasError(validation.type)) {
        hasError = true;
      }
    }
    return hasError && this.form.get(checkItem).touched
  }

  sendAssignElderlyAccountOffer() {
    let offerData : OfferData;
    offerData = this.createOfferData();

    this.elderlyAssignmentService.sendAssignElderlyOffer(offerData)
      .subscribe((response) => {
        if(response.status === 200) {
          const modalRef = this.modalService.open(SuccessModalComponent);
          modalRef.componentInstance.messageToShow = "Offer for assigning elderly account was send";
          this.closeModal();
        } else {
          this.closeModal();
          const modal = this.modalService.open(FailureModalComponent);
          modal.componentInstance.errorToShow = response;
        }
      }, error1 => {
        this.closeModal();
        const modal = this.modalService.open(FailureModalComponent);
        modal.componentInstance.errorToShow = "";
      });
  }

  createOfferData(): OfferData {
    return {
      phoneNumber: this.form.value.phoneNumber.trim(),
      fromEmail: localStorage.getItem('userEmail')
    };
  }

  closeModal() {
    this.activeModal.close();
  }

  sendAssignCaregiverAccountOffer() {
    let offerData : OfferData;
    offerData = this.createOfferData();

    this.elderlyAssignmentService.sendAssignElderlyOffer(offerData)
      .subscribe((response) => {
        if(response.status === 200) {
          const modalRef = this.modalService.open(SuccessModalComponent);
          modalRef.componentInstance.messageToShow = "Offer for assigning caregiver account was send";
          this.closeModal();
        } else {
          this.closeModal();
          const modal = this.modalService.open(FailureModalComponent);
          modal.componentInstance.errorToShow = response;
        }
      }, error1 => {
        this.closeModal();
        const modal = this.modalService.open(FailureModalComponent);
        modal.componentInstance.errorToShow = "";
      });

  }
}
