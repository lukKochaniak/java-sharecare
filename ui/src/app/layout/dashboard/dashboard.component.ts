import {Component, OnInit} from '@angular/core';
import {BlockchainService} from "../../core/services/blockchain.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  public alerts: Array<any> = [];
  public sliders: Array<any> = [];
  tokensAmount: number;

  constructor(private blockchainService: BlockchainService) {
  }

  ngOnInit() {
    this.getBalance();
  }

  getBalance() {
    this.blockchainService.getBalance(parseInt(localStorage.getItem('id'))).subscribe(value => {
      this.tokensAmount = value.result;
    });
  }

  public closeAlert(alert: any) {
    const index: number = this.alerts.indexOf(alert);
    this.alerts.splice(index, 1);
  }
}
