import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElderlyCardComponent } from './elderly-card.component';

describe('ElderlyCardComponent', () => {
  let component: ElderlyCardComponent;
  let fixture: ComponentFixture<ElderlyCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElderlyCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElderlyCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
