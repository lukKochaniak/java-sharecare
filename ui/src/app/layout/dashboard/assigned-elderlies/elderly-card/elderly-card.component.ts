import {Component, Input, OnInit} from '@angular/core';
import {UserSnapshot} from "../../../../model/user.model";
import {Router} from "@angular/router";

@Component({
  selector: 'app-elderly-card',
  templateUrl: './elderly-card.component.html',
  styleUrls: ['./elderly-card.component.css']
})
export class ElderlyCardComponent implements OnInit {
  @Input() elderly: UserSnapshot;
  @Input() bgClass: string;
  @Input() icon: string;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  onGoToDetailsClick() {
    let link = '/home/elderly/' + this.elderly.id;
    this.router.navigate([link]);
  }
}
