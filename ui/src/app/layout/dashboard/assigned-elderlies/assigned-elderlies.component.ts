import {Component, OnInit} from '@angular/core';
import {ElderlyAssignmentService} from "../../../core/services/elderlyAssignment.service";
import {UserSnapshot} from "../../../model/user.model";
import {SendAssignElderlyModalComponent} from "../../linking-accounts/send-assign-elderly-modal/send-assign-elderly-modal.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-assigned-elderlies',
  templateUrl: './assigned-elderlies.component.html',
  styleUrls: ['./assigned-elderlies.component.css']
})
export class AssignedElderliesComponent implements OnInit {

  users: UserSnapshot[];
  userRole: string;

  constructor(private elderlyAssignmentService: ElderlyAssignmentService,
              private modalService: NgbModal) {
  }

  ngOnInit() {
    this.getUserRole();
    this.getElderliesFromServer();
    console.log(this.users);
  }

  getUserRole(){
    this.userRole = localStorage.getItem('role');
  }

  getElderliesFromServer() {
    this.elderlyAssignmentService.getAssignedUsers(parseInt(localStorage.getItem('id')))
      .subscribe(response => {
        this.users = response.result;
        console.log(this.users);
      });
  }

  openSendAssignElderlyOfferModal() {
    const modal = this.modalService.open(SendAssignElderlyModalComponent);
  }

}
