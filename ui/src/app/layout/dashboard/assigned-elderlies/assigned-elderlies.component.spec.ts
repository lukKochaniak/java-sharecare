import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignedElderliesComponent } from './assigned-elderlies.component';

describe('AssignedElderliesComponent', () => {
  let component: AssignedElderliesComponent;
  let fixture: ComponentFixture<AssignedElderliesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignedElderliesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignedElderliesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
