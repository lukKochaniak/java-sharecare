import {Component} from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {WebSocketService} from "../../../../core/services/websocket.service";
import {NotificationSnapshot} from "../../../../model/user.model";
import {NotificationService} from "../../../../core/services/notification.service";

@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.css']
})
export class NotificationListComponent {

  public notifications: NotificationSnapshot[];

  constructor(private modalService: NgbModal,
              private webSocketService: WebSocketService,
              private notificationService: NotificationService) {
    this.getUserNotification();
    this.getNotifications();
  }

  getNotifications(){
    this.notificationService.getNotifications(localStorage.getItem('id')).subscribe(value => {
      this.notifications = value;
    })
  }

  getUserNotification() {
    let stompClient = this.webSocketService.connect();

    stompClient.connect({}, frame => {
      stompClient.subscribe('/notification/websocket/' + localStorage.getItem("id"), notifications => {
        this.getNotifications();
      })
    });
  }

}
