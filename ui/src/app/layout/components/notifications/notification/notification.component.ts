import {Component, Input, OnInit} from '@angular/core';
import {NotificationSnapshot} from "../../../../model/user.model";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {NotificationModalComponent} from "../notification-modal/notification-modal.component";
import {NotificationService} from "../../../../core/services/notification.service";

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  @Input() notificationSnapshot: NotificationSnapshot;
  @Input() inHeader: boolean;
  shortenMessage: string;
  date: string;

  constructor(private modalService: NgbModal,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.controlMessageLength(this.notificationSnapshot.message);
    this.determineDate();
  }

  private onNotificationClick() {
    const modalRef = this.modalService.open(NotificationModalComponent);
    modalRef.componentInstance.notification = this.notificationSnapshot;
    this.markAsRead();
  }

  private markAsRead() {
    this.notificationService.markAsRead(this.notificationSnapshot.id).subscribe();
    this.notificationSnapshot.read = true;
  }

  private controlMessageLength(message: string) {
    if (message.length > 40) {
      this.shortenMessage = message.substring(0, 37) + "...";
    }
  }

  private determineDate() {
    let newDate = new Date(this.notificationSnapshot.dateOfSending);
    this.date = newDate.getDate().toString() + '-' + newDate.getMonth().valueOf() + '-' + newDate.getFullYear().toString();
  }
}
