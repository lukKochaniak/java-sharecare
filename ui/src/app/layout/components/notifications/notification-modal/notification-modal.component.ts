import {Component, OnInit} from '@angular/core';
import {ElderlyAssignment, NotificationSnapshot} from "../../../../model/user.model";
import {NgbActiveModal, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ServiceService} from "../../../../core/services/service.service";
import {SuccessModalComponent} from "../../../../frontpage/success-modal/success-modal.component";
import {FailureModalComponent} from "../../../../frontpage/failure-modal/failure-modal.component";
import {NotificationService} from "../../../../core/services/notification.service";
import {ElderlyAssignmentService} from "../../../../core/services/elderlyAssignment.service";
import {FindTasksComponent} from "../../../tasks/find-tasks/find-tasks.component";

@Component({
  selector: 'app-notification-modal',
  templateUrl: './notification-modal.component.html',
  styleUrls: ['./notification-modal.component.css']
})
export class NotificationModalComponent implements OnInit {

  public notification: NotificationSnapshot;
  public date: Date;
  public elderlyAssignment: ElderlyAssignment;

  public isTypeServiceRequested = false;
  public isTypeAccountAssignmentOffer = false;
  public isTypeServiceRequestAccepted = false;

  constructor(private activeModal: NgbActiveModal,
              private serviceService: ServiceService,
              private notificationService: NotificationService,
              private modalService: NgbModal,
              private elderlyAssignmentService: ElderlyAssignmentService) {
  }

  ngOnInit() {
    this.determineType();
    this.date = new Date(this.notification.dateOfSending);
  }

  determineType() {
    if (this.notification.type.toString() == "ACCOUNT_ASSIGNMENT_OFFER") {
      this.isTypeAccountAssignmentOffer = true;
    }

    if (this.notification.type.toString() == "SERVICE_REQUESTED") {
      this.isTypeServiceRequested = true;
    }

    if (this.notification.type.toString() == "SERVICE_REQUEST_ACCEPTED") {
      this.isTypeServiceRequestAccepted = true;
    }
  }

  acceptServiceRequest() {
    this.serviceService.acceptServiceRequest(this.getRequestData())
      .subscribe((response) => {
        if (response.status === 200) {
          const modalRef = this.modalService.open(SuccessModalComponent);
          modalRef.componentInstance.messageToShow = "You succesfully accepted the request!";

        } else {
          const modal = this.modalService.open(FailureModalComponent);
          modal.componentInstance.messageToShow = response.message;
        }
      }, error1 => {
        const modal = this.modalService.open(FailureModalComponent);
        modal.componentInstance.errorToShow = "Something went wrong";
      });

    this.closeModal();
  }

  declineServiceRequest() {
    this.serviceService.declineServiceRequest(this.getRequestData())
      .subscribe((response) => {
        if (response.status === 200) {
          const modalRef = this.modalService.open(SuccessModalComponent);
          modalRef.componentInstance.messageToShow = "Request declined!";

        } else {
          const modal = this.modalService.open(FailureModalComponent);
          modal.componentInstance.messageToShow = response.message;
        }
      }, error1 => {
        const modal = this.modalService.open(FailureModalComponent);
        modal.componentInstance.errorToShow = "Something went wrong";
      });
  }

  getRequestData() {
    return {
      userId: localStorage.getItem("id"),
      serviceId: this.getServiceId(),
      from: this.notification.fromUserId
    }
  }

  getServiceId(): number {
    let message: string = this.notification.message;

    let indexBeforeId;
    for (let i = message.length - 1; i > 1; i--) {
      if (message.charAt(i - 1) == '=') {
        indexBeforeId = i - 1;
        break;
      }
    }

    return parseInt(message.substr(indexBeforeId + 1));
  }

  closeModal() {
    this.activeModal.close();
  }

  acceptCaregiver() {
    this.elderlyAssignment = {
      caregiverId: this.notification.fromUserId,
      elderlyId: parseInt(localStorage.getItem('id')).valueOf()
    };

    this.elderlyAssignmentService.acceptElderlyAssignment(this.elderlyAssignment)
      .subscribe((response) => {
        if (response.status === 200) {
          const modalRef = this.modalService.open(SuccessModalComponent);
          modalRef.componentInstance.messageToShow = "You were successfully assigned to your caregiver!";
        } else {
          const modal = this.modalService.open(FailureModalComponent);
          modal.componentInstance.messageToShow = response.message;
        }
      }, error1 => {
        const modal = this.modalService.open(FailureModalComponent);
        modal.componentInstance.errorToShow = "Something went wrong";
      });

    this.closeModal();
  }

  declineCaregiver() {
    this.elderlyAssignment = {
      caregiverId: this.notification.fromUserId,
      elderlyId: parseInt(localStorage.getItem('id')).valueOf()
    };

    console.log(this.elderlyAssignment);

    this.elderlyAssignmentService.declineElderlyAssignment(this.elderlyAssignment)
      .subscribe((response) => {
        if (response.status === 200) {
          const modalRef = this.modalService.open(SuccessModalComponent);
          modalRef.componentInstance.messageToShow = "You successfully declined this caregiver!";
        } else {
          const modal = this.modalService.open(FailureModalComponent);
          modal.componentInstance.messageToShow = response.message;
        }
      }, error1 => {
        const modal = this.modalService.open(FailureModalComponent);
        modal.componentInstance.errorToShow = "Something went wrong";
      });

    this.closeModal();

  }
}
