import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {SendAssignElderlyModalComponent} from "../../linking-accounts/send-assign-elderly-modal/send-assign-elderly-modal.component";
import {NotificationSnapshot} from "../../../model/user.model";
import {NotificationService} from "../../../core/services/notification.service";
import {WebSocketService} from "../../../core/services/websocket.service";
import {forEach} from "@angular/router/src/utils/collection";
import {of} from "rxjs";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public pushRightClass: string;
  public firstName: string;
  public lastName: string;
  public id: string;
  public notifications: NotificationSnapshot[];
  public userRole: string;

  constructor(private translate: TranslateService,
              public router: Router,
              private modalService: NgbModal,
              private notificationService: NotificationService,
              private webSocketService: WebSocketService) {

    this.toggleSidbebar();
    this.subscribeForNewUserNotification();
    this.notifications = [];
  }

  ngOnInit() {
    this.pushRightClass = 'push-right';
    this.firstName = localStorage.getItem('firstName');
    this.lastName = localStorage.getItem('lastName');
    this.id = localStorage.getItem('id');
    this.getNotifications();
    this.getUserRole();
  }

  getNotifications(){
    this.notificationService.getNotifications(this.id).subscribe(value => {
      this.notifications = value;
    })
  }

  getUserRole(){
    this.userRole = localStorage.getItem('role');
  }

  subscribeForNewUserNotification() {
    let stompClient = this.webSocketService.connect();

    stompClient.connect({}, frame => {
      stompClient.subscribe('/notification/websocket/' + localStorage.getItem("id"), notifications => {
        this.getNotifications();
        console.log(this.notifications);
      })
    });
  }

  countUnreadNotifications() : number{
    let unreadMessages = 0;

    this.notifications.forEach(value => {
      if(!value.read) unreadMessages++;
    });

    return unreadMessages;
  }

  toggleSidbebar() {
    this.router.events.subscribe(val => {
      if (
        val instanceof NavigationEnd &&
        window.innerWidth <= 992 &&
        this.isToggled()
      ) {
        this.toggleSidebar();
      }
    });
  }

  isToggled(): boolean {
    const dom: Element = document.querySelector('body');
    return dom.classList.contains(this.pushRightClass);
  }

  toggleSidebar() {
    const dom: any = document.querySelector('body');
    dom.classList.toggle(this.pushRightClass);
  }

  openProfilePage() {
    this.router.navigate(['/home/profile']);
  }

  openDashboard() {
    this.router.navigate(['/home/dashboard']);
  }

  openSendAssignElderlyOfferModal() {
    const modal = this.modalService.open(SendAssignElderlyModalComponent);
  }

  openQrgeneratorPage() {
    this.router.navigate(['/home/qrgenerator']);
  }

  onLoggedout() {
    this.router.navigate(['']);
  }

  changeLang(language: string) {
    this.translate.use(language);
  }
}
