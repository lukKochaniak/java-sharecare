import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from "@angular/router";
import {ServiceService} from "../../../core/services/service.service";
import {Service, ServiceSnapshot} from "../../../model/service.model";

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  services: Service[];

  constructor(private router: Router, private serviceService: ServiceService) {
  }

  ngOnInit() {
    this.retrieveServices();
  }

  retrieveServices(){
    this.serviceService.getServicesOfUser(parseInt(localStorage.getItem('id')))
      .subscribe(value => {
        this.services = value;
      })
  }

}
