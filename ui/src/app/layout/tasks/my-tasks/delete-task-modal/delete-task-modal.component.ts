import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {ServiceService} from "../../../../core/services/service.service";
import {Service} from "../../../../model/service.model";

@Component({
  selector: 'app-delete-task-modal',
  templateUrl: './delete-task-modal.component.html',
  styleUrls: ['./delete-task-modal.component.css']
})
export class DeleteTaskModalComponent implements OnInit {

  task: Service;
  @Output() valueChange = new EventEmitter();

  constructor(private fb: FormBuilder,
              private activeModal: NgbActiveModal,
              private serviceService: ServiceService) {
  }

  ngOnInit() {
  }

  closeModal() {
    this.activeModal.close();
  }

  onSubmit(): void {
    this.serviceService.deleteService(this.task.id)
      .subscribe(value => {
        this.closeModal();
        this.valueChange.emit()
      }, error1 => this.closeModal());
  };

}
