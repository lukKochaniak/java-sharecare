import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {first} from "rxjs/operators";
import {ServiceService} from "../../../../core/services/service.service";
import {Service} from "../../../../model/service.model";

@Component({
  selector: 'app-edit-task-modal',
  templateUrl: './edit-task-modal.component.html',
  styleUrls: ['./edit-task-modal.component.css']
})
export class EditTaskModalComponent implements OnInit {
  form: FormGroup;
  @Input() task: Service;
  @Output() valueChange = new EventEmitter();

  constructor(private fb: FormBuilder,
              private activeModal: NgbActiveModal,
              private serviceService: ServiceService) {
  }

  ngOnInit() {
    this.formInit();
  }

  formInit() {
    this.form = this.fb.group({
      id: this.task.id,
      checkindatehour: this.task.checkindatehour,
      checkoutdatehour: this.task.checkoutdatehour,
      providedby: this.task.providedby,
      requestedby: this.task.requestedby,
      status: this.task.status,
      title: [this.task.title, Validators.required],
      description: [this.task.description, Validators.required],
      cost: [this.task.cost, Validators.required],
      duration: [this.task.duration, Validators.required],
      address: [this.task.address, Validators.required],
      city: [this.task.city, Validators.required],
      zipcode: [this.task.zipcode, Validators.required],
    });
  }

  closeModal() {
    this.activeModal.close();
  }

  onSubmit() {
    this.form['providedby'] = this.task.providedby;
    this.form['requestedby'] = this.task.requestedby;
    this.serviceService.updateService(this.form.value)
      .pipe(first())
      .subscribe(
        data => {
          this.valueChange.emit()
          this.closeModal();
        },
        error => {
          alert("We got error in task editing, try again later!");
        });
  }

}
