import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ServiceService} from "../../../../core/services/service.service";
import {SuccessModalComponent} from "../../../../frontpage/success-modal/success-modal.component";
import {FailureModalComponent} from "../../../../frontpage/failure-modal/failure-modal.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private router: Router, private serviceService: ServiceService, private modalService: NgbModal) {
  }

  addForm: FormGroup;
  @Output() valueChange = new EventEmitter();

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      cost: ['', Validators.required],
      duration: ['', Validators.required],
      requestedBy: [window.localStorage.getItem('id'), Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      zipcode: ['', Validators.required],
    });

  }

  onSubmit() {
    this.serviceService.createService(this.addForm.value)
      .subscribe(data => {
        if(data !== undefined){
          this.valueChange.emit();
          this.onSuccess();
        }
      }, error1 => this.onFailure(error1));
  }

  private onSuccess = () => {
    const modal = this.modalService.open(SuccessModalComponent);
    modal.componentInstance.messageToShow = 'We added the task successfully!';
  };

  private onFailure = (error) => {
    const modal = this.modalService.open(FailureModalComponent);
    modal.componentInstance.errorToShow = error.error.errors[0].defaultMessage;
  };

}
