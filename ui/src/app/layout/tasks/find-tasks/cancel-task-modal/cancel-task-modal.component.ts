import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {ServiceService} from "../../../../core/services/service.service";
import {CancelServiceRequest, Service} from "../../../../model/service.model";

@Component({
  selector: 'app-cancel-task-modal',
  templateUrl: './cancel-task-modal.component.html',
  styleUrls: ['./cancel-task-modal.component.css']
})
export class CancelTaskModalComponent implements OnInit {

  task: Service;
  @Output() valueChange = new EventEmitter();

  constructor(private fb: FormBuilder,
              private activeModal: NgbActiveModal,
              private serviceService: ServiceService) {
  }

  ngOnInit() {

  }

  closeModal() {
    this.activeModal.close();
  }

  onSubmit() {
    this.serviceService.cancelService(this.task.id)
      .subscribe(value => {
        this.closeModal();
        this.valueChange.emit();
      }, error1 => this.closeModal());
  };

}
