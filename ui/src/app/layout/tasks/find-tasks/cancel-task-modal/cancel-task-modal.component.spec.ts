import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelTaskModalComponent } from './cancel-task-modal.component';

describe('CancelTaskModalComponent', () => {
  let component: CancelTaskModalComponent;
  let fixture: ComponentFixture<CancelTaskModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelTaskModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelTaskModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
