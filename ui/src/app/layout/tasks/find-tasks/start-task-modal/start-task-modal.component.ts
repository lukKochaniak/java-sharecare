import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-start-task-modal',
  templateUrl: './start-task-modal.component.html',
  styleUrls: ['./start-task-modal.component.css']
})
export class StartTaskModalComponent implements OnInit {

  constructor(private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  closeModal() {
    this.activeModal.close();
  }
}
