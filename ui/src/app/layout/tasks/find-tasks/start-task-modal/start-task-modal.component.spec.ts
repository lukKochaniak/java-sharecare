import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartTaskModalComponent } from './start-task-modal.component';

describe('StartTaskModalComponent', () => {
  let component: StartTaskModalComponent;
  let fixture: ComponentFixture<StartTaskModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartTaskModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartTaskModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
