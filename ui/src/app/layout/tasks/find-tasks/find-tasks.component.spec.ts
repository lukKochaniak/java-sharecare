import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindTasksComponent } from './find-tasks.component';

describe('FindTasksComponent', () => {
  let component: FindTasksComponent;
  let fixture: ComponentFixture<FindTasksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindTasksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindTasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
