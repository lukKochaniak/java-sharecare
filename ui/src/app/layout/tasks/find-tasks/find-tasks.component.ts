import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ServiceService} from "../../../core/services/service.service";

@Component({
  selector: 'app-find-tasks',
  templateUrl: './find-tasks.component.html',
  styleUrls: ['./find-tasks.component.css']
})
export class FindTasksComponent implements OnInit {

  services;
  pendingServices;

  constructor(private router: Router,
              private serviceService: ServiceService) {
  }

  ngOnInit() {
    this.getOpenServices();
    this.getPendingServices();
  }

  getOpenServices(){
    const request = { userId: parseInt(localStorage.getItem('id'))};
    this.serviceService.getOpenServices(request)
      .subscribe(value => {
        this.services = value;
      })
  }

  getPendingServices(){
    const request = { userId: parseInt(localStorage.getItem('id'))};
    this.serviceService.getPendingServices(request)
      .subscribe(value => {
        this.pendingServices = value;
      })
  }

}
