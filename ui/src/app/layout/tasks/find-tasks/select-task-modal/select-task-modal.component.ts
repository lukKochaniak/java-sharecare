import {Component, Input, OnInit} from '@angular/core';
import {Service} from "../../../../model/service.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ServiceService} from "../../../../core/services/service.service";
import {SuccessModalComponent} from "../../../../frontpage/success-modal/success-modal.component";
import {FailureModalComponent} from "../../../../frontpage/failure-modal/failure-modal.component";
import {NgbActiveModal, NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-select-task',
  templateUrl: './select-task-modal.component.html',
  styleUrls: ['./select-task-modal.component.css']
})
export class SelectTaskModalComponent implements OnInit {
  form: FormGroup;
  @Input() task: Service;

  constructor(private fb: FormBuilder,
              private activeModal: NgbActiveModal,
              private serviceService: ServiceService,
              private modalService: NgbModal) {
  }

  ngOnInit() {
    this.formInit();
  }

  formInit() {
    this.form = this.fb.group({
      id: this.task.id,
      checkindatehour: this.task.checkindatehour,
      checkoutdatehour: this.task.checkoutdatehour,
      providedby: this.task.providedby,
      requestedby: this.task.requestedby,
      title: [this.task.title, Validators.required],
      description: [this.task.description, Validators.required],
      cost: [this.task.cost, Validators.required],
      duration: [this.task.duration, Validators.required],
      address: [this.task.address, Validators.required],
      city: [this.task.city, Validators.required],
      zipcode: [this.task.zipcode, Validators.required],
    });
  }

  closeModal() {
    this.activeModal.close();
  }

  onSubmit() {
    const request = {
      serviceId: this.task.id,
      userId: parseInt(localStorage.getItem('id'))
    };

    this.serviceService.requestService(request)
      .subscribe((response) => {
        if (response.status === 200) {
          this.closeModal();
          const modalRef = this.modalService.open(SuccessModalComponent);
          modalRef.componentInstance.messageToShow = "Request sent";
        } else {
          this.closeModal();
          const modal = this.modalService.open(FailureModalComponent);
          modal.componentInstance.messageToShow = response.message;
        }
      }, error1 => {
        this.closeModal();
        const modal = this.modalService.open(FailureModalComponent);
        modal.componentInstance.errorToShow = "Something went wrong";
      });
  }

}
