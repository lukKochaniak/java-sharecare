import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Service, ServiceSnapshot} from "../../../model/service.model";
import {ServiceService} from "../../../core/services/service.service";
import {Router} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {EditTaskModalComponent} from "../my-tasks/edit-task-modal/edit-task-modal.component";
import {DeleteTaskModalComponent} from "../my-tasks/delete-task-modal/delete-task-modal.component";
import {SelectTaskModalComponent} from "../find-tasks/select-task-modal/select-task-modal.component";
import {StartTaskModalComponent} from "../find-tasks/start-task-modal/start-task-modal.component";
import {CancelTaskModalComponent} from "../find-tasks/cancel-task-modal/cancel-task-modal.component";
import {ShowTaskOnMapComponent} from "../show-task-on-map/show-task-on-map.component";

@Component({
  selector: 'app-single-task',
  templateUrl: './single-task.component.html',
  styleUrls: ['./single-task.component.css']
})
export class SingleTaskComponent implements OnInit {

  @Input() service: Service;
  @Input() mode: string;
  @Output() valueChange = new EventEmitter();

  constructor(private serviceService: ServiceService,
              private modalService: NgbModal,
              private router: Router) {
  }

  ngOnInit() {
  }

  deleteService(): void {
    const modalRef = this.modalService.open(DeleteTaskModalComponent);
    modalRef.componentInstance.task = this.service;
    modalRef.componentInstance.valueChange.subscribe( () => {
      this.valueChange.emit();
    })
  };

  editService(): void {
    const modalRef = this.modalService.open(EditTaskModalComponent);
    modalRef.componentInstance.task = this.service;
    modalRef.componentInstance.valueChange.subscribe( () => {
      this.valueChange.emit();
    })
  };

  selectService(): void {
    const modalRef = this.modalService.open(SelectTaskModalComponent);
    modalRef.componentInstance.task = this.service;
  };


  showOnMap() {
    const modalRef = this.modalService.open(ShowTaskOnMapComponent);
    modalRef.componentInstance.task = this.service;
  }

  startService() {
    const modalRef = this.modalService.open(StartTaskModalComponent);
    modalRef.componentInstance.task = this.service;
  }

  cancelTask() {
    const modalRef = this.modalService.open(CancelTaskModalComponent);
    modalRef.componentInstance.task = this.service;
    modalRef.componentInstance.valueChange.subscribe( () => {
      this.valueChange.emit();
    })
  }
}
