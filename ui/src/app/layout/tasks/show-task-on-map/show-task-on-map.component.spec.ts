import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowTaskOnMapComponent } from './show-task-on-map.component';

describe('ShowTaskOnMapComponent', () => {
  let component: ShowTaskOnMapComponent;
  let fixture: ComponentFixture<ShowTaskOnMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowTaskOnMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowTaskOnMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
