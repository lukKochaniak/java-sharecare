import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {Service} from "../../../model/service.model";
declare var google: any;

@Component({
  selector: 'app-show-task-on-map',
  templateUrl: './show-task-on-map.component.html',
  styleUrls: ['./show-task-on-map.component.css']
})
export class ShowTaskOnMapComponent implements OnInit {

  lat;
  lng;
  private geoCoder;
  zoom: number;
  address: string;
  task: Service;

  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.geoCoder = new google.maps.Geocoder;
    this.getAddress();
  }

  getAddress() {
    this.geoCoder.geocode({ 'address': this.task.address + this.task.city + this.task.zipcode }, (results, status) => {

      if (status === 'OK') {
        if (results[0]) {
          this.lat = results[0].geometry.location.lat();
          this.lng = results[0].geometry.location.lng();
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
    });
  }

  closeModal() {
    this.activeModal.close();
  }

}
