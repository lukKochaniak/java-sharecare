import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TasksComponent} from './my-tasks/tasks.component';
import {AddTaskComponent} from './my-tasks/add-task/add-task.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FindTasksComponent} from './find-tasks/find-tasks.component';
import {HistoryTasksComponent} from './history-tasks/history-tasks.component';
import {SingleTaskComponent} from './single-task/single-task.component';
import {EditTaskModalComponent} from './my-tasks/edit-task-modal/edit-task-modal.component';
import {DeleteTaskModalComponent} from './my-tasks/delete-task-modal/delete-task-modal.component';
import {NgbModalModule} from "@ng-bootstrap/ng-bootstrap";
import {StartTaskModalComponent} from './find-tasks/start-task-modal/start-task-modal.component';
import {SelectTaskModalComponent} from "./find-tasks/select-task-modal/select-task-modal.component";
import {CancelTaskModalComponent} from './find-tasks/cancel-task-modal/cancel-task-modal.component';
import { AgmCoreModule } from '@agm/core';
import { ShowTaskOnMapComponent } from './show-task-on-map/show-task-on-map.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModalModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA31CtPyy0lfZ-Gnzpj6ovTI0xAjVCbeiw',
      libraries: ['places']
    })

  ],
  declarations: [
    TasksComponent,
    AddTaskComponent,
    FindTasksComponent,
    HistoryTasksComponent,
    SingleTaskComponent,
    EditTaskModalComponent,
    DeleteTaskModalComponent,
    StartTaskModalComponent,
    CancelTaskModalComponent,
    ShowTaskOnMapComponent
  ],
  entryComponents: [
    EditTaskModalComponent,
    DeleteTaskModalComponent,
    SelectTaskModalComponent,
    StartTaskModalComponent,
    CancelTaskModalComponent,
    ShowTaskOnMapComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class TasksModule {
}
