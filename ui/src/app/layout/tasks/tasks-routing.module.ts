import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TasksComponent} from "./my-tasks/tasks.component";
import {AddTaskComponent} from "./my-tasks/add-task/add-task.component";
import {FindTasksComponent} from "./find-tasks/find-tasks.component";
import {SelectTaskModalComponent} from "./find-tasks/select-task-modal/select-task-modal.component";
import {HistoryTasksComponent} from "./history-tasks/history-tasks.component";

const routes: Routes = [
  {
    path: '',
    component: TasksComponent,
    children: [
      {path: 'add', component: AddTaskComponent},
    ],
  },
  {
    path: 'find',
    component: FindTasksComponent,
    children: [
      {path: 'select', component: SelectTaskModalComponent}
    ]
  },
  {
    path: 'history',
    component: HistoryTasksComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TasksRoutingModule {
}
