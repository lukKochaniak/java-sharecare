import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ServiceService} from "../../../core/services/service.service";
import {SendAssignElderlyModalComponent} from "../../linking-accounts/send-assign-elderly-modal/send-assign-elderly-modal.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-history-tasks',
  templateUrl: './history-tasks.component.html',
  styleUrls: ['./history-tasks.component.css']
})
export class HistoryTasksComponent implements OnInit {

  services;

  constructor(private router: Router, private serviceService: ServiceService, private modalService: NgbModal) {
  }

  ngOnInit() {
    this.serviceService.getCompletedBy(parseInt(localStorage.getItem('id')))
      .subscribe(value => {
        this.services = value;
        console.log(this.services);
      })
  }

  openSendAssignElderlyOfferModal() {
  }


}
