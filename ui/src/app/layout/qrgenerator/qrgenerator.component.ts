import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-qrgenerator',
  templateUrl: './qrgenerator.component.html',
  styleUrls: ['./qrgenerator.component.css']
})
export class QrgeneratorComponent implements OnInit {

  private userId: string = null;

  constructor() {
    this.userId = localStorage.getItem('id');
  }

  ngOnInit() {
  }

}
