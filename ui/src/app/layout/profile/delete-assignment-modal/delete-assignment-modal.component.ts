import {Component, OnInit} from '@angular/core';
import {Service} from "../../../model/service.model";
import {FormBuilder} from "@angular/forms";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {ElderlyAssignmentService} from "../../../core/services/elderlyAssignment.service";

@Component({
  selector: 'app-delete-assignment-modal',
  templateUrl: './delete-assignment-modal.component.html',
  styleUrls: ['./delete-assignment-modal.component.css']
})
export class DeleteAssignmentModalComponent implements OnInit {

  task: Service;
  secondUserId: number;
  userRole: string;
  messageToSend;

  constructor(private fb: FormBuilder,
              private activeModal: NgbActiveModal,
              private elderlyAssignmentService: ElderlyAssignmentService) {
  }

  ngOnInit() {
    this.getRole();
  }

  getRole() {
    this.userRole = localStorage.getItem('role');
  }

  closeModal() {
    this.activeModal.close();
  }


  onSubmit() {
    this.decideOnMessageDependingOnRole();
    this.elderlyAssignmentService.deleteElderlyAssignment(this.messageToSend)
      .subscribe(value => this.closeModal(), error1 => this.closeModal());
  };

  decideOnMessageDependingOnRole() {
    if (this.userRole === 'CAREGIVER') {
      this.messageToSend = {
        elderlyId: this.secondUserId,
        caregiverId: parseInt(localStorage.getItem('id'))
      }
    } else {
      this.messageToSend = {
        elderlyId: parseInt(localStorage.getItem('id')),
        caregiverId: this.secondUserId
      }
    }
  }


}
