import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteAssignmentModalComponent } from './delete-assignment-modal.component';

describe('DeleteAssignmentModalComponent', () => {
  let component: DeleteAssignmentModalComponent;
  let fixture: ComponentFixture<DeleteAssignmentModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteAssignmentModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAssignmentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
