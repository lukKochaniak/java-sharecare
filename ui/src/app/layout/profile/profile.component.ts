import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../core/services/user.service";
import {ProfileData} from "../../model/user.model";
import {validation_messages} from "../../frontpage/common/validation_messages";
import {MyErrorStateMatcher} from "../../frontpage/sign-up-modal/myErrorStateMatcher";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {SuccessModalComponent} from "../../frontpage/success-modal/success-modal.component";
import {FailureModalComponent} from "../../frontpage/failure-modal/failure-modal.component";
import {ActivatedRoute} from "@angular/router";
import {BlockchainService} from "../../core/services/blockchain.service";
import {DeleteAssignmentModalComponent} from "./delete-assignment-modal/delete-assignment-modal.component";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  form: FormGroup;
  matching_passwords_group: FormGroup;
  dateForm: FormGroup;
  editingElderly: boolean;
  tokensAmount: number;
  elderlyId: number;
  userRole: string;
  @ViewChild('tokensToSend') tokensToSend:ElementRef;

  profile_validation_messages = validation_messages;
  matcher = new MyErrorStateMatcher();

  public email: string;
  public firstName: string;
  public lastName: string;
  public address: string;
  public city: string;
  public zipCode: string;
  public dateOfBirth: Date;
  public password: string;

  public months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

  public testYear = 2019;
  public testMonth = "Jan";
  public testDay = 1;

  constructor(private fb: FormBuilder,
              private userService: UserService,
              private modalService: NgbModal,
              private route: ActivatedRoute,
              private blockchainService: BlockchainService) {
  }

  ngOnInit() {
    this.buildForm();
    this.loadUserData();
    this.getTimetokenBalance();
    this.getUserRole();
  }

  private buildForm(): void {
    this.form = this.fb.group({
      firstName: ['', Validators.compose([
        Validators.required,
      ])],
      lastName: ['', Validators.compose([
        Validators.required,
      ])],
      dateOfBirth: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      zipcode: ['', Validators.required]
    });

    this.matching_passwords_group = this.fb.group({
      currentPassword: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.maxLength(255),
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$') //this is for the letters (both uppercase and lowercase) and numbers validation
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.maxLength(255),
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$') //this is for the letters (both uppercase and lowercase) and numbers validation
      ])),
      confirmPassword: new FormControl('', Validators.required)
    }, {validator: this.checkPasswords});

    this.dateForm = this.fb.group({
      dayForm: ['', Validators.required],
      monthForm: ['', Validators.required],
      yearForm: ['', Validators.required]
    });
  }

  getTimetokenBalance() {
    this.blockchainService.getBalance(parseInt(localStorage.getItem('id'))).subscribe(value => {
      this.tokensAmount = value.result;
    });
  }

  getUserRole(){
    this.userRole = localStorage.getItem('role');
  }

  private loadUserData() {
    if (this.route.snapshot.params['id']) {
      this.editingElderly = true;
      this.elderlyId = this.route.snapshot.params['id'];
      this.userService.getElderly(this.elderlyId).subscribe(request => {
        this.firstName = request.result.firstName;
        this.lastName = request.result.lastName;
        this.address = request.result.address;
        this.city = request.result.city;
        this.zipCode = request.result.zipcode;
        this.dateOfBirth = new Date(request.result.dateOfBirth);

        this.testYear = this.dateOfBirth.getFullYear();
        this.testMonth = this.months[this.dateOfBirth.getMonth()];
        this.testDay = this.dateOfBirth.getDate();

        this.form.value.dateOfBirth = this.dateOfBirth;
      });
    } else {
      this.loadMyProfile();
    }
  }

  loadMyProfile() {
    this.email = localStorage.getItem('userEmail');
    this.firstName = localStorage.getItem('firstName');
    this.lastName = localStorage.getItem('lastName');

    this.getProfileData();
  }

  getProfileData() {
    this.userService.getProfileData(this.email) //add checking status
      .subscribe((request) => {
        this.address = request.result.address;
        this.city = request.result.city;
        this.zipCode = request.result.zipcode;
        this.dateOfBirth = new Date(request.result.dateOfBirth);

        this.testYear = this.dateOfBirth.getFullYear();
        this.testMonth = this.months[this.dateOfBirth.getMonth()];
        this.testDay = this.dateOfBirth.getDate();

        this.form.value.dateOfBirth = this.dateOfBirth;
      });
  }

  checkPasswords(group: FormGroup) {
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : {areEqual: true}
  } // change this function

  checkIfHighlightError(checkItem: string): boolean {
    let hasError = false;
    let array;

    if (checkItem === 'firstName') {
      array = this.profile_validation_messages.firstName;
    }
    if (checkItem === 'lastName') {
      array = this.profile_validation_messages.lastName;
    }
    if (checkItem === 'address') {
      array = this.profile_validation_messages.address;
    }
    if (checkItem === 'city') {
      array = this.profile_validation_messages.city;
    }
    if (checkItem === 'zipcode') {
      array = this.profile_validation_messages.zipcode;
    }
    for (let validation of array) {
      if (this.form.get(checkItem).hasError(validation.type)) {
        hasError = true;
      }
    }
    return hasError && this.form.get(checkItem).touched
  }

  checkIfPasswordCorrect(checkItem: string): boolean {
    let hasError = false;
    let array;

    if (checkItem === 'confirmPassword') {
      array = this.profile_validation_messages.confirmPassword;
    }
    if (checkItem === 'password') {
      array = this.profile_validation_messages.password;
    }
    for (let validation of array) {
      if (this.matching_passwords_group.get(checkItem).hasError(validation.type)) {
        hasError = true;
      }
    }
    return hasError && this.matching_passwords_group.get(checkItem).touched
  }

  updateProfile() {
    let profileData: ProfileData;
    profileData = this.createUpdateDataForm();

    this.userService.updateProfile(profileData)
      .subscribe((response) => {
        if (response.status === 200) {
          const modalRef = this.modalService.open(SuccessModalComponent);
          modalRef.componentInstance.messageToShow = "Profile successfully updated";
          localStorage.setItem('firstName', profileData.firstName);
          localStorage.setItem('lastName', profileData.lastName);
        } else {
          const modal = this.modalService.open(FailureModalComponent);
          modal.componentInstance.messageToShow = response.message;
        }
      }, error1 => {
        const modal = this.modalService.open(FailureModalComponent);
        modal.componentInstance.errorToShow = "Something went wrong";
      });
  }

  private createUpdateDataForm(): ProfileData {

    let newFirstName: string;
    let newLastName: string;
    let newAddress: string;
    let newCity: string;
    let newZipCode: string;
    let newDateOfBirth: Date;

    if (this.form.value.firstName.trim() === null || this.form.value.firstName.trim() === "") {
      newFirstName = this.firstName;
    } else {
      newFirstName = this.form.value.firstName.trim();
    }

    if (this.form.value.lastName.trim() === null || this.form.value.lastName.trim() === "") {
      newLastName = this.lastName;
    } else {
      newLastName = this.form.value.lastName.trim();
    }

    if (this.form.value.address.trim() === null || this.form.value.address.trim() === "") {
      newAddress = this.address;
    } else {
      newAddress = this.form.value.address.trim();
    }

    if (this.form.value.city.trim() === null || this.form.value.city.trim() === "") {
      newCity = this.city;
    } else {
      newCity = this.form.value.city.trim();
    }

    if (this.form.value.zipcode.trim() === null || this.form.value.zipcode.trim() === "") {
      newZipCode = this.zipCode;
    } else {
      newZipCode = this.form.value.zipcode.trim();
    }

    newDateOfBirth = new Date(this.form.value.dateOfBirth);

    return {
      email: this.email,
      firstName: newFirstName,
      lastName: newLastName,
      address: newAddress,
      city: newCity,
      zipcode: newZipCode,
      dateOfBirth: newDateOfBirth
    };
  }

  updatePassword() {
    let passwordUpdateData = {
      email: this.email,
      currentPassword: this.matching_passwords_group.value.currentPassword,
      newPassword: this.matching_passwords_group.value.password
    }

    this.userService.updatePassword(passwordUpdateData)
      .subscribe((response) => {
        if (response.status === 200) {
          const modalRef = this.modalService.open(SuccessModalComponent);
          modalRef.componentInstance.messageToShow = "Password successfully changed"
        } else {
          const modal = this.modalService.open(FailureModalComponent);
          modal.componentInstance.messageToShow = response.message;
        }
      }, error1 => {
        const modal = this.modalService.open(FailureModalComponent);
        modal.componentInstance.errorToShow = "Something went wrong";
      });
  }

  transferTimetokens() {
    const addContractRequest = {
      amount: this.tokensToSend.nativeElement.value,
      fromId: parseInt(localStorage.getItem('id')),
      toId: this.elderlyId
    };
    this.blockchainService.pay(addContractRequest).subscribe(value => console.log(value));
  }

  deleteConnection() {
    const modal = this.modalService.open(DeleteAssignmentModalComponent);
    modal.componentInstance.secondUserId = this.elderlyId;
  }
}
