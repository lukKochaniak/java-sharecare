import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LayoutComponent} from './layout.component';
import {ProfileComponent} from "./profile/profile.component";
import {TasksComponent} from "./tasks/my-tasks/tasks.component";
import {AddTaskComponent} from "./tasks/my-tasks/add-task/add-task.component";
import {QrgeneratorComponent} from "./qrgenerator/qrgenerator.component";
import {FindTasksComponent} from "./tasks/find-tasks/find-tasks.component";
import {SelectTaskModalComponent} from "./tasks/find-tasks/select-task-modal/select-task-modal.component";
import {HistoryTasksComponent} from "./tasks/history-tasks/history-tasks.component";
import {NotificationListComponent} from "./components/notifications/notification-list/notification-list.component";
import {DashboardComponent} from "./dashboard/dashboard.component";

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', component: DashboardComponent },
            { path: 'profile', component: ProfileComponent },
            { path: 'elderly/:id', component: ProfileComponent },
            { path: 'tasks', component: TasksComponent },
            { path: 'tasks/add', component: AddTaskComponent },
            { path: 'tasks/find', component: FindTasksComponent },
            { path: 'tasks/select', component: SelectTaskModalComponent },
            { path: 'tasks/history', component: HistoryTasksComponent },
            { path: 'qrgenerator', component: QrgeneratorComponent },
            { path: 'notifications', component: NotificationListComponent }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule {
}
