import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {NgbDropdownModule, NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import {LayoutRoutingModule} from './layout-routing.module';
import {LayoutComponent} from './layout.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {HeaderComponent} from './components/header/header.component';
import {ProfileComponent} from './profile/profile.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ComboDatepickerModule} from "ngx-combo-datepicker";
import {MatButtonModule, MatFormFieldModule, MatInputModule, MatRippleModule} from "@angular/material";
import {TasksComponent} from "./tasks/my-tasks/tasks.component";
import {AddTaskComponent} from "./tasks/my-tasks/add-task/add-task.component";
import {FindTasksComponent} from "./tasks/find-tasks/find-tasks.component";
import {SelectTaskModalComponent} from "./tasks/find-tasks/select-task-modal/select-task-modal.component";
import {HistoryTasksComponent} from "./tasks/history-tasks/history-tasks.component";
import {NgSelectModule} from '@ng-select/ng-select';
import {QrgeneratorComponent} from './qrgenerator/qrgenerator.component';
import {QRCodeModule} from "angularx-qrcode";
import {InternationalPhoneNumberModule} from "ngx-international-phone-number";
import {NotificationListComponent} from './components/notifications/notification-list/notification-list.component';
import {DashboardComponent} from "./dashboard/dashboard.component";
import {StatModule} from "../shared/stat/stat.module";
import {NotificationComponent} from './components/notifications/notification/notification.component';
import {WebSocketService} from "../core/services/websocket.service";
import {AssignedElderliesComponent} from './dashboard/assigned-elderlies/assigned-elderlies.component';
import {ElderlyCardComponent} from './dashboard/assigned-elderlies/elderly-card/elderly-card.component';
import {SingleTaskComponent} from "./tasks/single-task/single-task.component";
import {EditTaskModalComponent} from "./tasks/my-tasks/edit-task-modal/edit-task-modal.component";
import {DeleteTaskModalComponent} from "./tasks/my-tasks/delete-task-modal/delete-task-modal.component";
import {StartTaskModalComponent} from "./tasks/find-tasks/start-task-modal/start-task-modal.component";
import {CancelTaskModalComponent} from "./tasks/find-tasks/cancel-task-modal/cancel-task-modal.component";
import { DeleteAssignmentModalComponent } from './profile/delete-assignment-modal/delete-assignment-modal.component';
import {ShowTaskOnMapComponent} from "./tasks/show-task-on-map/show-task-on-map.component";
import {AgmCoreModule} from "@agm/core";

@NgModule({
  imports: [
    CommonModule,
    LayoutRoutingModule,
    TranslateModule,
    NgbDropdownModule,
    ReactiveFormsModule,
    ComboDatepickerModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    NgbDropdownModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    QRCodeModule,
    InternationalPhoneNumberModule,
    StatModule,
    NgbModalModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA31CtPyy0lfZ-Gnzpj6ovTI0xAjVCbeiw',
      libraries: ['places']
    })
  ],
  declarations: [
    LayoutComponent,
    SidebarComponent,
    HeaderComponent,
    TasksComponent,
    AddTaskComponent,
    FindTasksComponent,
    SelectTaskModalComponent,
    HistoryTasksComponent,
    SingleTaskComponent,
    StartTaskModalComponent,
    EditTaskModalComponent,
    DeleteTaskModalComponent,
    CancelTaskModalComponent,
    ProfileComponent,
    QrgeneratorComponent,
    NotificationListComponent,
    DashboardComponent,
    NotificationComponent,
    AssignedElderliesComponent,
    ElderlyCardComponent,
    DeleteAssignmentModalComponent,
    ShowTaskOnMapComponent
  ],
  entryComponents: [
    EditTaskModalComponent,
    DeleteTaskModalComponent,
    SelectTaskModalComponent,
    StartTaskModalComponent,
    CancelTaskModalComponent,
    DeleteAssignmentModalComponent,
    ShowTaskOnMapComponent
  ],
  providers: [WebSocketService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class LayoutModule {
}
