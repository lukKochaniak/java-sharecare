import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {LanguageTranslationModule} from "./shared/language-translation/language-translation.module";
import {AppRoutingModule} from "./app-routing.module";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {CommonModule} from "@angular/common";
import {SuccessModalComponent} from "./frontpage/success-modal/success-modal.component";
import {FailureModalComponent} from "./frontpage/failure-modal/failure-modal.component";
import {SendAssignElderlyModalComponent} from "./layout/linking-accounts/send-assign-elderly-modal/send-assign-elderly-modal.component";
import {ReactiveFormsModule} from "@angular/forms";
import {InternationalPhoneNumberModule} from "ngx-international-phone-number";
import {NotificationModalComponent} from "./layout/components/notifications/notification-modal/notification-modal.component";

@NgModule({
  declarations: [
    AppComponent,
    SuccessModalComponent,
    FailureModalComponent,
    SendAssignElderlyModalComponent,
    NotificationModalComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    LanguageTranslationModule,
    NoopAnimationsModule,
    ReactiveFormsModule,
    InternationalPhoneNumberModule
  ],
  entryComponents: [
    SuccessModalComponent,
    FailureModalComponent,
    SendAssignElderlyModalComponent,
    NotificationModalComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
