import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {NgbActiveModal, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {UserService} from "../../core/services/user.service";
import {SignInForm, SignUpForm, UserSnapshot} from "../../model/user.model";
import {ForgotPasswordModalComponent} from "../forgot-password-modal/forgot-password-modal.component";
import {Router} from '@angular/router';
import {FailureModalComponent} from '../failure-modal/failure-modal.component';
import {validation_messages} from "../common/validation_messages";
import {ModalStepperComponent} from "../sign-up-modal/modal-stepper/modal-stepper.component";

@Component({
  selector: 'app-sign-in-modal',
  templateUrl: './sign-in-modal.component.html',
  styleUrls: ['./sign-in-modal.component.css']
})
export class SignInModalComponent implements OnInit {
  form: FormGroup;
  account_validation_messages = validation_messages;


  constructor(private fb: FormBuilder,
              private activeModal: NgbActiveModal,
              private userService: UserService,
              private router: Router,
              private modalService: NgbModal) { }


  ngOnInit() {
    this.buildForm();
  }

  private buildForm(): void {
    this.form = this.fb.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])],
      password: ['', Validators.compose([
        Validators.minLength(5),
        Validators.maxLength(255),
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$') //this is for the letters (both uppercase and lowercase) and numbers validation
      ])]
    });
  }

  private createSingInData(): SignInForm {
    return {
      email: this.form.value.email.trim(),
      password: this.form.value.password.trim()
    };
  }

  login() {
    const loginPayload = this.createSingInData();
    this.userService.login(loginPayload)
      .subscribe((response) => {
        if (response.status === 200) {
          this.saveUserDataToLocalStorage(response.result);
          this.router.navigate(['/home']);
          this.activeModal.close();
        } else {
          this.activeModal.close();
          const modal = this.modalService.open(FailureModalComponent);
          modal.componentInstance.errorToShow = response;
        }
      }, error1 => {
        this.activeModal.close();
        const modal = this.modalService.open(FailureModalComponent);
        modal.componentInstance.errorToShow = error1.error.errors[0].defaultMessage;
      });
  }

  checkIfCorrect(checkItem: string): boolean {
    let hasError = false;
    let array;

    if (checkItem === 'email') {
      array = this.account_validation_messages.email;
    }
    if (checkItem === 'password') {
      array = this.account_validation_messages.password;
    }
    for (let validation of array) {
      if (this.form.get(checkItem).hasError(validation.type)) {
        hasError = true;
      }
    }
    return hasError && this.form.get(checkItem).touched
  }


  saveUserDataToLocalStorage(userSnapshot: UserSnapshot) {
    localStorage.setItem('id', userSnapshot.id);
    localStorage.setItem('userEmail', userSnapshot.email);
    localStorage.setItem('firstName', userSnapshot.firstName);
    localStorage.setItem('lastName', userSnapshot.lastName);
    localStorage.setItem('role', userSnapshot.role.toString());
  }

  openSignUpModal(){
    const modalRef = this.modalService.open(ModalStepperComponent);
    this.closeModal();
  }

  openForgotPasswordModal(){
    const modalRef = this.modalService.open(ForgotPasswordModalComponent);
    this.closeModal();
  }

  closeModal(){
    this.activeModal.close();
  }

}
