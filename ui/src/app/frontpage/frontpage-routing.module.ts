import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FrontpageComponent} from "./frontpage.component";

const routes: Routes = [
  {
    path: '', component: FrontpageComponent,
    children: [
      {path: 'error/:param', component: FrontpageComponent},
      {path: 'success/:param', component: FrontpageComponent,},
      {path: 'set/:param', component: FrontpageComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FrontpageRoutingModule {
}
