import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {validation_messages} from "../../common/validation_messages";
import {MyErrorStateMatcher} from "../myErrorStateMatcher";
import {NgbActiveModal, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {UserService} from "../../../core/services/user.service";
import {SignUpForm} from "../../../model/user.model";
import {SuccessModalComponent} from "../../success-modal/success-modal.component";
import {FailureModalComponent} from "../../failure-modal/failure-modal.component";

@Component({
  selector: 'app-modal-stepper',
  templateUrl: './modal-stepper.component.html',
  styleUrls: ['./modal-stepper.component.css'],
})
export class ModalStepperComponent implements OnInit {
  isFirstNotVisible = false;
  isSecondNotVisible = true;
  isThirdNotVisible = true;
  progressBar = 0;
  form: FormGroup;
  account_validation_messages = validation_messages;
  matching_passwords_group: FormGroup;
  matching_email_group: FormGroup;
  matcher = new MyErrorStateMatcher();


  constructor(private fb: FormBuilder,
              private activeModal: NgbActiveModal,
              private userService: UserService,
              private modalService: NgbModal) {
  }

  ngOnInit() {
    this.buildForm();
  }

  private buildForm(): void {
    this.form = this.fb.group({
      firstName: ['', Validators.compose([
        Validators.required,
      ])],
      lastName: ['', Validators.compose([
        Validators.required,
      ])],
      dateOfBirth: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      zipcode: ['', Validators.required],
      role: ['CAREGIVER', Validators.required],
      phoneNumber: ['', Validators.required]
    });

    this.matching_passwords_group = this.fb.group({
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.maxLength(255),
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$') //this is for the letters (both uppercase and lowercase) and numbers validation
      ])),
      confirmPassword: new FormControl('', Validators.required)
    }, {validator: this.checkPasswords});

    this.matching_email_group = this.fb.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])],
      confirmEmail: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])]
    }, {validator: this.checkEmails});
  }

  checkPasswords(group: FormGroup) {
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : {areEqual: true}
  }

  checkEmails(group: FormGroup) {
    let email = group.controls.email.value;
    let confirmEmail = group.controls.confirmEmail.value;

    return email === confirmEmail ? null : {areEqual: true}
  }

  private createSingUpData(): SignUpForm {
    return {
      firstName: this.form.value.firstName.trim(),
      lastName: this.form.value.lastName.trim(),
      email: this.matching_email_group.value.email.trim(),
      password: this.matching_passwords_group.value.password.trim(),
      dateOfBirth: this.form.value.dateOfBirth,
      phoneNumber: this.form.value.phoneNumber,
      address: this.form.value.address.trim(),
      city: this.form.value.city.trim(),
      zipcode: this.form.value.zipcode.trim(),
      role: this.form.value.role
    };
  }

  ifThereAreAnyErrors(): boolean {
    if (this.checkIfFieldIsEmpty(this.form)) return true;
    if (this.checkIfFieldIsEmpty(this.matching_email_group)) return true;
    if (this.checkIfFieldIsEmpty(this.matching_passwords_group)) return true;

    let hasErrors = false;
    this.form.errors === null ? null : hasErrors = true;
    this.matching_passwords_group.errors === null ? null : hasErrors = true;
    this.matching_email_group.errors === null ? null : hasErrors = true;
    return hasErrors;

  }

  checkIfFieldIsEmpty(form: FormGroup): boolean {
    let hasErrors = false;
    Object.keys(this.form.controls).forEach(key => {
      this.form.get(key).value === '' ? hasErrors = true : null;
    });
    if(this.form.value.dateOfBirth === null) hasErrors = true;
    return hasErrors;
  }

  createNewAccount() {
    const newAccount = this.createSingUpData();
    this.userService.create(newAccount)
      .subscribe((newUser) => this.onSuccess(newUser), error1 => this.onFailure(error1));
  }

  closeModal() {
    this.activeModal.close();
  }

  checkIfHighlightError(checkItem: string): boolean {
    let hasError = false;
    let array;

    if (checkItem === 'firstName') {
      array = this.account_validation_messages.firstName;
    }
    if (checkItem === 'lastName') {
      array = this.account_validation_messages.lastName;
    }
    if (checkItem === 'address') {
      array = this.account_validation_messages.address;
    }
    if (checkItem === 'city') {
      array = this.account_validation_messages.city;
    }
    if (checkItem === 'zipcode') {
      array = this.account_validation_messages.zipcode;
    }
    if (checkItem === 'phoneNumber') {
      array = this.account_validation_messages.phoneNumber;
    }
    if (checkItem === 'dateOfBirth') {
      array = this.account_validation_messages.dateOfBirth;
    }
    for (let validation of array) {
      if (this.form.get(checkItem).hasError(validation.type)) {
        hasError = true;
      }
    }
    return hasError && this.form.get(checkItem).touched
  }

  checkIfPasswordCorrect(checkItem: string): boolean {
    let hasError = false;
    let array;

    if (checkItem === 'confirmPassword') {
      array = this.account_validation_messages.confirmPassword;
    }
    if (checkItem === 'password') {
      array = this.account_validation_messages.password;
    }
    for (let validation of array) {
      if (this.matching_passwords_group.get(checkItem).hasError(validation.type)) {
        hasError = true;
      }
    }
    return hasError && this.matching_passwords_group.get(checkItem).touched
  }

  checkIfEmailCorrect(checkItem: string): boolean {
    let hasError = false;
    let array;

    if (checkItem === 'confirmEmail') {
      array = this.account_validation_messages.confirmEmail;
    }
    if (checkItem === 'email') {
      array = this.account_validation_messages.email;
    }
    for (let validation of array) {
      if (this.matching_email_group.get(checkItem).hasError(validation.type)) {
        hasError = true;
      }
    }
    return hasError && this.matching_email_group.get(checkItem).touched
  }

  private onSuccess = (newUser) => {
    this.activeModal.close();
    const modal = this.modalService.open(SuccessModalComponent);
    modal.componentInstance.messageToShow = 'Verification email has been sent to: ' + newUser.email;
  };

  private onFailure = (error) => {
    this.activeModal.close();
    const modal = this.modalService.open(FailureModalComponent);
    modal.componentInstance.errorToShow = error.error.errors[0].defaultMessage;
  };


  checkWhichStepAndMove(isBack: boolean) {
    if(isBack){
      this.moveBackwards();
      return;
    }

    if (!this.isFirstNotVisible && this.isSecondNotVisible && this.isThirdNotVisible) {
      this.isFirstNotVisible = true;
      this.isSecondNotVisible = false;
      this.progressBar = 50;
      return;
    }
    if (!this.isSecondNotVisible && this.isFirstNotVisible && this.isThirdNotVisible) {
      this.isSecondNotVisible = true;
      this.isThirdNotVisible = false;
      this.progressBar = 100;
      return;
    }

  }

  moveBackwards(){
    if (!this.isThirdNotVisible && this.isFirstNotVisible && this.isSecondNotVisible ) {
      this.isThirdNotVisible = true;
      this.isSecondNotVisible = false;
      this.progressBar = 50;
      return;
    }
    if (!this.isSecondNotVisible && this.isFirstNotVisible && this.isThirdNotVisible) {
      this.isSecondNotVisible = true;
      this.isFirstNotVisible = false;
      this.progressBar = 0;
      return;
    }
  }
}
