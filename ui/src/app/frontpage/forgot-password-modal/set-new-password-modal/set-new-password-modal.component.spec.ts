import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetNewPasswordModalComponent } from './set-new-password-modal.component';

describe('SetNewPasswordModalComponent', () => {
  let component: SetNewPasswordModalComponent;
  let fixture: ComponentFixture<SetNewPasswordModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetNewPasswordModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetNewPasswordModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
