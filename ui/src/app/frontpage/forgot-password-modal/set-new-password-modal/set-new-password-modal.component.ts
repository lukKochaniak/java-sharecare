import { Component, OnInit } from '@angular/core';
import {NgbActiveModal, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {UserService} from "../../../core/services/user.service";
import {SuccessModalComponent} from "../../success-modal/success-modal.component";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {validation_messages} from "../../common/validation_messages";
import {MyErrorStateMatcher} from "../../sign-up-modal/myErrorStateMatcher";
import {FailureModalComponent} from "../../failure-modal/failure-modal.component";
import {printLine} from "tslint/lib/verify/lines";

@Component({
  selector: 'app-set-new-password-modal',
  templateUrl: './set-new-password-modal.component.html',
  styleUrls: ['./set-new-password-modal.component.css']
})
export class SetNewPasswordModalComponent implements OnInit {

  form: FormGroup;
  validation_messages = validation_messages;
  matcher = new MyErrorStateMatcher();

  constructor(private fb: FormBuilder,
              private activeModal: NgbActiveModal,
              private modalService: NgbModal,
              private userService: UserService) { }

  ngOnInit() {
    this.buildForm();
  }

  private buildForm(): void {
    this.form = this.fb.group({
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.maxLength(255),
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$') //this is for the letters (both uppercase and lowercase) and numbers validation
      ])),
      confirmPassword: new FormControl('', Validators.required)
    }, {validator: this.checkPasswords});
  }

  checkPasswords(group: FormGroup) {
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : {areEqual: true}
  }

  isPasswordIncorrect() : boolean {
    if(this.checkIfFieldIsEmpty(this.form)) return true;

    let isPasswordIncorrect = false;
    this.form.errors === null ? null : isPasswordIncorrect = true;

    return isPasswordIncorrect;
  }

  checkIfPasswordCorrect(checkItem: string): boolean {
    let hasError = false;
    let array;

    if (checkItem === 'confirmPassword') {
      array = this.validation_messages.confirmPassword;
    }
    if (checkItem === 'password') {
      array = this.validation_messages.password;
    }
    for (let validation of array) {
      if (this.form.get(checkItem).hasError(validation.type)) {
        hasError = true;
      }
    }
    return hasError && this.form.get(checkItem).touched
  }

  checkIfFieldIsEmpty(form: FormGroup): boolean {
    let hasErrors = false;
    Object.keys(this.form.controls).forEach(key => {
      this.form.get(key).value === '' ? hasErrors = true : null;
    });
    return hasErrors;
  }

  getTokenStringFromUrl(): string {
    let currentUrl = window.location.href;
    return currentUrl.split('=')[1];
  }

  setNewPassword() {
    const newPasswordData = {
      token: this.getTokenStringFromUrl(),
      password: this.form.value.password.trim()
    };

    this.userService.setNewPassword(newPasswordData)
      .subscribe((response) => {
        if (response.status === 200) {
          const modalRef = this.modalService.open(SuccessModalComponent);
          modalRef.componentInstance.messageToShow = "New password has been successfully set!";
          this.closeModal();
        } else {
          this.closeModal();
          const modal = this.modalService.open(FailureModalComponent);
          modal.componentInstance.errorToShow = response;
        }
      }, error1 => {
        this.closeModal();
        const modal = this.modalService.open(FailureModalComponent);
        modal.componentInstance.errorToShow = error1;
      });
  }

  closeModal(){
    this.activeModal.close();
  }

}
