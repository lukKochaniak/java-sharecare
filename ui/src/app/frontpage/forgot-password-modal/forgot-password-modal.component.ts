import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NgbActiveModal, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {UserService} from "../../core/services/user.service";
import {SignInModalComponent} from "../sign-in-modal/sign-in-modal.component";
import {FailureModalComponent} from "../failure-modal/failure-modal.component";
import {SignInForm} from "../../model/user.model";
import {validation_messages} from "../common/validation_messages";
import {SuccessModalComponent} from "../success-modal/success-modal.component";

@Component({
  selector: 'app-forgot-password-modal',
  templateUrl: './forgot-password-modal.component.html',
  styleUrls: ['./forgot-password-modal.component.css']
})
export class ForgotPasswordModalComponent implements OnInit {
  form: FormGroup;
  validation_messages = validation_messages;

  constructor(private fb: FormBuilder,
              private activeModal: NgbActiveModal,
              private userService: UserService,
              private modalService: NgbModal) { }

  private buildForm(): void {
    this.form = this.fb.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])]
    });
  }

  ngOnInit() {
    this.buildForm();
  }

  checkIfEmailCorrect(checkItem: string): boolean {
    let hasError = false;
    let array;

    if (checkItem === 'email') {
      array = this.validation_messages.email;
    }
    for (let validation of array) {
      if (this.form.get(checkItem).hasError(validation.type)) {
        hasError = true;
      }
    }
    return hasError && this.form.get(checkItem).touched
  }

  backToSignIn(){
    const modalRef = this.modalService.open(SignInModalComponent);
    this.closeModal();
  }

  sendEmail(){

    const email = { email: this.form.value.email.trim()};

    this.userService.resetPassword(email)
      .subscribe((response) => {
        if (response.status === 200) {
          const modalRef = this.modalService.open(SuccessModalComponent);
          modalRef.componentInstance.messageToShow = "E-mail with link to reset password was sent!";
          this.closeModal();
        } else {
          this.closeModal();
          const modal = this.modalService.open(FailureModalComponent);
          modal.componentInstance.errorToShow = response;
        }
      }, error1 => {
        this.closeModal();
        const modal = this.modalService.open(FailureModalComponent);
        modal.componentInstance.errorToShow = "";
      });
  }

  private closeModal(){
    this.activeModal.close();
  }

}
