import {Component} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-failure-modal',
  templateUrl: './failure-modal.component.html',
  styleUrls: ['./failure-modal.component.css']
})
export class FailureModalComponent {
  errorToShow = '';

  constructor(private activeModal: NgbActiveModal) {
  }

  setErrorToShow(errorToShow: string) {
    this.errorToShow = errorToShow;
  }

  closeModal() {
    this.activeModal.close();
  }


}
