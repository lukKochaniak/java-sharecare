import {NgModule} from '@angular/core';

import {CommonModule} from "@angular/common";
import {MainNavComponent} from "./main-nav/main-nav.component";
import {PageFooterComponent} from "./page-footer/page-footer.component";
import {SignInModalComponent} from "./sign-in-modal/sign-in-modal.component";
import {ForgotPasswordModalComponent} from "./forgot-password-modal/forgot-password-modal.component";
import {FrontpageComponent} from "./frontpage.component";
import {FrontpageRoutingModule} from "./frontpage-routing.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgbActiveModal, NgbModalModule} from "@ng-bootstrap/ng-bootstrap";
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
  MatRippleModule
} from "@angular/material";
import {ComboDatepickerModule} from "ngx-combo-datepicker";
import { SetNewPasswordModalComponent } from './forgot-password-modal/set-new-password-modal/set-new-password-modal.component';
import { ModalStepperComponent } from './sign-up-modal/modal-stepper/modal-stepper.component';
import {ProgressBarModule} from "angular-progress-bar";
import {InternationalPhoneNumberModule} from "ngx-international-phone-number";
import { PageAboutComponent } from './page-about/page-about.component';

@NgModule({
  declarations: [
    MainNavComponent,
    PageFooterComponent,
    SignInModalComponent,
    ForgotPasswordModalComponent,
    FrontpageComponent,
    SetNewPasswordModalComponent,
    ModalStepperComponent,
    PageAboutComponent,
  ],
  entryComponents: [
    SignInModalComponent,
    ForgotPasswordModalComponent,
    SetNewPasswordModalComponent,
    ModalStepperComponent,
    SetNewPasswordModalComponent
  ],
  providers: [
    NgbActiveModal,
  ],
  bootstrap: [
    ModalStepperComponent
  ],
  imports: [
    CommonModule,
    FrontpageRoutingModule,
    ReactiveFormsModule,
    NgbModalModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    ComboDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    ProgressBarModule,
    InternationalPhoneNumberModule,
    FormsModule
  ]
})
export class FrontpageModule {
}
