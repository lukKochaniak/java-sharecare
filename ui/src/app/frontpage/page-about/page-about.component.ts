import { Component, OnInit } from '@angular/core';
import {ModalStepperComponent} from "../sign-up-modal/modal-stepper/modal-stepper.component";
import {SignInModalComponent} from "../sign-in-modal/sign-in-modal.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-page-about',
  templateUrl: './page-about.component.html',
  styleUrls: ['./page-about.component.css']
})
export class PageAboutComponent implements OnInit {

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }

  private openSignUpModal() {
    const modalRef = this.modalService.open(ModalStepperComponent);

  }

  private openSignInModal() {
    const modalRef = this.modalService.open(SignInModalComponent);
  }
}
