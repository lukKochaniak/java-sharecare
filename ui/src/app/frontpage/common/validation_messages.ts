export var validation_messages = {
  'firstName': [
    {type: 'required', message: 'First name is required!'}
  ],
  'lastName': [
    {type: 'required', message: 'Last name is required!'}
  ],
  'address': [
    {type: 'required', message: 'Required!'}
  ],
  'city': [
    {type: 'required', message: 'Required!'}
  ],
  'zipcode': [
    {type: 'required', message: 'Required!'}
  ],
  'phoneNumber': [
    {type: 'required', message: 'Required!'}
  ],
  'dateOfBirth': [
    {type: 'required', message: 'Required!'}
  ],
  'email': [
    {type: 'required', message: 'Email is required!'},
    {type: 'pattern', message: 'Enter a valid email!'}
  ],
  'confirmEmail': [
    {type: 'required', message: 'Confirm email is required!'},
    {type: 'areEqual', message: 'Email mismatch!'}
  ],
  'confirmPassword': [
    {type: 'required', message: 'Confirm password is required!'},
    {type: 'areEqual', message: 'Password mismatch!'}
  ],
  'password': [
    {type: 'required', message: 'Password is required!\n'},
    {type: 'minlength', message: 'Password must be at least 5 characters long!\n'},
    {type: 'maxlength', message: 'Password must be shorter than 255 characters!\n'},
    {type: 'pattern', message: 'Your password must contain at least one uppercase, one lowercase, and one number!\n'},
  ]
};
