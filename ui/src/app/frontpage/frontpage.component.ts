import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {NgbActiveModal, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {SuccessModalComponent} from "./success-modal/success-modal.component";
import {FailureModalComponent} from "./failure-modal/failure-modal.component";
import {SetNewPasswordModalComponent} from "./forgot-password-modal/set-new-password-modal/set-new-password-modal.component";

@Component({
  selector: 'app-frontpage',
  templateUrl: './frontpage.component.html',
  styleUrls: ['./frontpage.component.css']
})
export class FrontpageComponent implements OnInit {

  private title: string = 'PeerCare';


  constructor(private router: Router,
              private activeModal: NgbActiveModal,
              private modalService: NgbModal) {
  }

  ngOnInit() {
    this.resolveRouting();
  }

  resolveRouting() {
    if (this.router.url.startsWith('/success')) {
      if (this.router.url.includes('verify')) {
        this.openSuccessModal("Your account has been verified!");
      }
    }

    if (this.router.url.startsWith('/error')) {
      if (this.router.url.includes('verify')) {
        this.openFailureModal("We couldn't verify your account! Maybe activation link expired?");
      }
    }

    if(this.router.url.startsWith("/set")) {  //CHECK IF IT'S OK
      if(this.router.url.includes('password')) {
        this.openSetNewPasswordModal();
      }
    }
  }

  openSuccessModal(messageToShow: string) {
    const modalRef = this.modalService.open(SuccessModalComponent);
    modalRef.componentInstance.messageToShow = messageToShow;
  }

  openFailureModal(messageToShow: string) {
    const modalRef = this.modalService.open(FailureModalComponent);
    modalRef.componentInstance.setErrorToShow(messageToShow);
  }

  openSetNewPasswordModal() {
    const modalRef = this.modalService.open(SetNewPasswordModalComponent);
  }
}
