import {Component, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UserService} from '../../core/services/user.service';
import {SignInModalComponent} from '../sign-in-modal/sign-in-modal.component';
import {ModalStepperComponent} from "../sign-up-modal/modal-stepper/modal-stepper.component";

@Component({
  selector: 'main-nav',
  templateUrl: './main-nav.component.html'
})

export class MainNavComponent implements OnInit {

  private title: string = 'PeerCare';

  constructor(private userService: UserService,
              private modalService: NgbModal) {

  }

  ngOnInit(): void {
  }

  private openSignUpModal() {
    const modalRef = this.modalService.open(ModalStepperComponent);

  }

  private openSignInModal() {
    const modalRef = this.modalService.open(SignInModalComponent);
  }

}
