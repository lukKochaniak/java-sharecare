package com.sharecare.core.users.factory;


import com.sharecare.app.restapi.users.signup.UserAddRequest;
import com.sharecare.core.users.Role;
import com.sharecare.core.users.entity.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserFactoryTest {

    private UserFactory userFactory;

    @Mock
    private PasswordEncoder passwordEncoder;

    private long id;
    private String email;
    private String password;

    @Before
    public void setUp() throws Exception {
        this.id = 1L;
        this.email = "email";
        this.password = "password";
        this.userFactory = new UserFactory(passwordEncoder);
    }

    @After
    public void tearDown() throws Exception {
        this.id = 1L;
        this.userFactory = null;
        this.password = null;
        this.email = null;
    }

    @Test
    public void shouldReturnUserInCreateWithoutRoles() {
        //given
        String encryptedPassword = "hash";
        UserAddRequest userAddInput = UserAddRequest.builder()
                .email(email)
                .password(password)
                .role(Role.CAREGIVER)
                .build();
        when(passwordEncoder.encode(password))
                .thenReturn(encryptedPassword);

        //when
        User result = userFactory.create(userAddInput);

        //then
        verify(passwordEncoder)
                .encode(password);

        assertThat(result)
                .isNotNull();
        assertThat(result)
                .isNotNull();
        assertThat(result.getEmail())
                .isEqualTo(email);
        assertThat(result.getPassword())
                .isEqualTo(encryptedPassword);
        assertThat(result.getRole())
                .isNotNull()
                .isEqualTo(Role.CAREGIVER);
    }

    @Test
    public void shouldReturnUserInCreateWithRoles() {
        //given
        Role role = Role.CAREGIVER;
        UserAddRequest userAddInput = UserAddRequest.builder()
                .email(email)
                .password(password)
                .role(role)
                .build();
        String encryptedPassword = "hash";
        when(passwordEncoder.encode(password))
                .thenReturn(encryptedPassword);

        //when
        User result = userFactory.create(userAddInput);

        //then
        verify(passwordEncoder)
                .encode(password);

        assertThat(result)
                .isNotNull();
        assertThat(result)
                .isNotNull();
        assertThat(result.getEmail())
                .isEqualTo(email);
        assertThat(result.getPassword())
                .isEqualTo(encryptedPassword);
        assertThat(result.getRole())
                .isNotNull()
                .isEqualTo(role);
    }
}

