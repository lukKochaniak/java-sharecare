package com.sharecare.core.users.entity;

import com.sharecare.core.users.Role;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UserTest {

    private Long id;
    private String email;
    private String password;
    private Role role;

    @Before
    public void setUp() throws Exception {
        this.id = 1L;
        this.email = "email";
        this.password = "password";
        this.role = Role.CAREGIVER;
    }

    @After
    public void tearDown() throws Exception {
        this.id = null;
        this.email = null;
        this.password = null;
        this.role = null;
    }

    @Test
    public void shouldCreateUser() {
        //given

        //when
        User result = new User(email, password, role);

        //then
        assertThat(result)
                .isNotNull();
        assertThat(result)
                .isNotNull();
        assertThat(result.getEmail())
                .isEqualTo(email);
        assertThat(result.getPassword())
                .isEqualTo(password);
        assertThat(result.getRole())
                .isNotNull()
                .isEqualTo(role);
    }

    @Test
    public void shouldGrantRole() {
        //given
        Role role = Role.CAREGIVER;
        User user = new User(email, password, Role.CAREGIVER);

        //when
        user.setRole(role);

        //then
        assertThat(user)
                .isNotNull();
        assertThat(user.getRole())
                .isNotNull()
                .isEqualTo(role);
    }
}
