package com.sharecare.core.users.bo;

import com.sharecare.app.restapi.users.signup.UserAddRequest;
import com.sharecare.core.users.Role;
import com.sharecare.core.users.UserBO;
import com.sharecare.core.users.UserSnapshot;
import com.sharecare.core.users.entity.User;
import com.sharecare.core.users.repository.UserRepository;
import com.sharecare.core.users.validator.PasswordValidator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest() //classes = Application.class
public class UserBOTest {

    private static final String EMAIL = "email@email.com";
    private static final String PASSWORD = "password";
    private static final Role ROLE = Role.CAREGIVER;
    @Autowired
    private UserBO userBO;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordValidator passwordValidator;
    @Autowired
    private PasswordEncoder passwordEncoder;
    private UserSnapshot user;

    @Before
    public void setUp() {
        UserAddRequest userAddRequest = UserAddRequest.builder()
                .firstName("firstName")
                .lastName("lastName")
                .address("address")
                .city("city")
                .zipcode("zipcode")
                .dateOfBirth(new Date(2010,  10, 10))
                .phoneNumber("phoneNumber")
                .email(EMAIL)
                .password(PASSWORD)
                .role(ROLE)
                .build();
        user = userBO.addUser(userAddRequest);
    }

    @After
    public void tearDown() throws Exception {
        userRepository.deleteAll();
        user = null;
    }

    @Test
    public void shouldChangePassword() {
        //given
        String newPassword = "NEW_PASSWORD";
        //when
        userBO.changePassword(user.getId(), newPassword);
        //
        assertThat(userBO.isMatchingPassword(user.getEmail(), newPassword))
                .isTrue();
    }

    @Test
    public void shouldCheckPasswordAndReturnTrue() {
        //given
        //when
        boolean result = userBO.isMatchingPassword(user.getEmail(), PASSWORD);
        //then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldCheckPasswordAndReturnFalse() {
        //given
        //when
        boolean result = userBO.isMatchingPassword(user.getEmail(), "NOT_A_PASSWORD");
        //then
        assertThat(result).isFalse();
    }
}

