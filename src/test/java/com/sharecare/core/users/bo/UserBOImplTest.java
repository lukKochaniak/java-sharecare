package com.sharecare.core.users.bo;

import com.sharecare.app.restapi.users.signup.UserAddRequest;
import com.sharecare.core.users.Role;
import com.sharecare.core.users.UserSnapshot;
import com.sharecare.core.users.entity.User;
import com.sharecare.core.users.factory.UserFactory;
import com.sharecare.core.users.repository.UserRepository;
import com.sharecare.core.users.validator.PasswordValidator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserBOImplTest {

    private UserBOImpl userBOImpl;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserFactory userFactory;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private PasswordValidator passwordValidator;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    private long id;
    private String email;
    private String password;
    private Role role;
    private UserSnapshot userSnapshot;

    @Before
    public void setUp() throws Exception {
        this.userBOImpl = new UserBOImpl(userRepository,
                userFactory,
                passwordEncoder,
                passwordValidator,
                applicationEventPublisher);

        this.id = 1L;
        this.email = "user";
        this.password = "password";
        this.role = Role.CAREGIVER;
        this.userSnapshot = UserSnapshot.builder()
                .email("user")
                .password("password")
                .role(Role.CAREGIVER)
                .build();
    }

    @After
    public void tearDown() throws Exception {
        this.userBOImpl = null;
    }

    @Test
    public void shouldAddUserWithoutRoles() {
        //given
        User user = mock(User.class);
        UserAddRequest userAddRequest = UserAddRequest.builder()
                .email(email)
                .password(password)
                .role(Role.CAREGIVER)
                .build();
        when(userFactory.create(userAddRequest))
                .thenReturn(user);
        when(userRepository.save(user))
                .thenReturn(user);
        when(user.toSnapshot())
                .thenReturn(userSnapshot);


        //when
        UserSnapshot result = userBOImpl.addUser(userAddRequest);

        //then
        verify(userFactory)
                .create(userAddRequest);
        verify(userRepository)
                .save(user);
        assertThat(result)
                .isNotNull();
    }

    @Test
    public void shouldAddUserWithRoles() {
        //given
        User user = mock(User.class);
        UserAddRequest userAddRequest = UserAddRequest.builder()
                .email(email)
                .password(password)
                .role(role)
                .build();
        when(userFactory.create(userAddRequest))
                .thenReturn(user);
        when(userRepository.save(user))
                .thenReturn(user);
        when(user.toSnapshot())
                .thenReturn(userSnapshot);

        //when
        UserSnapshot result = userBOImpl.addUser(userAddRequest);

        //then
        verify(userFactory)
                .create(userAddRequest);
        verify(userRepository)
                .save(user);
        assertThat(result)
                .isNotNull();
    }

    @Test
    public void shouldGrantRoleToUser() {
        //given
        Role role = Role.CAREGIVER;
        User user = mock(User.class);

        when(userRepository.getOne(id))
                .thenReturn(user);

        //when
        userBOImpl.setRole(id, role);

        //then
        verify(userRepository)
                .getOne(id);
        verify(user)
                .setRole(role);
    }


    @Test
    public void shouldDelete() {
        //given

        //when
        userBOImpl.delete(1L);

        //then
        verify(userRepository)
                .deleteById(1L);
    }

    @Test
    public void shouldChangePassword() {
        //given
        String userName = "user";
        String newPassword = "newPassword";
        String newHash = "newHash";
        User user = mock(User.class);

        when(userRepository.getOne(id))
                .thenReturn(user);
        when(passwordEncoder.encode(newPassword))
                .thenReturn(newHash);

        //when
        userBOImpl.changePassword(id, newPassword);

        //then
        verify(user)
                .updatePassword(newHash);
    }

}
