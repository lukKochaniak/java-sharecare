package com.sharecare.core.notifications.entity;

import com.sharecare.core.notifications.NotificationType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class NotificationTest {

    private Long id;
    private long fromId;
    private long toId;
    private String title;
    private String message;
    private NotificationType type;
    private Date date;

    @Before
    public void setUp() {
        this.id = 1L;
        this.fromId = 1L;
        this.title = "title";
        this.message = "message";
        this.toId = 2L;
        this.type = NotificationType.ACCOUNT_ASSIGNMENT_OFFER;
        this.date = new Date();
    }

    @After
    public void tearDown() {
        this.id = null;
        this.title = null;
        this.message = null;
        this.type = null;
        this.date = null;
    }

    @Test
    public void shouldCreateNotification() {
        //given

        //when
        Notification result = new Notification(fromId, toId, title, message, date, type);

        //then
        assertThat(result)
                .isNotNull();
        assertThat(result.getNotificationId())
                .isNotNull();
        assertThat(result.getToUserId())
                .isNotNull();
        assertThat(result.getNotificationTitle())
                .isNotNull();
        assertThat(result.getNotificationMessage())
                .isNotNull();
        assertThat(result.getFromUserId())
                .isNotNull();
        assertThat(result.getNotificationType())
                .isNotNull();
        assertThat(result.getDateOfSending())
                .isNotNull();
        assertThat(result.isWasNotificationRead())
                .isFalse();
    }

    @Test
    public void shouldMarkAsRead() {
        //given

        //when
        Notification result = new Notification(fromId, toId, title, message, date, type);
        result.markAsRead();

        //then
        assertThat(result.isWasNotificationRead()).isTrue();
    }
}
