package com.sharecare.core.notifications.bo;

import com.sharecare.core.notifications.NotificationBO;
import com.sharecare.core.notifications.NotificationSnapshot;
import com.sharecare.core.notifications.NotificationType;
import com.sharecare.core.notifications.repository.NotificationRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest() //classes = Application.class
public class NotificationBOTest {

    private static final long fromId = 1L;
    private static final long toId = 2L;
    private static final String from = "from";
    private static final NotificationType type = NotificationType.ACCOUNT_ASSIGNMENT_OFFER;

    @Autowired
    private NotificationBO notificationBO;
    @Autowired
    private NotificationRepository notificationRepository;

    private NotificationSnapshot notification;

    @Before
    public void setUp() {
        notification = notificationBO.addNotification(fromId, toId, from, type);
    }

    @After
    public void tearDown() throws Exception {
        notificationRepository.deleteAll();
        notification = null;
    }

    @Test
    public void shouldFindExistingNotification() {
        //given
        //when
        NotificationSnapshot notificationSnapshot = notificationBO.findById(notification.getId());

        //then
        assertThat(notificationSnapshot).isNotNull();
        assertThat(notification.getId() == notificationSnapshot.getId()).isTrue();
    }

    @Test
    public void shouldNotFindNonExistingNotification() {
        //given
        long id = -1;
        //when
        NotificationSnapshot notificationSnapshot = notificationBO.findById(id);

        //then
        assertThat(notificationSnapshot).isNull();
    }

    @Test
    public void shouldMarkNotificationAsRead() {
        //given
        if (notification.isRead()) notification.setRead(false);

        //when
        notificationBO.markAsRead(notification.getId());

        //then
        assertThat(notificationBO.wasRead(notification.getId())).isTrue();
    }

    @Test
    public void shouldReturnListOfNotifications() {
        //given
        List<NotificationSnapshot> notifications;

        //when
        notifications = notificationBO.findAllForUser(fromId);

        //then
        assertThat(notifications.isEmpty()).isFalse();
    }

}
