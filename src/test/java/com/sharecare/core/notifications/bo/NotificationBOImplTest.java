package com.sharecare.core.notifications.bo;

import com.sharecare.core.notifications.NotificationSnapshot;
import com.sharecare.core.notifications.NotificationType;
import com.sharecare.core.notifications.entity.Notification;
import com.sharecare.core.notifications.factory.NotificationFactory;
import com.sharecare.core.notifications.repository.NotificationRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NotificationBOImplTest {

    private NotificationBOImpl notificationBOImpl;

    @Mock
    private NotificationRepository notificationRepository;

    @Mock
    private NotificationFactory notificationFactory;

    @Mock
    private SimpMessagingTemplate notificationTemplate;

    private String phoneNumber;
    private String fromUser;
    private NotificationType type;

    private NotificationSnapshot notificationSnapshot;

    @Before
    public void setUp() {
        this.notificationBOImpl = new NotificationBOImpl(notificationRepository, notificationFactory, notificationTemplate);

        this.phoneNumber = "phoneNumber";
        this.fromUser = "from";
        this.type = NotificationType.ACCOUNT_ASSIGNMENT_OFFER;

        this.notificationSnapshot = NotificationSnapshot.builder()
                .type(type)
                .build();
    }

    @After
    public void tearDown() {
        this.notificationBOImpl = null;
    }

    @Test
    public void shouldAddNotification() {
//        //given
//        Notification notification = mock(Notification.class);
//
//        when(notificationFactory.createNotification(phoneNumber, fromUser, type))
//                .thenReturn(notification);
//        when(notificationRepository.save(notification))
//                .thenReturn(notification);
//        when(notification.toSnapshot())
//                .thenReturn(notificationSnapshot);
//
//        //when
//        NotificationSnapshot result = notificationBOImpl.addNotification(phoneNumber, fromUser, type);
//
//        //then
//        verify(notificationFactory)
//                .create(phoneNumber, fromUser, type);
//        verify(notificationRepository)
//                .save(notification);
//        assertThat(result).isNotNull();
    }
}
