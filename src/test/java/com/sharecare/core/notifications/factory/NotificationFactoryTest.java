package com.sharecare.core.notifications.factory;

import com.sharecare.core.notifications.NotificationType;
import com.sharecare.core.notifications.entity.Notification;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class NotificationFactoryTest {

    private NotificationFactory notificationFactory;

    private long fromId;
    private long toId;
    private String from;
    private NotificationType type;

    @Before
    public void setUp() {
        this.fromId = 1L;
        this.toId = 2L;
        this.from = "from";
        this.type = NotificationType.ACCOUNT_ASSIGNMENT_OFFER;
        this.notificationFactory = new NotificationFactory();
    }

    @After
    public void tearDown() {
        this.from = null;
        this.type = null;
    }

    @Test
    public void shouldReturnNotificationInCreate() {
        //given

        //when
        Notification result = notificationFactory.createNotification(fromId, toId, from, type);

        //then
        assertThat(result)
                .isNotNull();
        assertThat(result.getNotificationId())
                .isNotNull();
        assertThat(result.getToUserId())
                .isNotNull();
        assertThat(result.getNotificationTitle())
                .isNotNull();
        assertThat(result.getNotificationMessage())
                .isNotNull();
        assertThat(result.getFromUserId())
                .isNotNull();
        assertThat(result.getNotificationType())
                .isNotNull();
        assertThat(result.getDateOfSending())
                .isNotNull();
    }
}
