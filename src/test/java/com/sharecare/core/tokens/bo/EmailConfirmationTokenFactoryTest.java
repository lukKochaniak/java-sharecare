package com.sharecare.core.tokens.bo;

import com.sharecare.core.tokens.EmailConfirmationTokenSnapshot;
import com.sharecare.core.tokens.TokenGeneratingService;
import com.sharecare.core.tokens.entity.EmailConfirmationToken;
import com.sharecare.core.tokens.entity.TokenCreationReason;
import com.sharecare.core.tokens.factory.EmailConfirmationTokenFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EmailConfirmationTokenFactoryTest {

    private EmailConfirmationTokenFactory emailConfirmationTokenFactory;
    @Mock
    private TokenGeneratingService tokenGeneratingService;

    private long registrationApplicationId;
    private LocalDateTime now;
    private String token;

    @Before
    public void setUp() throws Exception {
        this.registrationApplicationId = 1L;
        this.now = LocalDateTime.now();
        this.token = "abc";

        emailConfirmationTokenFactory = new EmailConfirmationTokenFactory(tokenGeneratingService);
    }

    @After
    public void tearDown() throws Exception {
        this.token = null;
        this.now = null;
        this.registrationApplicationId = 0L;
    }

    @Test
    public void shouldReturnInstanceOfEmailConfirmationToken() {
        //given
        when(tokenGeneratingService.generateToken())
                .thenReturn(token);

        //when
        EmailConfirmationToken result = emailConfirmationTokenFactory.create(TokenCreationReason.PASSWORD_RESET, registrationApplicationId);

        //then
        verify(tokenGeneratingService)
                .generateToken();

        assertThat(result)
                .isNotNull();
        EmailConfirmationTokenSnapshot emailConfirmationTokenSnapshot = result.toSnapshot();
        assertThat(emailConfirmationTokenSnapshot)
                .isNotNull();
        assertThat(emailConfirmationTokenSnapshot.getToken())
                .isEqualTo(token);
//        assertThat(emailConfirmationTokenSnapshot.getExpiresAt())
//                .isEqualTo(now.plusDays(2));
        assertThat(emailConfirmationTokenSnapshot.getReferenceUserId())
                .isEqualTo(registrationApplicationId);
    }

    @Test
    public void shouldCreate2ndInstanceOfEmailConfirmationToken() {
        //given
        when(tokenGeneratingService.generateToken())
                .thenReturn(token);

        EmailConfirmationToken emailConfirmationToken = emailConfirmationTokenFactory.create(TokenCreationReason.PASSWORD_RESET, registrationApplicationId);

        //when
        EmailConfirmationToken result = emailConfirmationTokenFactory.create(TokenCreationReason.PASSWORD_RESET, registrationApplicationId);

        //then
        verify(tokenGeneratingService, times(2))
                .generateToken();

        assertThat(result)
                .isNotNull()
                .isNotSameAs(emailConfirmationToken);
    }

}
