package com.sharecare.core.tokens.entity;

import com.sharecare.core.tokens.EmailConfirmationTokenSnapshot;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

public class EmailConfirmationTokenTest {

    private String token;
    private LocalDateTime expiresAt;
    private long registrationApplicationId;

    @Before
    public void setUp() throws Exception {
        this.token = "abc";
        this.expiresAt = LocalDateTime.now();
        this.registrationApplicationId = 1L;
    }

    @After
    public void tearDown() throws Exception {
        this.registrationApplicationId = 0L;
        this.expiresAt = null;
        this.token = null;
    }

    @Test
    public void shouldCreteEmailConfirmationToken() {
        //given

        //when
        EmailConfirmationToken result = new EmailConfirmationToken(token, expiresAt, TokenCreationReason.PASSWORD_RESET, registrationApplicationId);

        //then
        assertThat(result)
                .isNotNull();
        EmailConfirmationTokenSnapshot emailConfirmationTokenSnapshot = result.toSnapshot();
        assertThat(emailConfirmationTokenSnapshot)
                .isNotNull();
        assertThat(emailConfirmationTokenSnapshot.getToken())
                .isEqualTo(token);
        assertThat(emailConfirmationTokenSnapshot.getExpiresAt())
                .isEqualTo(expiresAt);
        assertThat(emailConfirmationTokenSnapshot.getReferenceUserId())
                .isEqualTo(registrationApplicationId);
    }

    @Test
    public void shouldReturnTrueInVerifyWhenTokenIsCorrectAndTokenIsNotExpired() {
        //given
        EmailConfirmationToken emailConfirmationToken = new EmailConfirmationToken(
                token, expiresAt, TokenCreationReason.PASSWORD_RESET, registrationApplicationId);

        //when
        boolean result = emailConfirmationToken.verify(token, expiresAt.minusSeconds(1));

        //then
        assertThat(result)
                .isTrue();
    }

    @Test
    public void shouldReturnFalseInVerifyWhenTokenDoesNotMatch() {
        //given
        EmailConfirmationToken emailConfirmationToken = new EmailConfirmationToken(
                token, expiresAt, TokenCreationReason.PASSWORD_RESET, registrationApplicationId);
        String incorrectToken = "xyz";

        //when
        boolean result = emailConfirmationToken.verify(incorrectToken, expiresAt.plusSeconds(1));

        //then
        assertThat(result)
                .isFalse();
    }

    @Test
    public void shouldReturnFalseInVerifyWhenTokenIsExpired() {
        //given
        EmailConfirmationToken emailConfirmationToken = new EmailConfirmationToken(
                token, expiresAt, TokenCreationReason.PASSWORD_RESET, registrationApplicationId);

        //when
        boolean result = emailConfirmationToken.verify(token, expiresAt.plusSeconds(1));

        //then
        assertThat(result)
                .isFalse();
    }

}
