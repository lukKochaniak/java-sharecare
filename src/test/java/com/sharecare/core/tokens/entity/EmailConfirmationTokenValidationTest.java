package com.sharecare.core.tokens.entity;

import com.sharecare.core.tokens.EmailConfirmationDomainTestConstants;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.time.LocalDateTime;
import java.util.Random;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class EmailConfirmationTokenValidationTest {

    private static Validator validator;

    @Before
    public void setUp() throws Exception {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void shouldNotLetToSetEmailConfirmationTokenWithLengthUnderMinDefined() {
        //given
        String tooShortText = randomString(EmailConfirmationDomainTestConstants.TOKEN_MIN_LENGTH - 1);

        EmailConfirmationToken emailConfirmationToken = new EmailConfirmationToken(tooShortText, LocalDateTime.now(), TokenCreationReason.PASSWORD_RESET, 1);

        //when
        Set<ConstraintViolation<EmailConfirmationToken>> violations = validator.validate(emailConfirmationToken);

        //then
        assertThat(violations.isEmpty()).isFalse();
        assertThat(violations.size()).isEqualTo(1);

        violations.forEach(violation -> {
            assertThat(violation.getMessage()).isEqualTo("size must be between 10 and 255");
        });
    }

    @Test
    public void shouldLetToSetEmailConfirmationTokenWithLengthAboveMinDefined() {
        //given
        String correctLengthText = randomString(EmailConfirmationDomainTestConstants.TOKEN_MIN_LENGTH + 1);

        EmailConfirmationToken emailConfirmationToken =
                new EmailConfirmationToken(correctLengthText, LocalDateTime.now(), TokenCreationReason.PASSWORD_RESET, 1);

        //when
        Set<ConstraintViolation<EmailConfirmationToken>> violations = validator.validate(emailConfirmationToken);

        //then
        assertThat(violations.isEmpty()).isTrue();
    }

    @Test
    public void shouldNotLetToSetEmailConfirmationTokenWithLengthAboveMaxDefined() {
        //given
        String tooLongText = randomString(EmailConfirmationDomainTestConstants.TOKEN_MAX_LENGTH + 1);

        EmailConfirmationToken emailConfirmationToken =
                new EmailConfirmationToken(tooLongText, LocalDateTime.now(), TokenCreationReason.PASSWORD_RESET, 1);

        //when
        Set<ConstraintViolation<EmailConfirmationToken>> violations = validator.validate(emailConfirmationToken);

        //then
        assertThat(violations.isEmpty()).isFalse();
        assertThat(violations.size()).isEqualTo(1);

        violations.forEach(violation -> {
            assertThat(violation.getMessage()).isEqualTo("size must be between 10 and 255");
        });
    }

    @Test
    public void shouldLetToSetEmailConfirmationTokenWithLengthBelowMaxDefined() {
        //given
        String correctLengthText = randomString(EmailConfirmationDomainTestConstants.TOKEN_MAX_LENGTH - 1);

        EmailConfirmationToken emailConfirmationToken =
                new EmailConfirmationToken(correctLengthText, LocalDateTime.now(), TokenCreationReason.PASSWORD_RESET, 1);

        //when
        Set<ConstraintViolation<EmailConfirmationToken>> violations = validator.validate(emailConfirmationToken);

        //then
        assertThat(violations.isEmpty()).isTrue();
    }

    @Test
    public void shouldNotLetToSetNullEmailConfirmationToken() {
        //given
        EmailConfirmationToken emailConfirmationToken =
                new EmailConfirmationToken(null, LocalDateTime.now(), TokenCreationReason.PASSWORD_RESET, 1);

        //when
        Set<ConstraintViolation<EmailConfirmationToken>> violations = validator.validate(emailConfirmationToken);

        //then
        assertThat(violations.isEmpty()).isFalse();
        assertThat(violations.size()).isEqualTo(1);

        violations.forEach(violation -> {
            assertThat(violation.getMessage()).isEqualTo("must not be null");
        });
    }

    public String randomString(final long length) {
        String ALLOWED_CHARS = "abcdefghijklmnopqrstuvwxyz";
        Random rand = new Random();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length; i++) {
            builder.append(ALLOWED_CHARS.charAt(rand.nextInt(ALLOWED_CHARS.length())));
        }
        return builder.toString();
    }

}
