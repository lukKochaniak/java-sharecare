package com.sharecare.core.tokens;

public final class EmailConfirmationDomainTestConstants {

    public static final int TOKEN_MIN_LENGTH = 10;
    public static final int TOKEN_MAX_LENGTH = 255;

    private EmailConfirmationDomainTestConstants() {
    }
}
