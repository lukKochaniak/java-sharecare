package com.sharecare.core.blockchain.bo;

import com.sharecare.app.restapi.blockchain.addContract.AddContractRequest;
import com.sharecare.core.blockchain.BlockchainWalletSnapshot;
import com.sharecare.core.blockchain.entity.BlockchainWallet;
import com.sharecare.core.blockchain.factory.BlockchainWalletFactory;
import com.sharecare.core.blockchain.repository.BlockchainWalletRepository;
import com.sharecare.core.users.Role;
import com.sharecare.core.users.UserSnapshot;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.web3j.crypto.CipherException;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthCoinbase;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthSendTransaction;

import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BlockchainWalletBOTest {

    private BlockchainWalletBOImpl blockchainWalletBOImpl;

    @Mock
    private BlockchainWalletRepository blockchainWalletRepository;

    @Mock
    private BlockchainWalletFactory blockchainWalletFactory;

    @Mock
    private Web3j web3j;

    private UserSnapshot userSnapshot;
    private BlockchainWalletSnapshot blockchainWalletSnapshot;

    @Before
    public void setUp() throws Exception {
        this.blockchainWalletBOImpl = new BlockchainWalletBOImpl(
                blockchainWalletRepository,
                blockchainWalletFactory,
                web3j);

        userSnapshot = UserSnapshot.builder()
                .id(1L)
                .email("email@email.com")
                .password("Passw0rd")
                .role(Role.CAREGIVER)
                .address("address")
                .firstName("firstName")
                .lastName("lastName")
                .city("city")
                .dateOfBirth(new Date(2010, 10, 10))
                .zipcode("zipcode")
                .phoneNumber("phoneNumber")
                .build();

        blockchainWalletSnapshot = BlockchainWalletSnapshot.builder()
                .userId(1L)
                .walletAddress("address")
                .build();
    }

    @After
    public void tearDown() throws Exception {
        this.blockchainWalletBOImpl = null;
    }

    @Test
    public void shouldCreateBlockchainWallet() throws IOException {
        //given
        BlockchainWallet blockchainWallet = mock(BlockchainWallet.class);

        when(blockchainWalletFactory.create(userSnapshot))
                .thenReturn(blockchainWallet);
        when(blockchainWalletRepository.save(blockchainWallet))
                .thenReturn(blockchainWallet);

        //when
        BlockchainWalletSnapshot result = blockchainWalletBOImpl.addWallet(userSnapshot);

        //then
        verify(blockchainWalletFactory)
                .create(userSnapshot);
        verify(blockchainWalletRepository)
                .save(blockchainWallet);
//        assertThat(result)
//                .isNotNull();
    }

    @Test
    public void shouldUpdateBlockchainWallet() {
        //given

        //when
        blockchainWalletBOImpl.updateWallet(blockchainWalletSnapshot);

        //then
        verify(blockchainWalletRepository)
                .findById(blockchainWalletSnapshot.getUserId());

    }

    @Test
    public void shouldDeleteBlockchainWallet() {
        //given

        //when
        blockchainWalletBOImpl.deleteWallet(1L);

        //then
        verify(blockchainWalletRepository)
                .deleteById(1L);

    }

//    @Test
//    public void shouldInitBlockchain() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException, CipherException, IOException {
//        //given
//        EthCoinbase coinbase = mock(EthCoinbase.class);
//        EthGetTransactionCount transactionCount = mock(EthGetTransactionCount.class);
//        Transaction transaction = mock(Transaction.class);
//        EthGetBalance balance = mock(EthGetBalance.class);
//        EthSendTransaction ethSendTransaction = mock(EthSendTransaction.class);
//        Request request = mock(Request.class);
//        Request requestTwo = mock(Request.class);
//        Request requestThree = mock(Request.class);
//        when(web3j.ethCoinbase())
//                .thenReturn(request);
//        when(request.send())
//                .thenReturn(coinbase);
//        when(coinbase.getAddress())
//                .thenReturn("address");
//        when(web3j.ethGetTransactionCount(coinbase.getAddress(), DefaultBlockParameterName.LATEST))
//                .thenReturn(requestTwo);
//        when(requestTwo.send())
//                .thenReturn(transactionCount);
//        when(web3j.ethSendTransaction(transaction))
//                .thenReturn(requestThree);
//        when(requestThree.send())
//                .thenReturn(ethSendTransaction);
//        //        when(Transaction.createEtherTransaction("address", transactionCount.getTransactionCount(), BigInteger.valueOf(20_000_000_000L), BigInteger.valueOf(21_000), applicationCredentials.getAddress(), BigInteger.valueOf(25_000_000_000_000_000L)))
////                .thenReturn(transaction);
//
//
//        //when
//        blockchainWalletBOImpl.initBlockchain();
//
//        //then
//        verify(web3j)
//                .ethCoinbase().send();
//    }

//    @Test
//    public void getBalanceTest() throws IOException {
//        //given
//        BlockchainWallet blockchainWallet = mock(BlockchainWallet.class);
//        EthGetBalance balance = new EthGetBalance();
//        Request request = mock(Request.class);
//        Optional<BlockchainWallet> blockchainWalletOptional = Optional.of(blockchainWallet);
//        when(blockchainWalletRepository.findById(1L))
//                .thenReturn(blockchainWalletOptional);
//        when(blockchainWalletOptional.get().getWalletAddress())
//                .thenReturn("address");
//        when(web3j.ethGetBalance("address", DefaultBlockParameterName.LATEST))
//                .thenReturn(request);
//        when(request.send())
//                .thenReturn(balance);
//        when(balance.getBalance())
//                .thenReturn(BigInteger.valueOf(1000));
//
//        //when
//        long result = blockchainWalletBOImpl.getBalance(1L);
//
//        //then
//        verify(blockchainWalletRepository)
//                .findById(1L);
//        assertThat(result)
//                .isNotNull();
//        assertThat(result)
//                .isEqualTo(1000);
//
//    }

    @Test
    public void performTransactionTest() throws Exception {
        //given
        AddContractRequest addContractRequest = AddContractRequest.builder()
                .fromId(1L)
                .toId(2L)
                .amount(1)
                .build();

        BlockchainWallet blockchainFromWallet = mock(BlockchainWallet.class);
        Optional<BlockchainWallet> blockchainFromWalletOptional = Optional.of(blockchainFromWallet);
        BlockchainWallet blockchainToWallet = mock(BlockchainWallet.class);
        Optional<BlockchainWallet> blockchainToWalletOptional = Optional.of(blockchainToWallet);
//        when(blockchainFromWalletOptional.get())
//                .thenReturn(blockchainFromWallet);
//        when(blockchainToWalletOptional.get())
//                .thenReturn(blockchainToWallet);

        //when
        boolean result = blockchainWalletBOImpl.performTransaction(addContractRequest);

        //then
        assertThat(result)
                .isEqualTo(false);

    }
}
