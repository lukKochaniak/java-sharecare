package com.sharecare.core.blockchain.factory;

import com.sharecare.core.blockchain.entity.BlockchainWallet;
import com.sharecare.core.users.Role;
import com.sharecare.core.users.UserSnapshot;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.admin.Admin;
import org.web3j.protocol.admin.methods.response.NewAccountIdentifier;
import org.web3j.protocol.admin.methods.response.PersonalUnlockAccount;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthCoinbase;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.http.HttpService;

import java.io.IOException;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BlockchainWalletFactoryTest {

    private BlockchainWalletFactory blockchainWalletFactory;

    private Admin blockchainAdmin;

    private UserSnapshot userSnapshot;

    @Mock
    private Web3j web3j;

    @Before
    public void setUp() throws Exception {
//        blockchainWalletFactory = new BlockchainWalletFactory(web3j);
//        userSnapshot = UserSnapshot.builder()
//                .id(1L)
//                .email("email@email.com")
//                .password("Passw0rd")
//                .role(Role.CAREGIVER)
//                .address("address")
//                .firstName("firstName")
//                .lastName("lastName")
//                .city("city")
//                .dateOfBirth(new Date(2010, 10, 10))
//                .zipcode("zipcode")
//                .phoneNumber("phoneNumber")
//                .build();
//
//        this.blockchainAdmin = Admin.build(new HttpService("http://192.168.99.100:8545"));

    }

    @After
    public void tearDown() throws Exception {
        userSnapshot = null;
    }

    @Test
    public void shouldCreateBlockchainWallet() throws IOException {
//        //given
//        NewAccountIdentifier newAccountIdentifier = mock(NewAccountIdentifier.class);
//        PersonalUnlockAccount personalUnlockAccount = mock(PersonalUnlockAccount.class);
//        EthCoinbase coinbase = mock(EthCoinbase.class);
//        EthGetTransactionCount transactionCount = mock(EthGetTransactionCount.class);
//        Transaction transaction = mock(Transaction.class);
//        when(blockchainAdmin.personalNewAccount(userSnapshot.getPassword()).send())
//                .thenReturn(newAccountIdentifier);
//        when(newAccountIdentifier.getAccountId())
//                .thenReturn("id");
//        when(blockchainAdmin.personalUnlockAccount("id", userSnapshot.getPassword()).send())
//                .thenReturn(personalUnlockAccount);
//        when(personalUnlockAccount.accountUnlocked())
//                .thenReturn(true);
//        when(web3j.ethCoinbase().send())
//                .thenReturn(coinbase);
//
//
//        //when
//        BlockchainWallet result = blockchainWalletFactory.create(userSnapshot);
//
//        //then
//        verify(passwordEncoder)
//                .encode(password);
//
//        assertThat(result)
//                .isNotNull();
//        assertThat(result)
//                .isNotNull();
//        assertThat(result.getEmail())
//                .isEqualTo(email);
//        assertThat(result.getPassword())
//                .isEqualTo(encryptedPassword);
//        assertThat(result.getRole())
//                .isNotNull()
//                .isEqualTo(Role.CAREGIVER);
    }
}
