package com.sharecare.core.blockchain.entity;

import com.sharecare.core.users.Role;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class BlockchainWalletTest {

    private Long id;
    private String address;


    @Before
    public void setUp() throws Exception {
        this.id = 1L;
        this.address = "address";
    }

    @After
    public void tearDown() throws Exception {
        this.id = null;
        this.address = null;
    }

    @Test
    public void shouldCreateBlockchainWallet(){
        //given

        //when
        BlockchainWallet result = new BlockchainWallet(id, address);

        //then
        assertThat(result)
                .isNotNull();
        assertThat(result.getWalletAddress())
                .isEqualTo("address");
        assertThat(result.getUserId())
                .isEqualTo(1L);

    }
}
