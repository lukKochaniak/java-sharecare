package com.sharecare.core.services.entity;

import com.sharecare.core.services.Status;
import com.sharecare.core.services.Title;
import com.sharecare.core.users.Role;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ServiceTest {

    private String description;
    private long requestedBy;
    private Status status;
    private String title;
    private long cost;
    private String address, city, zipcode;
    private long duration;

    @Before
    public void setUp() throws Exception {
        this.description = "description";
        this.requestedBy = 1L;
        this.status = Status.OPEN;
        this.title = "title";
        this.cost = 100;
        this.address = "address";
        this.city = "city";
        this.zipcode = "zipcode";
        this.duration = 45;
    }

    @After
    public void tearDown() throws Exception {
        this.description = null;
        this.status = null;
        this.title = null;
    }

    @Test
    public void shouldCreateService() {
        //given

        //when
        Service result = new Service(title, description, cost, address, city, zipcode, duration, requestedBy);

        //then
        assertThat(result)
                .isNotNull();
        assertThat(result.getDescription())
                .isEqualTo("description");
        assertThat(result.getAddress())
                .isEqualTo("address");
        assertThat(result.getRequestedby())
                .isEqualTo(1L);
        assertThat(result.getStatus())
                .isEqualTo(Status.OPEN);
        assertThat(result.getTitle())
                .isEqualTo("title");
    }

}
