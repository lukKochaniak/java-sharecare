package com.sharecare.core.services.factory;

import com.sharecare.app.restapi.services.addService.ServiceAddRequest;
import com.sharecare.core.services.Status;
import com.sharecare.core.services.Title;
import com.sharecare.core.services.entity.Service;
import com.sharecare.core.users.factory.UserFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ServiceFactoryTest {

    private ServiceFactory serviceFactory;

    @Before
    public void setUp() throws Exception {
        this.serviceFactory = new ServiceFactory();
    }

    @After
    public void tearDown() throws Exception {
        this.serviceFactory = null;
    }

    @Test
    public void shouldCreateService() {
        //given
        ServiceAddRequest serviceAddRequest = ServiceAddRequest.builder()
                .title("title")
                .description("description")
                .address("address")
                .zipcode("zipcode")
                .city("city")
                .cost(100)
                .requestedBy(1L)
                .build();

        //when
        Service service = serviceFactory.create(serviceAddRequest);

        //then
        assertThat(service)
                .isNotNull();
        assertThat(service.getDescription())
                .isEqualTo("description");
        assertThat(service.getAddress())
                .isEqualTo("address");
        assertThat(service.getRequestedby())
                .isEqualTo(1L);
        assertThat(service.getStatus())
                .isEqualTo(Status.OPEN);
        assertThat(service.getTitle())
                .isEqualTo("title");

    }
}
