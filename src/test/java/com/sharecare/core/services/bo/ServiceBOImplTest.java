package com.sharecare.core.services.bo;

import com.sharecare.app.restapi.services.addService.ServiceAddRequest;
import com.sharecare.core.services.ServiceSnapshot;
import com.sharecare.core.services.Status;
import com.sharecare.core.services.Title;
import com.sharecare.core.services.entity.Service;
import com.sharecare.core.services.factory.ServiceFactory;
import com.sharecare.core.services.repository.ServiceRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ServiceBOImplTest {

    private ServiceBOImpl serviceBOImpl;

    @Mock
    private ServiceRepository serviceRepository;

    @Mock
    private ServiceFactory serviceFactory;

    private String description;
    private String location;
    private long requestedBy;
    private Status status;
    private String title;
    private ServiceSnapshot serviceSnapshot;
    private ServiceAddRequest serviceAddRequest;
    private String address, city, zipcode;
    private long duration;
    private long cost;


    @Before
    public void setUp() throws Exception {
        this.serviceBOImpl = new ServiceBOImpl(serviceRepository, serviceFactory);
        this.description = "description";
        this.location = "location";
        this.requestedBy = 1L;
        this.status = Status.OPEN;
        this.title = "title";
        this.address = "address";
        this.city = "city";
        this.zipcode = "zipcode";
        this.cost = 100L;
        this.serviceSnapshot = ServiceSnapshot.builder().build();
        serviceAddRequest = ServiceAddRequest.builder()
                .description(description)
                .address(location)
                .requestedBy(requestedBy)
                .title(title)
                .build();
    }

    @After
    public void tearDown() throws Exception {
        this.serviceBOImpl = null;
        this.description = null;
        this.location = null;
        this.status = null;
        this.title = null;
    }

    @Test
    public void shouldAddService(){
        //given
        Service service = mock(Service.class);
        when(serviceFactory.create(serviceAddRequest))
                .thenReturn(service);
        when(serviceRepository.save(service))
                .thenReturn(service);
        when(service.getId())
                .thenReturn(1L);
        when(service.toSnapshot())
                .thenReturn(serviceSnapshot);

        //when
        ServiceSnapshot result = serviceBOImpl.addService(serviceAddRequest);

        //then
        assertThat(result)
                .isNotNull();
    }

    @Test
    public void shouldGetServices(){
        //given
        List<Service> services = new ArrayList<>();
        services.add(new Service(title, description, cost, address, city, zipcode, duration, requestedBy));
        when(serviceRepository.findAll())
                .thenReturn(services);

        //when
        List<ServiceSnapshot> result = serviceBOImpl.getAllServices();

        //then
        assertThat(result)
                .isNotNull();
        assertThat(result.size())
                .isEqualTo(1);
    }

    @Test
    public void shouldGetService(){
        //given
        when(serviceRepository.findById(1L))
                .thenReturn(Optional.of(new Service(title, description, cost, address, city, zipcode, duration, requestedBy)));

        //when
        ServiceSnapshot result = serviceBOImpl.getService(1L);

        //then
        assertThat(result)
                .isNotNull();
    }

    @Test
    public void shouldUpdateService(){
        //given
//        Service service = mock(Service.class);
//        when(serviceRepository.findById(1L))
//                .thenReturn(Optional.of(service));
//        when(serviceRepository.save(service))
//                .thenReturn(service);
//        when(service.toSnapshot())
//                .thenReturn(serviceSnapshot);

        //when
        ServiceSnapshot result = serviceBOImpl.updateService(serviceSnapshot);

        //then
        assertThat(result)
                .isNotNull();
    }

    @Test
    public void shouldDeleteService(){
        //given

        //when
        serviceRepository.deleteById(1L);

        //then
    }
}
