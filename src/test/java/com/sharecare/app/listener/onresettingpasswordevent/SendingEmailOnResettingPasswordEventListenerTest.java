package com.sharecare.app.listener.onresettingpasswordevent;

import com.sharecare.app.email.user.passwordreset.PasswordResetEmailSendingService;
import com.sharecare.app.listener.onresettingpasswordapplicationevent.SendingEmailOnResettingPasswordEventListener;
import com.sharecare.core.users.Role;
import com.sharecare.core.users.UserSnapshot;
import com.sharecare.core.users.event.ResetPasswordEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class SendingEmailOnResettingPasswordEventListenerTest {

    private SendingEmailOnResettingPasswordEventListener sendingEmailOnResettingPasswordEventListener;
    private UserSnapshot userSnapshot;

    @Mock
    private PasswordResetEmailSendingService passwordResetEmailSendingService;

    @Before
    public void setUp() {
        sendingEmailOnResettingPasswordEventListener = new SendingEmailOnResettingPasswordEventListener(passwordResetEmailSendingService);

        userSnapshot = UserSnapshot.builder()
                .firstName("firstName")
                .lastName("lastName")
                .email("email")
                .password("password")
                .address("address")
                .dateOfBirth(new Date(2010, 10, 10))
                .city("city")
                .zipcode("zipcode")
                .role(Role.CAREGIVER)
                .build();
    }

    @After
    public void tearDown() throws Exception {
        sendingEmailOnResettingPasswordEventListener = null;
        userSnapshot = null;
    }

    @Test
    public void shouldCallSendEmailOnApplicationEvent() {
        //given
        ResetPasswordEvent event = new ResetPasswordEvent(this, userSnapshot);

        //when
        sendingEmailOnResettingPasswordEventListener.onApplicationEvent(event);

        //then
        verify(passwordResetEmailSendingService)
                .sendEmail(userSnapshot);
    }
}
