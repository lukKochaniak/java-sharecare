package com.sharecare.app.listener.onsignupevent;

import com.sharecare.app.email.user.activation.EmailConfirmationEmailSendingService;
import com.sharecare.core.users.Role;
import com.sharecare.core.users.UserSnapshot;
import com.sharecare.core.users.event.SignupEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class EmailConfirmationSendingEmailOnSignupEventListenerTest {

    private EmailConfirmationSendingEmailOnSignupEventListener emailConfirmationSendingEmailOnSignupEventListener;
    private UserSnapshot userSnapshot;
    @Mock
    private EmailConfirmationEmailSendingService emailConfirmationEmailSendingService;

    @Before
    public void setUp() throws Exception {
        emailConfirmationSendingEmailOnSignupEventListener =
                new EmailConfirmationSendingEmailOnSignupEventListener(emailConfirmationEmailSendingService);
        userSnapshot = UserSnapshot.builder()
                .firstName("firstName")
                .lastName("lastName")
                .email("email")
                .password("password")
                .address("address")
                .dateOfBirth(new Date(2010, 10, 10))
                .city("city")
                .zipcode("zipcode")
                .role(Role.CAREGIVER)
                .build();

    }

    @After
    public void tearDown() throws Exception {
        emailConfirmationSendingEmailOnSignupEventListener = null;
        userSnapshot = null;
    }

    @Test
    public void shouldCallSendEmailOnApplicationEvent() {
        //given
        SignupEvent signupEvent = new SignupEvent(this, userSnapshot);

        //when
        emailConfirmationSendingEmailOnSignupEventListener.onApplicationEvent(signupEvent);

        //then
        verify(emailConfirmationEmailSendingService)
                .sendEmail(userSnapshot);
    }
}
