package com.sharecare.app.restapi.users;

import com.sharecare.app.restapi.users.profileupdate.PasswordUpdateRequest;
import com.sharecare.app.restapi.users.profileupdate.PasswordUpdateRequestValidator;
import com.sharecare.core.users.UserBO;
import com.sharecare.core.users.UserSnapshot;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.validation.Errors;

import static org.mockito.Mockito.*;

public class PasswordUpdateRequestValidatorTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private UserBO userBO;

    @Mock
    private Errors errors;

    @InjectMocks
    private PasswordUpdateRequestValidator validator;

    private PasswordUpdateRequest request;

    private UserSnapshot userSnapshot;

    @Before
    public void setUp() {
        request = new PasswordUpdateRequest();
        request.setEmail("email@email.com");
        request.setCurrentPassword("oldPassword");
        request.setNewPassword("newPassword");

        userSnapshot = UserSnapshot.builder()
                .email("email@email.com")
                .password("oldPassword")
                .build();
    }

    @Test
    public void shouldPassValidation() {
        //given
        when(userBO.findByEmail(request.getEmail()))
                .thenReturn(userSnapshot);
        when(userBO.isMatchingPassword(userSnapshot.getEmail(), userSnapshot.getPassword()))
                .thenReturn(true);

        //when
        validator.customValidation(request, errors);

        //then
        verifyZeroInteractions(errors);
    }

    @Test
    public void shouldFailIfEmailDoesNotExist() {
        //given
        when(userBO.findByEmail(request.getEmail()))
                .thenReturn(null);
        when(userBO.isMatchingPassword(userSnapshot.getEmail(), userSnapshot.getPassword()))
                .thenReturn(true);

        //when
        validator.customValidation(request, errors);

        //then
        verify(errors)
                .rejectValue("email", "userWithSpecifiedEmailDoesNotExist", "Email doesn't exist");
    }

    @Test
    public void shouldFailIfPasswordNotCorrect() {
        //given
        when(userBO.findByEmail(request.getEmail()))
                .thenReturn(userSnapshot);
        when(userBO.isMatchingPassword(userSnapshot.getEmail(), userSnapshot.getPassword()))
                .thenReturn(false);
        //when
        validator.customValidation(request, errors);

        //then
        verify(errors)
                .rejectValue("password", "passwordIsNotCorrect", "Wrong password");
    }
}
