package com.sharecare.app.restapi.users;

import com.sharecare.app.restapi.users.passwordreset.SetNewPasswordRequest;
import com.sharecare.app.restapi.users.passwordreset.SetNewPasswordRequestValidator;
import com.sharecare.core.tokens.EmailConfirmationTokenBO;
import com.sharecare.core.tokens.EmailConfirmationTokenSnapshot;
import com.sharecare.core.tokens.entity.EmailConfirmationToken;
import com.sharecare.core.tokens.entity.TokenCreationReason;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.validation.Errors;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.Mockito.*;

public class SetNewPasswordRequestValidatorTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private EmailConfirmationTokenBO emailConfirmationTokenBO;

    @Mock
    private Errors errors;

    @InjectMocks
    private SetNewPasswordRequestValidator validator;

    private SetNewPasswordRequest request;

    private Optional<EmailConfirmationTokenSnapshot> optionalSnapshot;

    @Before
    public void setUp() {
        request = new SetNewPasswordRequest();
        request.setPassword("password");
        request.setToken("tokens");

        String token = request.getToken();
        LocalDateTime date = LocalDateTime.now();
        TokenCreationReason reason = TokenCreationReason.PASSWORD_RESET;
        long referenceUserId = 1L;

        EmailConfirmationToken emailConfirmationToken = new EmailConfirmationToken(token, date, reason, referenceUserId);
        optionalSnapshot = Optional.of(emailConfirmationToken.toSnapshot());
    }

    @Test
    public void shouldPassValidation() {
        //given
        when(emailConfirmationTokenBO.findById(request.getToken()))
                .thenReturn(optionalSnapshot);

        //when
        validator.customValidation(request, errors);

        //then
        verifyZeroInteractions(errors);
    }

    @Test
    public void shouldFailIfGivenTokenIsInvalid() {
        when(emailConfirmationTokenBO.findById(request.getToken()))
                .thenReturn(Optional.empty());

        //when
        validator.customValidation(request, errors);

        //then
        verify(errors)
                .rejectValue("tokens", "noResettingPasswordRequestWithSuchToken", "There was no such request!");
    }
}
