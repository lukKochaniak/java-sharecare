package com.sharecare.app.restapi.users;

import com.sharecare.app.restapi.users.signup.UserAddRequest;
import com.sharecare.app.restapi.users.signup.UserAddRequestValidator;
import com.sharecare.core.users.Role;
import com.sharecare.core.users.UserBO;
import com.sharecare.core.users.UserSnapshot;
import com.sharecare.core.users.entity.User;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.validation.Errors;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class UserAddRequestValidatorTest {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private UserBO userBO;

    @Mock
    private Errors errors;

    @InjectMocks
    private UserAddRequestValidator validator;

    private UserAddRequest request;

    private UserSnapshot userSnapshot;

    @Before
    public void setUp() {
        request = UserAddRequest.builder().build();
        request.setEmail("email@email.com");
        request.setPassword("password");

        userSnapshot = UserSnapshot.builder()
                .firstName("firstName")
                .lastName("lastName")
                .address("address")
                .email("email@email.com")
                .password("password")
                .role(Role.CAREGIVER)
                .build();
    }

    @Test
    public void shouldPassValidation() {
        //given
        when(userBO.findByEmail(request.getEmail()))
                .thenReturn(null);

        //when
        validator.customValidation(request, errors);

        //then
        verifyZeroInteractions(errors);
    }

    @Test
    public void shouldFailIfEmailDoesNotExist() {
        //given
        when(userBO.findByEmail(request.getEmail()))
                .thenReturn(userSnapshot);

        //when
        validator.customValidation(request, errors);

        //then
        verify(errors)
                .rejectValue("email", "userWithSpecifiedEmailAlreadyExists", "Email is already used");
        verifyNoMoreInteractions(errors);
    }

}
