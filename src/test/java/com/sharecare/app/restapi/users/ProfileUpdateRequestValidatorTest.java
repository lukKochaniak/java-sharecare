package com.sharecare.app.restapi.users;

import com.sharecare.app.restapi.users.profileupdate.ProfileUpdateRequest;
import com.sharecare.app.restapi.users.profileupdate.ProfileUpdateRequestValidator;
import com.sharecare.core.users.Role;
import com.sharecare.core.users.UserBO;
import com.sharecare.core.users.UserSnapshot;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.validation.Errors;

import java.util.Date;

import static org.mockito.Mockito.*;


public class ProfileUpdateRequestValidatorTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private UserBO userBO;

    @Mock
    private Errors errors;

    @InjectMocks
    private ProfileUpdateRequestValidator validator;

    private ProfileUpdateRequest request;

    private UserSnapshot userSnapshot;

    @Before
    public void setUp() {
        request = new ProfileUpdateRequest();
        request.setEmail("email@email.com");
        request.setFirstName("firstName");
        request.setLastName("lastName");
        request.setAddress("address");
        request.setCity("city");
        request.setZipcode("123");
        request.setDateOfBirth(new Date(2017, 01, 01));

        userSnapshot = UserSnapshot.builder()
                .email("email@email.com")
                .build();
    }

    @Test
    public void shouldPassValidation() {
        //given
        when(userBO.findByEmail(request.getEmail()))
                .thenReturn(userSnapshot);
        when(userBO.isUserVerified(userSnapshot.getEmail()))
                .thenReturn(true);

        //when
        validator.customValidation(request, errors);

        //then
        verifyZeroInteractions(errors);
    }

    @Test
    public void shouldFailIfEmailDoesNotExist() {
        //given
        when(userBO.findByEmail(request.getEmail()))
                .thenReturn(null);

        //when
        validator.customValidation(request, errors);

        //then
        verify(errors)
                .rejectValue("email", "userWithSpecifiedEmailDoesNotExist", "Email doesn't exist");
    }

    @Test
    public void shouldFailIfUserNotVerified() {
        //given
        when(userBO.findByEmail(request.getEmail()))
                .thenReturn(userSnapshot);
        when(userBO.isUserVerified(userSnapshot.getEmail()))
                .thenReturn(false);

        //when
        validator.customValidation(request, errors);

        //then
        verify(errors)
                .rejectValue("verify", "userIsNotVerified", "User is not verified!");
    }

}
