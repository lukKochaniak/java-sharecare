package com.sharecare.app.restapi.users;

import com.sharecare.app.restapi.users.login.LoginPayloadRequest;
import com.sharecare.app.restapi.users.login.LoginPayloadRequestValidator;
import com.sharecare.core.users.Role;
import com.sharecare.core.users.UserBO;
import com.sharecare.core.users.UserSnapshot;
import com.sharecare.core.users.entity.User;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.validation.Errors;

import static org.mockito.Mockito.*;

public class LoginPayloadRequestValidatorTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private UserBO userBO;

    @Mock
    private Errors errors;

    @InjectMocks
    private LoginPayloadRequestValidator validator;

    private LoginPayloadRequest request;

    private UserSnapshot userSnapshot;

    @Before
    public void setUp() {
        request = new LoginPayloadRequest();
        request.setEmail("email@email.com");
        request.setPassword("password");

        userSnapshot = UserSnapshot.builder()
                .firstName("firstName")
                .lastName("lastName")
                .address("address")
                .email("email@email.com")
                .password("password")
                .role(Role.CAREGIVER)
                .build();
    }

    @Test
    public void shouldPassValidation() {
        //given
        when(userBO.findByEmail(request.getEmail()))
                .thenReturn(userSnapshot);
        when(userBO.isMatchingPassword(userSnapshot.getEmail(), userSnapshot.getPassword()))
                .thenReturn(true);
        when(userBO.isUserVerified(userSnapshot.getEmail()))
                .thenReturn(true);

        //when
        validator.customValidation(request, errors);

        //then
        verifyZeroInteractions(errors);
    }

    @Test
    public void shouldFailIfEmailDoesNotExist() {
        //given
        when(userBO.findByEmail(request.getEmail()))
                .thenReturn(null);
        when(userBO.isMatchingPassword(userSnapshot.getEmail(), userSnapshot.getPassword()))
                .thenReturn(true);

        //when
        validator.customValidation(request, errors);

        //then
        verify(errors)
                .rejectValue("email", "userWithSpecifiedEmailDoesNotExist", "Email doesn't exist");
    }

    @Test
    public void shouldFailIfPasswordNotCorrect() {
        //given
        when(userBO.findByEmail(request.getEmail()))
                .thenReturn(userSnapshot);
        when(userBO.isMatchingPassword(userSnapshot.getEmail(), userSnapshot.getPassword()))
                .thenReturn(false);
        //when
        validator.customValidation(request, errors);

        //then
        verify(errors)
                .rejectValue("password", "passwordIsNotCorrect", "Wrong password");
    }

    @Test
    public void shouldFailIfUserNotVerified() {
        //given
        when(userBO.findByEmail(request.getEmail()))
                .thenReturn(userSnapshot);
        when(userBO.isMatchingPassword(userSnapshot.getEmail(), userSnapshot.getPassword()))
                .thenReturn(true);
        when(userBO.isUserVerified(userSnapshot.getEmail()))
                .thenReturn(false);

        //when
        validator.customValidation(request, errors);

        //then
        verify(errors)
                .rejectValue("email", "userIsNotVerified", "User is not verified!");
    }
}

