package com.sharecare.app.restapi.users;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
/*
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShareCareApplicationTest.class)
@ActiveProfiles("sandbox")
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private UserBO userBO;
    @Autowired
    private UserRepository userRepository;

    private UserAddInput userAddInput;

    @Before
    public void setUp() {
        userAddInput = UserAddInput.builder()
                .firstName("firstName")
                .lastName("lastName")
                .address("address")
                .email("email")
                .password("password")
                .role(Role.CAREGIVER)
                .build();
    }

    @After
    public void tearDown() {
        userRepository.deleteAll();
    }
    @Test
    public void shouldAddUser() throws Exception {
    }

    @Test
    public void shouldAddUser() throws Exception {
        //given
        MockHttpServletRequestBuilder requestBuilder = post("/api/users/add")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(generateRequest("email", "password", "firstName", "lastName", "address", "22", "CAREGIVER"))
                .with(csrf());

        //when
        ResultActions result = mockMvc.perform(requestBuilder);

        //then
        result.andExpect(status().isOk());
        assertThat(userBO.findByEmail("email"))
                .isNotNull();
    }


    @Test
    public void shouldLoginUser() throws Exception {
        //given
        userBO.addUser(userAddInput);
        MockHttpServletRequestBuilder requestBuilder = post("/api/users/add")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(generateRequest("email", "password"))
                .with(csrf());


        //when
        ResultActions result = mockMvc.perform(requestBuilder);

        //then
        result.andExpect(status().is(200));
    }


    private String generateRequest(final String... strings) throws JsonProcessingException {
        Map<String, Object> map = new HashMap<>();
        for (String string : strings) {
            map.put(string, "string");
        }

        return objectMapper.writeValueAsString(map);
    }

}
*/
