package com.sharecare.core.blockchain.entity;

import com.sharecare.core.blockchain.BlockchainWalletSnapshot;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
public class BlockchainWallet {

    @Id
    private long userId;

    @NotNull
    private String walletAddress;

    protected BlockchainWallet(){

    }

    public BlockchainWallet(long userId, @NotNull String walletAddress) {
        this.userId = userId;
        this.walletAddress = walletAddress;
    }

    public BlockchainWalletSnapshot toSnapshot(){
        return BlockchainWalletSnapshot.builder()
                .userId(userId)
                .walletAddress(walletAddress)
                .build();

    }
}
