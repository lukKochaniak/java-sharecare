package com.sharecare.core.blockchain.bo;

import com.sharecare.app.restapi.blockchain.addContract.AddContractRequest;
import com.sharecare.core.blockchain.BlockchainWalletBO;
import com.sharecare.core.blockchain.BlockchainWalletConstants;
import com.sharecare.core.blockchain.BlockchainWalletSnapshot;
import com.sharecare.core.blockchain.entity.BlockchainWallet;
import com.sharecare.core.blockchain.factory.BlockchainWalletFactory;
import com.sharecare.core.blockchain.repository.BlockchainWalletRepository;
import com.sharecare.core.users.UserSnapshot;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.*;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Optional;

@Service
@Slf4j
public class BlockchainWalletBOImpl implements BlockchainWalletBO {

    private final BlockchainWalletRepository blockchainWalletRepository;
    private final BlockchainWalletFactory blockchainWalletFactory;
    private final Web3j web3j;

    public BlockchainWalletBOImpl(final BlockchainWalletRepository blockchainWalletRepository,
                                  final BlockchainWalletFactory blockchainWalletFactory,
                                  final Web3j web3j) {
        this.blockchainWalletRepository = blockchainWalletRepository;
        this.blockchainWalletFactory = blockchainWalletFactory;
        this.web3j = web3j;
    }

    @Override
    public BlockchainWalletSnapshot addWallet(final UserSnapshot userSnapshot) throws IOException {
        BlockchainWallet blockchainWallet = blockchainWalletFactory.create(userSnapshot);

        BlockchainWallet blockchainWalletSaved = blockchainWalletRepository.save(blockchainWallet);

        return blockchainWalletSaved == null ? null : blockchainWalletSaved.toSnapshot();
    }

    @Override
    public long getBalance(final long userId) throws IOException {
        Optional<BlockchainWallet> blockchainWalletSavedOptional = blockchainWalletRepository.findById(userId);

        if (blockchainWalletSavedOptional.isPresent()) {
            String walletAddress = blockchainWalletSavedOptional.get().getWalletAddress();
            EthGetBalance balance = web3j.ethGetBalance(walletAddress, DefaultBlockParameterName.LATEST).send();
            return balance.getBalance().longValue();
        }

        return BlockchainWalletConstants.BALANCE_NOT_FOUND;
    }

    @Override
    public boolean performTransaction(long toId, long amount, long fromId) throws Exception {
        Optional<BlockchainWallet> fromBlockchainWalletSavedOptional = blockchainWalletRepository.findById(fromId);
        Optional<BlockchainWallet> toBlockchainWalletSavedOptional = blockchainWalletRepository.findById(toId);

        if (fromBlockchainWalletSavedOptional.isPresent() && toBlockchainWalletSavedOptional.isPresent()) {
            BlockchainWallet fromBlockchainWallet = fromBlockchainWalletSavedOptional.get();
            BlockchainWallet toBlockchainWallet = toBlockchainWalletSavedOptional.get();

            EthGetTransactionCount transactionCount = web3j.ethGetTransactionCount(fromBlockchainWallet.getWalletAddress(), DefaultBlockParameterName.LATEST).send();
            Transaction transaction = Transaction.createEtherTransaction(fromBlockchainWallet.getWalletAddress(), transactionCount.getTransactionCount(),
                    BlockchainWalletConstants.GAS_PRICE, BlockchainWalletConstants.GAS_LIMIT, toBlockchainWallet.getWalletAddress(), BigInteger.valueOf(amount));
            EthSendTransaction response = web3j.ethSendTransaction(transaction).send();
            if (response.getError() != null) {
                log.error("Transaction error: {}", response.getError().getMessage());
                return false;
            }
            log.info("Transaction: {}", response.getResult());
            EthGetTransactionReceipt receipt = web3j.ethGetTransactionReceipt(response.getTransactionHash()).send();
            if (receipt.getTransactionReceipt().isPresent()) {
                TransactionReceipt r = receipt.getTransactionReceipt().get();
                log.info("Tx receipt: from={}, to={}, gas={}, cumulativeGas={}", r.getFrom(), r.getTo(), r.getGasUsed().intValue(), r.getCumulativeGasUsed().intValue());
            }
            return true;

        }
        return false;
    }

    @Override
    public boolean performTransaction(AddContractRequest addContractRequest) throws Exception {
        return performTransaction(addContractRequest.getToId(), addContractRequest.getAmount(), addContractRequest.getFromId());
    }

    @Override
    public BlockchainWalletSnapshot updateWallet(final BlockchainWalletSnapshot blockchainWalletSnapshot) {
        Optional<BlockchainWallet> blockchainWalletOptional = blockchainWalletRepository.findById(blockchainWalletSnapshot.getUserId());
        if (blockchainWalletOptional.isPresent()) {
            BlockchainWallet blockchainWallet = blockchainWalletOptional.get();
            BeanUtils.copyProperties(blockchainWalletSnapshot, blockchainWallet);
            BlockchainWallet blockchainWalletSaved = blockchainWalletRepository.save(blockchainWallet);
            return blockchainWalletSaved == null ? null : blockchainWalletSaved.toSnapshot();
        }

        return null;
    }

    @Override
    public void deleteWallet(final long walletId) {
        blockchainWalletRepository.deleteById(walletId);
        log.info("Deleted blockchain wallet with id <{}>", walletId);
    }
}
