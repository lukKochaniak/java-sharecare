package com.sharecare.core.blockchain.factory;

import com.sharecare.core.blockchain.BlockchainWalletConstants;
import com.sharecare.core.blockchain.entity.BlockchainWallet;
import com.sharecare.core.users.UserSnapshot;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.admin.Admin;
import org.web3j.protocol.admin.methods.response.NewAccountIdentifier;
import org.web3j.protocol.admin.methods.response.PersonalUnlockAccount;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthCoinbase;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.http.HttpService;

import java.io.IOException;
import java.math.BigInteger;

@Component
@Slf4j
public class BlockchainWalletFactory {

    private final Admin blockchainAdmin;
    private final Web3j web3j;

    public BlockchainWalletFactory(final Web3j web3j,
                                   final Admin blockchainAdmin) {
        this.web3j = web3j;
        this.blockchainAdmin = blockchainAdmin;
    }

    public BlockchainWallet create(final UserSnapshot userSnapshot) throws IOException {
        NewAccountIdentifier newAccountIdentifier = blockchainAdmin.personalNewAccount(userSnapshot.getPassword()).send();

        unlockAccount(blockchainAdmin, newAccountIdentifier.getAccountId(), userSnapshot.getPassword());

        transferStartingAmountOfTokens(newAccountIdentifier.getAccountId());

        return new BlockchainWallet(userSnapshot.getId(), newAccountIdentifier.getAccountId());

    }

    private void transferStartingAmountOfTokens(final String receiverAddress) throws IOException {
        EthCoinbase coinbase = web3j.ethCoinbase().send();
        EthGetTransactionCount transactionCount = web3j.ethGetTransactionCount(coinbase.getAddress(), DefaultBlockParameterName.LATEST).send();
        Transaction transaction = Transaction.createEtherTransaction(coinbase.getAddress(), transactionCount.getTransactionCount(),
                BlockchainWalletConstants.GAS_PRICE, BlockchainWalletConstants.GAS_LIMIT, receiverAddress,
                BigInteger.valueOf(BlockchainWalletConstants.STARTING_AMOUNT_OF_TOKENS));
        web3j.ethSendTransaction(transaction).send();
    }

    private void unlockAccount(final Admin blockchainAdmin, final String walletAddress, final String walletPassword) throws IOException {
        PersonalUnlockAccount personalUnlockAccount = blockchainAdmin.personalUnlockAccount(walletAddress, walletPassword).send();
        if(personalUnlockAccount == null){
            log.info("Couldnt unlock wallet for new user!");
        }
    }
}
