package com.sharecare.core.blockchain.repository;

import com.sharecare.core.blockchain.entity.BlockchainWallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlockchainWalletRepository extends JpaRepository<BlockchainWallet, Long> {
}
