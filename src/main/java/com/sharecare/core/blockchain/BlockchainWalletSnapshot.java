package com.sharecare.core.blockchain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class BlockchainWalletSnapshot {
    private long userId;
    private String walletAddress;
}
