package com.sharecare.core.blockchain.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Contract {

    private int fee;
    private String receiver;
    private String address;

}
