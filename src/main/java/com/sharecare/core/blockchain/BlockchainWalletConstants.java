package com.sharecare.core.blockchain;

import java.math.BigInteger;

public final class BlockchainWalletConstants {

    public static final String PASSWORD = "shareCare123";
    public static final BigInteger GAS_PRICE = BigInteger.valueOf(1L);
    public static final BigInteger GAS_LIMIT = BigInteger.valueOf(500_000L);
    public static final String APPLICATION_WALLET_PATH = "src/main/resources/application_credentials.json";
    public static final long BALANCE_NOT_FOUND = -1;
    public static final long STARTING_AMOUNT_OF_TOKENS = 25_000_000_000_000_000L;

    private BlockchainWalletConstants() {
    }

}
