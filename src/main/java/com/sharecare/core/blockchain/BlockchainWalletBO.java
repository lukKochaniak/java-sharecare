package com.sharecare.core.blockchain;

import com.sharecare.app.restapi.blockchain.addContract.AddContractRequest;
import com.sharecare.core.blockchain.model.Contract;
import com.sharecare.core.users.UserSnapshot;
import org.web3j.crypto.CipherException;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

public interface BlockchainWalletBO {

    BlockchainWalletSnapshot addWallet(UserSnapshot userSnapshot) throws IOException;

    long getBalance(long userId) throws IOException;

    void deleteWallet(final long walletId);

    BlockchainWalletSnapshot updateWallet(final BlockchainWalletSnapshot blockchainWalletSnapshot);

    boolean performTransaction(AddContractRequest addContractRequest) throws Exception;

    boolean performTransaction(long toId, long amount, long fromId) throws Exception;
}
