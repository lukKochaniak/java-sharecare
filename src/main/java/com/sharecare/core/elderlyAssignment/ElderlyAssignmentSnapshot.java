package com.sharecare.core.elderlyAssignment;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ElderlyAssignmentSnapshot {
    private long id;
    private long assignedCaregiverId;
    private long assignedElderlyId;
    private ElderlyAssignmentStatus elderlyAssignmentStatus;
}
