package com.sharecare.core.elderlyAssignment.bo;

import com.sharecare.core.elderlyAssignment.ElderlyAssignmentBO;
import com.sharecare.core.elderlyAssignment.ElderlyAssignmentSnapshot;
import com.sharecare.core.elderlyAssignment.ElderlyAssignmentStatus;
import com.sharecare.core.elderlyAssignment.entity.ElderlyAssignment;
import com.sharecare.core.elderlyAssignment.repository.ElderlyAssignmentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ElderlyAssignmentBOImpl implements ElderlyAssignmentBO {

    private final ElderlyAssignmentRepository elderlyAssignmentRepository;

    public ElderlyAssignmentBOImpl(final ElderlyAssignmentRepository elderlyAssignmentRepository) {
        this.elderlyAssignmentRepository = elderlyAssignmentRepository;
    }

    @Override
    public ElderlyAssignmentSnapshot add(long caregiverId, long elderlyId) {
        ElderlyAssignment elderlyAssignment = elderlyAssignmentRepository.save(new ElderlyAssignment(caregiverId, elderlyId));
        if (elderlyAssignment != null) return elderlyAssignment.toSnapshot();
        else return null;
    }

    @Override
    public ElderlyAssignmentSnapshot accept(long caregiverId, long elderlyId) {
        ElderlyAssignment elderlyAssignment = elderlyAssignmentRepository.findByAssignedCaregiverIdAndAndAssignedElderlyId(caregiverId, elderlyId);
        elderlyAssignment.setElderlyAssignmentStatus(ElderlyAssignmentStatus.ASSIGNMENT_ACCEPTED);
        return elderlyAssignment.toSnapshot();
    }

    @Override
    public ElderlyAssignmentSnapshot decline(long caregiverId, long elderlyId) {
        ElderlyAssignment elderlyAssignment = elderlyAssignmentRepository.findByAssignedCaregiverIdAndAndAssignedElderlyId(caregiverId, elderlyId);
        elderlyAssignment.setElderlyAssignmentStatus(ElderlyAssignmentStatus.ASSIGNMENT_REJECTED);
        return elderlyAssignment.toSnapshot();
    }

    @Override
    public void delete(long caregiverId, long elderlyId) {
        ElderlyAssignment elderlyAssignment = findByCaregiverAndElderlyIdInRepository(caregiverId, elderlyId);
        elderlyAssignmentRepository.delete(elderlyAssignment);
    }

    @Override
    public List<ElderlyAssignmentSnapshot> getAllCaregiversAssignedToElderly(long id) {
        return elderlyAssignmentRepository.findAllByAssignedElderlyId(id)
                .stream()
                .filter(elderlyAssignment -> elderlyAssignment.getElderlyAssignmentStatus() == ElderlyAssignmentStatus.ASSIGNMENT_ACCEPTED)
                .map(ElderlyAssignment::toSnapshot)
                .collect(Collectors.toList());
    }

    @Override
    public List<ElderlyAssignmentSnapshot> getAllElderliesAssignedToCaregiver(long id) {
        return elderlyAssignmentRepository.findAllByAssignedCaregiverId(id)
                .stream()
                .filter(elderlyAssignment -> elderlyAssignment.getElderlyAssignmentStatus() == ElderlyAssignmentStatus.ASSIGNMENT_ACCEPTED)
                .map(ElderlyAssignment::toSnapshot)
                .collect(Collectors.toList());
    }

    @Override
    public ElderlyAssignmentSnapshot findByCaregiverAndElderlyId(long caregiverId, long elderlyId) {
        ElderlyAssignment elderlyAssignment = findByCaregiverAndElderlyIdInRepository(caregiverId, elderlyId);
        if (elderlyAssignment != null) return elderlyAssignment.toSnapshot();
        else return null;
    }

    private ElderlyAssignment findByCaregiverAndElderlyIdInRepository(long caregiverId, long elderlyId) {
        return elderlyAssignmentRepository.findByAssignedCaregiverIdAndAndAssignedElderlyId(caregiverId, elderlyId);
    }
}
