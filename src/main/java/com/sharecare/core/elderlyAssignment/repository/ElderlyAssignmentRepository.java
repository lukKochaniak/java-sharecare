package com.sharecare.core.elderlyAssignment.repository;

import com.sharecare.core.elderlyAssignment.entity.ElderlyAssignment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ElderlyAssignmentRepository extends JpaRepository<ElderlyAssignment, Long> {
    ElderlyAssignment findByAssignedCaregiverIdAndAndAssignedElderlyId(long caregiverId, long elderlyId);
    List<ElderlyAssignment> findAllByAssignedCaregiverId(long caregiverId);
    List<ElderlyAssignment> findAllByAssignedElderlyId(long elderlyId);
}
