package com.sharecare.core.elderlyAssignment.entity;

import com.sharecare.core.elderlyAssignment.ElderlyAssignmentSnapshot;
import com.sharecare.core.elderlyAssignment.ElderlyAssignmentStatus;
import com.sharecare.core.notifications.NotificationType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
public class ElderlyAssignment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private long assignedCaregiverId;

    private long assignedElderlyId;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ElderlyAssignmentStatus elderlyAssignmentStatus;


    protected ElderlyAssignment() {
    }

    public ElderlyAssignment(final long assignedCaregiverId,
                             final long assignedElderlyId) {
        this.assignedCaregiverId = assignedCaregiverId;
        this.assignedElderlyId = assignedElderlyId;
        this.elderlyAssignmentStatus = ElderlyAssignmentStatus.ASSIGNMENT_WAITING;
    }

    public ElderlyAssignmentSnapshot toSnapshot() {
        return ElderlyAssignmentSnapshot.builder()
                .id(id)
                .assignedCaregiverId(assignedCaregiverId)
                .assignedElderlyId(assignedElderlyId)
                .elderlyAssignmentStatus(elderlyAssignmentStatus)
                .build();
    }
}
