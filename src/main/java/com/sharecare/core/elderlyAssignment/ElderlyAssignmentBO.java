package com.sharecare.core.elderlyAssignment;

import java.util.List;

public interface ElderlyAssignmentBO {

    ElderlyAssignmentSnapshot add(long caregiverId, long elderlyId);

    ElderlyAssignmentSnapshot accept(long caregiverId, long elderlyId);

    ElderlyAssignmentSnapshot decline(long caregiverId, long elderlyId);

    void delete(long caregiverId, long elderlyId);

    ElderlyAssignmentSnapshot findByCaregiverAndElderlyId(long caregiverId, long elderlyId);

    List<ElderlyAssignmentSnapshot> getAllCaregiversAssignedToElderly(long id);

    List<ElderlyAssignmentSnapshot> getAllElderliesAssignedToCaregiver(long id);

    }
