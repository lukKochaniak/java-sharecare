package com.sharecare.core.elderlyAssignment;

public enum ElderlyAssignmentStatus {
    ASSIGNMENT_WAITING,
    ASSIGNMENT_ACCEPTED,
    ASSIGNMENT_REJECTED
}
