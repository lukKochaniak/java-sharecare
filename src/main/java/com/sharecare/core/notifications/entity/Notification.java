package com.sharecare.core.notifications.entity;

import com.sharecare.core.notifications.NotificationSnapshot;
import com.sharecare.core.notifications.NotificationType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Getter
@Setter
public class Notification {

    @Id
    @Column(unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long notificationId;

    @NotNull
    private long toUserId;

    @NotNull
    private long fromUserId;

    @NotNull
    private String notificationTitle;

    @NotNull
    private String notificationMessage;

    private boolean wasNotificationRead;

    @NotNull
    private Date dateOfSending;

    @NotNull
    @Enumerated(EnumType.STRING)
    private NotificationType notificationType;

    protected Notification() {
    }

    public Notification(long fromUserId,
                        long toUserId,
                        @NotNull String notificationTitle,
                        @NotNull String notificationMessage,
                        @NotNull Date dateOfSending,
                        NotificationType notificationType) {
        this.toUserId = toUserId;
        this.fromUserId = fromUserId;
        this.notificationTitle = notificationTitle;
        this.notificationMessage = notificationMessage;
        this.wasNotificationRead = false;
        this.dateOfSending = dateOfSending;
        this.notificationType = notificationType;
    }

    public void markAsRead() {
        this.wasNotificationRead = true;
    }

    public NotificationSnapshot toSnapshot() {
        return NotificationSnapshot.builder()
                .id(notificationId)
                .toUserId(toUserId)
                .fromUserId(fromUserId)
                .title(notificationTitle)
                .message(notificationMessage)
                .read(wasNotificationRead)
                .dateOfSending(dateOfSending)
                .type(notificationType)
                .build();
    }
}
