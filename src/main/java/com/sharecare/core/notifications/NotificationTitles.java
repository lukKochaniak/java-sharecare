package com.sharecare.core.notifications;

public class NotificationTitles {

    public static final String TITLE_ON_ASSIGN_ELDERLY_OFFER = "Assignment to caregiver account offer";
    public static final String TITLE_ON_ACCEPTING_ASSIGNMENT = "Assignment account accepted";
    public static final String TITLE_ON_REQUESTING_SERVICE = "Service requested";
    public static final String TITLE_ON_ACCEPTING_SERVICE_REQUEST = "Requested service accepted";
    public static final String TITLE_ON_DECLINING_SERVICE_REQUEST = "Requested service declined";
    public static final String TITLE_ON_SERVICE_COMPLETED = "Requested service completed";
    public static final String TITLE_ON_SERVICE_CANCELED= "Service canceled";
    public static final String TITLE_ON_SERVICE_DELETED= "Service deleted";
    public static final String ACCOUNT_ASSIGNMENT_DECLINED= "Connection declined";

    public static String getTitle(NotificationType type) {

        switch(type) {
            case ACCOUNT_ASSIGNMENT_OFFER:
                return TITLE_ON_ASSIGN_ELDERLY_OFFER;
            case ACCOUNT_ASSIGNMENT_DECLINED:
                return TITLE_ON_ASSIGN_ELDERLY_OFFER;
            case ACCOUNT_ASSIGNMENT_ACCEPTED:
                return TITLE_ON_ACCEPTING_ASSIGNMENT;
            case SERVICE_REQUESTED:
                return TITLE_ON_REQUESTING_SERVICE;
            case SERVICE_REQUEST_ACCEPTED:
                return TITLE_ON_ACCEPTING_SERVICE_REQUEST;
            case SERVICE_REQUESTED_DECLINED:
                return TITLE_ON_DECLINING_SERVICE_REQUEST;
            case SERVICE_COMPLETED:
                return TITLE_ON_SERVICE_COMPLETED;
            case SERVICE_CANCELED:
                return TITLE_ON_SERVICE_CANCELED;
            case SERVICE_DELETED:
                return TITLE_ON_SERVICE_DELETED;
        }
        return null;
    }

    private NotificationTitles() {}
}
