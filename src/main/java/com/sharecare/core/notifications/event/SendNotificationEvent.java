package com.sharecare.core.notifications.event;

import com.sharecare.core.notifications.NotificationSnapshot;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class SendNotificationEvent extends ApplicationEvent {

    private final NotificationSnapshot notificationSnapshot;

    public SendNotificationEvent(final Object source, NotificationSnapshot notificationSnapshot) {
        super(source);
        this.notificationSnapshot = notificationSnapshot;
    }
}
