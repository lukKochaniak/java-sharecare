package com.sharecare.core.notifications;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@Builder
public class NotificationSnapshot {

    private long id;
    private long toUserId;
    private long fromUserId;
    private String fromUser;
    private String title;
    private String message;
    private boolean read; //true if user read this notification
    private Date dateOfSending;
    private NotificationType type;
}
