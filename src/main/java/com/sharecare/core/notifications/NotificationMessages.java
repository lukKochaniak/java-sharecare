package com.sharecare.core.notifications;

public class NotificationMessages {

    public static final String MESSAGE_ON_ASSIGN_ELDERLY_OFFER = "You got proposition of assigning your account to user: ";
    public static final String MESSAGE_ON_ACCEPTING_ASSIGNMENT = "Your proposition of assigning account has been accepted by user: ";
    public static final String MESSAGE_ON_REQUESTING_SERVICE = "You've got request for executing the service from user: ";
    public static final String MESSAGE_ON_ACCEPTING_SERVICE_REQUEST = "Your request for executing the service has been accepted by user: ";
    public static final String MESSAGE_ON_DECLINING_SERVICE_REQUEST = "Your request for executing the service has been declined by user: ";
    public static final String MESSAGE_ON_COMPLETING_SERVICE = "The service you requested was confirmed as completed by user: ";
    public static final String MESSAGE_ON_CANCELLING_SERVICE = "The service is waiting for caregiver and was canceled by user: ";
    public static final String MESSAGE_ON_DELETING_SERVICE = "The service was deleted by user: ";
    public static final String MESSAGE_ON_DELETING_ACCOUNT_ASSIGNMENT= "Connection between you and: ";
    public static final String MESSAGE_ON_ACCOUNT_ASSIGNMENT_DECLINED= "Your connection with: ";

    public static String getMessage(String from, NotificationType type) {

        switch (type) {
            case ACCOUNT_ASSIGNMENT_OFFER:
                return MESSAGE_ON_ASSIGN_ELDERLY_OFFER + from;
            case ACCOUNT_ASSIGNMENT_ACCEPTED:
                return MESSAGE_ON_ACCEPTING_ASSIGNMENT + from;
            case SERVICE_REQUESTED:
                return MESSAGE_ON_REQUESTING_SERVICE + from;
            case SERVICE_REQUEST_ACCEPTED:
                return MESSAGE_ON_ACCEPTING_SERVICE_REQUEST + from + ". Refresh the page!";
            case SERVICE_REQUESTED_DECLINED:
                return  MESSAGE_ON_DECLINING_SERVICE_REQUEST + from;
            case SERVICE_COMPLETED:
                return MESSAGE_ON_COMPLETING_SERVICE + from;
            case SERVICE_CANCELED:
                return MESSAGE_ON_CANCELLING_SERVICE + from;
            case SERVICE_DELETED:
                return MESSAGE_ON_DELETING_SERVICE + from;
            case ACCOUNT_ASSIGNMENT_DELETED:
                    return MESSAGE_ON_DELETING_ACCOUNT_ASSIGNMENT + from + " was deleted";
            case ACCOUNT_ASSIGNMENT_DECLINED:
                return MESSAGE_ON_ACCOUNT_ASSIGNMENT_DECLINED + from + " was declined";
        }

        return null;
    }

    private NotificationMessages() {}
}
