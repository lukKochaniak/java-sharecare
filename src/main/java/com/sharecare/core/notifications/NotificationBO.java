package com.sharecare.core.notifications;

import java.util.List;

public interface NotificationBO {
    NotificationSnapshot addNotification(long fromId, long toId, String fromUser, NotificationType type);

    NotificationSnapshot addServiceNotification(long fromId, long toId, String fromUser, NotificationType type, String serviceMessage);

    List<NotificationSnapshot> findAllForUser(long userId);

    NotificationSnapshot findById(long id);

    void markAsRead(long id);

    boolean wasRead(long id);

    void delete(long id);
}
