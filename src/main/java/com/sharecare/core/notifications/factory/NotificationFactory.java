package com.sharecare.core.notifications.factory;

import com.sharecare.core.notifications.NotificationMessages;
import com.sharecare.core.notifications.NotificationTitles;
import com.sharecare.core.notifications.NotificationType;
import com.sharecare.core.notifications.entity.Notification;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class NotificationFactory {

    public Notification createNotification(final long fromId, final long toId, final String from, final NotificationType type) {
        String title = NotificationTitles.getTitle(type);
        String message = NotificationMessages.getMessage(from, type);
        Date date = new Date();

        return new Notification(fromId, toId, title, message, date, type);
    }
}
