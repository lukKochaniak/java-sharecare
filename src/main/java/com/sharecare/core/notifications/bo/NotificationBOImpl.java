package com.sharecare.core.notifications.bo;

import com.sharecare.core.notifications.NotificationBO;
import com.sharecare.core.notifications.NotificationSnapshot;
import com.sharecare.core.notifications.NotificationType;
import com.sharecare.core.notifications.entity.Notification;
import com.sharecare.core.notifications.factory.NotificationFactory;
import com.sharecare.core.notifications.repository.NotificationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@Slf4j
public class NotificationBOImpl implements NotificationBO {

    private final NotificationRepository notificationRepository;
    private final NotificationFactory notificationFactory;
    private final SimpMessagingTemplate notificationTemplate;

    public NotificationBOImpl(final NotificationRepository notificationRepository,
                              final NotificationFactory notificationFactory,
                              final SimpMessagingTemplate notificationTemplate) {
        this.notificationRepository = notificationRepository;
        this.notificationFactory = notificationFactory;
        this.notificationTemplate = notificationTemplate;
    }

    @Override
    public NotificationSnapshot addNotification(long fromId, long toId, String fromUser, NotificationType type) {
        Notification notification = notificationFactory.createNotification(fromId, toId, fromUser, type);

        notificationRepository.save(notification);

        log.info("Added notification with id <{}> of type <{}>", notification.getNotificationId(), notification.getNotificationType());

        sendNotification(toId, fromUser);

        return notification.toSnapshot();
    }

    @Override
    public NotificationSnapshot addServiceNotification(long fromId, long toId, String fromUser, NotificationType type, String serviceMessage) {
        Notification notification = notificationFactory.createNotification(fromId, toId, fromUser, type);
        notification.setNotificationMessage(notification.getNotificationMessage() + ", " + serviceMessage);
        notificationRepository.save(notification);

        log.info("Added notification with id <{}> of type <{}>", notification.getNotificationId(), notification.getNotificationType());

        sendNotification(toId, fromUser);

        return notification.toSnapshot();
    }

    @Override
    public List<NotificationSnapshot> findAllForUser(long userId) {
        List<Notification> notifications = notificationRepository.findAllByToUserId(userId);
        return notifications.stream()
                .map(Notification::toSnapshot)
                .sorted((o1, o2) -> o2.getDateOfSending().compareTo(o1.getDateOfSending()))
                .collect(Collectors.toList());
    }

    @Override
    public NotificationSnapshot findById(long id) {
        Optional<Notification> optionalNotification = notificationRepository.findById(id);

        return optionalNotification.map(Notification::toSnapshot).orElse(null);
    }

    @Override
    public void markAsRead(long id) {
        Optional<Notification> optionalNotification = notificationRepository.findById(id);
        optionalNotification.ifPresent(Notification::markAsRead);
    }

    @Override
    public boolean wasRead(long id) {
        Optional<Notification> optionalNotification = notificationRepository.findById(id);

        if (optionalNotification.isPresent()) {
            Notification notification = optionalNotification.get();
            return notification.isWasNotificationRead();
        }

        return false;
    }

    @Override
    public void delete(long id) {
        Optional<Notification> optionalNotification = notificationRepository.findById(id);
        optionalNotification.ifPresent(notificationRepository::delete);
    }

    private void sendNotification(long toId, String fromUser) {
        //TODO: maybe try to replace it with smth!!!
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        notificationTemplate.convertAndSend("/notification/websocket/" + toId, fromUser);
    }
}
