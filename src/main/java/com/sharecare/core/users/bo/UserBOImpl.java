package com.sharecare.core.users.bo;

import com.sharecare.app.restapi.users.profileupdate.ProfileUpdateRequest;
import com.sharecare.app.restapi.users.signup.UserAddRequest;
import com.sharecare.core.users.Role;
import com.sharecare.core.users.UserBO;
import com.sharecare.core.users.UserSnapshot;
import com.sharecare.core.users.entity.User;
import com.sharecare.core.users.event.ResetPasswordEvent;
import com.sharecare.core.users.event.SignupEvent;
import com.sharecare.core.users.factory.UserFactory;
import com.sharecare.core.users.repository.UserRepository;
import com.sharecare.core.users.validator.PasswordValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserBOImpl implements UserBO {

    private final UserRepository userRepository;
    private final UserFactory userFactory;
    private final PasswordEncoder passwordEncoder;
    private final PasswordValidator passwordValidator;
    private final ApplicationEventPublisher applicationEventPublisher;


    public UserBOImpl(final UserRepository userRepository,
                      final UserFactory userFactory,
                      final PasswordEncoder passwordEncoder,
                      final PasswordValidator passwordValidator,
                      final ApplicationEventPublisher applicationEventPublisher) {
        this.userRepository = userRepository;
        this.userFactory = userFactory;
        this.passwordEncoder = passwordEncoder;
        this.passwordValidator = passwordValidator;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public UserSnapshot findByEmail(String email) {
        User user = userRepository.findByEmail(email);
        return user == null ? null : user.toSnapshot();
    }

    @Override
    public UserSnapshot findByPhoneNumber(String phoneNumber) {
        User user = userRepository.findByPhoneNumber(phoneNumber);
        return user == null ? null : user.toSnapshot();
    }

    @Override
    public void delete(long id) {
        userRepository.deleteById(id);
        log.info("Deleted user with id <{}>", id);
    }

    @Override
    public UserSnapshot findById(long id) {
        Optional<User> optionalUser = userRepository.findById(id);
        return optionalUser.map(User::toSnapshot).orElse(null);
    }

    @Override
    public UserSnapshot updateProfile(ProfileUpdateRequest profileUpdateRequest) {
        UserSnapshot userSnapshot = findByEmail(profileUpdateRequest.getEmail());
        Optional<User> optionalUser = userRepository.findById(userSnapshot.getId());
        boolean isUserPresent = optionalUser.isPresent();
        if (isUserPresent) {
            User user = optionalUser.get();
            BeanUtils.copyProperties(profileUpdateRequest, user);
            userRepository.save(user);
            log.info("Updated user with id <{}>", user.getId());
            userSnapshot = userRepository.findById(user.getId()).get().toSnapshot();
        }
        return userSnapshot;
    }

    @Override
    public void setRole(long id, Role role) {
        User user = userRepository.getOne(id);

        user.setRole(role);

        log.info("Set role <{}> for user with id <{}>", role.name(), user.getId());
    }

    @Override
    public void changePassword(long userId, String password) {
        passwordValidator.validate(password);

        User user = userRepository.getOne(userId);
        user.updatePassword(passwordEncoder.encode(password));

        log.info("Password changed for user with id <{}>", user.getId());

    }

    @Override
    public void resetPassword(UserSnapshot userSnapshot) {
        applicationEventPublisher.publishEvent(new ResetPasswordEvent(this, userSnapshot));
        log.info("Attempt to change password by user with id <{}>", userSnapshot.getId());
    }

    @Override
    public void changeEmail(long userId, String newEmail) {
        User user = userRepository.getOne(userId);
        String previousEmail = user.getEmail();
        user.updateEmail(newEmail);
        log.info("Email changed from <{}> for user with id <{}> to a new email <{}>", previousEmail, userId, newEmail);
    }

    @Override
    public boolean isMatchingPassword(String email, String password) {
        User user = userRepository.findByEmail(email);
        if (user == null) return false;
        else return passwordEncoder.matches(password, user.getPassword());
    }

    @Override
    public UserSnapshot addUser(UserAddRequest userAddRequest) {
        User user = userFactory.create(userAddRequest);

        user = userRepository.save(user);

        UserSnapshot userSnapshot = user.toSnapshot();

        userSnapshot.setPassword(userAddRequest.getPassword());

        applicationEventPublisher.publishEvent(new SignupEvent(this, userSnapshot));

        log.info("Added user with id <{}> with email <{}>", user.getId(), user.getEmail());

        return user.toSnapshot();
    }

    @Override
    public void verifyUser(long userId) {
        Optional<User> optionalUser = userRepository.findById(userId);

        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setVerified(true);
            userRepository.save(user);
            log.info("Verified user with id <{}>", user.getId());
        }
    }

    public boolean isUserVerified(String email) {
        User user = userRepository.findByEmail(email);

        if (user != null) {
            return user.isVerified();
        }
        return false;
    }

    @Override
    public Role getUserRole(long userId) {
        User user = userRepository.getOne(userId);

        return user.getRole();
    }
}
