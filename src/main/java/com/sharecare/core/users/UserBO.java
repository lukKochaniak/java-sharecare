package com.sharecare.core.users;

import com.sharecare.app.restapi.users.profileupdate.ProfileUpdateRequest;
import com.sharecare.app.restapi.users.signup.UserAddRequest;

import java.util.List;

public interface UserBO {

    UserSnapshot addUser(UserAddRequest user);

    void verifyUser(long userId);

    boolean isUserVerified(String email);

    UserSnapshot findByEmail(String email);

    UserSnapshot findByPhoneNumber(String phoneNumber);

    void delete(long id);

    UserSnapshot findById(long id);

    UserSnapshot updateProfile(ProfileUpdateRequest profileUpdateRequest);

    void setRole(long id, Role role);

    void changePassword(long userId, String password);

    void resetPassword(UserSnapshot userSnapshot);

    void changeEmail(long userId, String newEmail);

    boolean isMatchingPassword(String email, String password);

    Role getUserRole(long userId);
}
