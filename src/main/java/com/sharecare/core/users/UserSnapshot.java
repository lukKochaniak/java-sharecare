package com.sharecare.core.users;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Locale;

@Getter
@Setter
@Builder
public class UserSnapshot {

    private long id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private Date dateOfBirth;
    private String phoneNumber;
    private String address;
    private String city;
    private String zipcode;
    private Role role;
    private Locale locale;

}
