package com.sharecare.core.users.factory;

import com.sharecare.app.restapi.users.signup.UserAddRequest;
import com.sharecare.core.users.entity.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserFactory {
    private final PasswordEncoder passwordEncoder;

    public UserFactory(final PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public User create(final UserAddRequest userAddRequest) {
        String passwordEncrypted = passwordEncoder.encode(userAddRequest.getPassword());

        return new User(userAddRequest.getFirstName(), userAddRequest.getLastName(), userAddRequest.getEmail(),
                passwordEncrypted, userAddRequest.getDateOfBirth(), userAddRequest.getPhoneNumber(),
                userAddRequest.getAddress(), userAddRequest.getCity(), userAddRequest.getZipcode(), userAddRequest.getRole());
    }
}
