package com.sharecare.core.users.event;

import com.sharecare.core.users.UserSnapshot;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class SignupEvent extends ApplicationEvent {

    private final UserSnapshot userSnapshot;

    public SignupEvent(final Object source, final UserSnapshot userSnapshot) {
        super(source);
        this.userSnapshot = userSnapshot;
    }
}
