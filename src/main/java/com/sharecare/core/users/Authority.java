package com.sharecare.core.users;

public enum Authority {
    MODIFY_ELDERLY,
    MANAGE_TOKENS,
    SEE_SERVICES
}
