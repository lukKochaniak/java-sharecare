package com.sharecare.core.users;

import java.time.Duration;

public class SignupConstants {
    public static final int MIN_PASSWORD_LENGTH = 8;
    public static final int MAX_PASSWORD_LENGTH = 255;

    public static final Duration COOL_DOWN_IN_MINUTES = Duration.ofMinutes(15);
    public static final int MIN_AGE = 0;


    private SignupConstants() { }
}
