package com.sharecare.core.users.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sharecare.core.users.Role;
import com.sharecare.core.users.SignupConstants;
import com.sharecare.core.users.UserSnapshot;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Locale;

@Entity
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    //TODO:unique?
    private String email;

    @JsonIgnore
    @NotNull
    @Size(min = SignupConstants.MIN_PASSWORD_LENGTH,
            max = SignupConstants.MAX_PASSWORD_LENGTH)
    private String password;

    @NotNull
    private Date dateOfBirth;

    @NotNull
    private String phoneNumber;

    @NotNull
    private String address;

    @NotNull
    private String city;

    @NotNull
    private String zipcode;

    private Role role;

    private boolean verified;

    protected User() {
    }

    public User(final String email,
                final String password,
                final Role role) {
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public User(@NotNull String firstName, @NotNull String lastName, @NotNull String email, @NotNull String password, @NotNull Date dateOfBirth, @NotNull String phoneNumber, @NotNull String address, @NotNull String city, @NotNull String zipcode, Role role) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.dateOfBirth = dateOfBirth;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.city = city;
        this.zipcode = zipcode;
        this.role = role;
    }

    public void updatePassword(final String newPassword) {
        this.password = newPassword;
    }

    public void updateEmail(final String newEmail) {
        this.email = newEmail;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(final Role role) {
        this.role = role;
    }

    public UserSnapshot toSnapshot() {
        return UserSnapshot.builder()
                .id(id)
                .firstName(firstName)
                .lastName(lastName)
                .dateOfBirth(dateOfBirth)
                .city(city)
                .zipcode(zipcode)
                .phoneNumber(phoneNumber)
                .address(address)
                .email(email)
                .role(role)
                .build();
    }

}
