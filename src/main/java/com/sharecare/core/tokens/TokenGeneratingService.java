package com.sharecare.core.tokens;

public interface TokenGeneratingService {
    String generateToken();
}
