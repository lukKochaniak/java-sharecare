package com.sharecare.core.tokens.service;

import com.sharecare.core.tokens.TokenGeneratingService;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.Random;

@Component
public class TokenGeneratingServiceImpl
        implements TokenGeneratingService {

    private final int tokenEncodingBase = 32;
    private final int tokenRandomnessBits = 130;
    private final Random random = new Random();

    public TokenGeneratingServiceImpl() {

    }

    @Override
    public String generateToken() {
        return new BigInteger(tokenRandomnessBits, random)
                .toString(tokenEncodingBase);
    }
}
