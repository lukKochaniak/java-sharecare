package com.sharecare.core.tokens;

import com.sharecare.core.tokens.entity.TokenCreationReason;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Builder
@Getter
public class EmailConfirmationTokenSnapshot {
    private final String token;
    private final LocalDateTime expiresAt;
    private final long referenceUserId;
    private final TokenCreationReason reason;
}
