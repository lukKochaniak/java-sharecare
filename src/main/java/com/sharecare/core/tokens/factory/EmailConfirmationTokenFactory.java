package com.sharecare.core.tokens.factory;

import com.sharecare.core.tokens.TokenConstants;
import com.sharecare.core.tokens.entity.EmailConfirmationToken;
import com.sharecare.core.tokens.entity.TokenCreationReason;
import com.sharecare.core.tokens.TokenGeneratingService;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class EmailConfirmationTokenFactory {

    private final TokenGeneratingService tokenGeneratingService;

    public EmailConfirmationTokenFactory(final TokenGeneratingService tokenGeneratingService) {
        this.tokenGeneratingService = tokenGeneratingService;
    }

    public EmailConfirmationToken create(final TokenCreationReason reason, final long userId) {
        String token = tokenGeneratingService.generateToken();
        LocalDateTime expiresAt = LocalDateTime.now()
                .plusDays(TokenConstants.TOKEN_EXPIRATION_DATE);

        return new EmailConfirmationToken(token, expiresAt, reason, userId);
    }
}
