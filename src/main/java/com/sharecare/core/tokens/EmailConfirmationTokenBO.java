package com.sharecare.core.tokens;

import com.sharecare.core.tokens.entity.TokenCreationReason;

import java.util.Optional;

public interface EmailConfirmationTokenBO {
    EmailConfirmationTokenSnapshot add(TokenCreationReason reason, long registrationApplicationId);

    Optional<EmailConfirmationTokenSnapshot> verify(TokenCreationReason reason, String token);

    Optional<EmailConfirmationTokenSnapshot> check(TokenCreationReason reason, String token);

    Optional<EmailConfirmationTokenSnapshot> findById(final String token);

    Optional<EmailConfirmationTokenSnapshot> findByUserId(final long referenceObjectId);

    long getTokenUserId(String token);

}
