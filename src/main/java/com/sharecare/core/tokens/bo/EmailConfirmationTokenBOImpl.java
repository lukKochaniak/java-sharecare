package com.sharecare.core.tokens.bo;

import com.sharecare.core.tokens.EmailConfirmationTokenBO;
import com.sharecare.core.tokens.EmailConfirmationTokenSnapshot;
import com.sharecare.core.tokens.entity.EmailConfirmationToken;
import com.sharecare.core.tokens.entity.TokenCreationReason;
import com.sharecare.core.tokens.factory.EmailConfirmationTokenFactory;
import com.sharecare.core.tokens.repository.EmailConfirmationTokenRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Slf4j
@Service
public class EmailConfirmationTokenBOImpl
        implements EmailConfirmationTokenBO {

    private final EmailConfirmationTokenRepository emailConfirmationTokenRepository;
    private final EmailConfirmationTokenFactory emailConfirmationTokenFactory;

    public EmailConfirmationTokenBOImpl(final EmailConfirmationTokenRepository emailConfirmationTokenRepository,
                                        final EmailConfirmationTokenFactory emailConfirmationTokenFactory) {
        this.emailConfirmationTokenRepository = emailConfirmationTokenRepository;
        this.emailConfirmationTokenFactory = emailConfirmationTokenFactory;
    }

    @Override
    public EmailConfirmationTokenSnapshot add(final TokenCreationReason reason, final long userId) {
        EmailConfirmationToken emailConfirmationToken = emailConfirmationTokenFactory.create(reason, userId);

        emailConfirmationToken = emailConfirmationTokenRepository.save(emailConfirmationToken);

        EmailConfirmationTokenSnapshot emailConfirmationTokenSnapshot = emailConfirmationToken.toSnapshot();

        log.info("Generated new EmailConfirmationToken for user <{}> <{}>",
                userId, emailConfirmationTokenSnapshot.getToken());

        return emailConfirmationTokenSnapshot;

    }

    @Override
    public Optional<EmailConfirmationTokenSnapshot> check(final TokenCreationReason reason, final String token) {
        EmailConfirmationToken emailConfirmationToken = emailConfirmationTokenRepository.findByTokenAndReason(token, reason);

        if (emailConfirmationToken == null) {
            log.info("Token <{}> does not exists", token);
            return Optional.empty();
        }

        boolean verified = emailConfirmationToken.verify(token, LocalDateTime.now());

        log.info("Token <{}> verified with result <{}>",
                token, verified);

        return verified ? Optional.of(emailConfirmationToken.toSnapshot()) : Optional.empty();
    }

    @Override
    public Optional<EmailConfirmationTokenSnapshot> verify(final TokenCreationReason reason, final String token) {
        Optional<EmailConfirmationTokenSnapshot> result = this.check(reason, token);
        if (result.isPresent()) {
            emailConfirmationTokenRepository.deleteByToken(token);
            log.info("Token <{}> deleted", token);
        }
        return result;
    }

    @Override
    public Optional<EmailConfirmationTokenSnapshot> findById(final String token) {
        EmailConfirmationToken emailConfirmationToken = emailConfirmationTokenRepository.findByToken(token);

        return emailConfirmationToken == null ? Optional.empty() : Optional.of(emailConfirmationToken.toSnapshot());
    }

    @Override
    public Optional<EmailConfirmationTokenSnapshot> findByUserId(final long userId) {
        EmailConfirmationToken emailConfirmationToken = emailConfirmationTokenRepository.findByReferenceUserId(userId);

        return emailConfirmationToken == null ? Optional.empty() : Optional.of(emailConfirmationToken.toSnapshot());
    }

    @Override
    public long getTokenUserId(String token) {
        return emailConfirmationTokenRepository.findByToken(token).toSnapshot().getReferenceUserId();
    }
}
