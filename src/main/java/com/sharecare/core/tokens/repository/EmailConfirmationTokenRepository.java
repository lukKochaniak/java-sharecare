package com.sharecare.core.tokens.repository;

import com.sharecare.core.tokens.entity.EmailConfirmationToken;
import com.sharecare.core.tokens.entity.TokenCreationReason;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailConfirmationTokenRepository
        extends JpaRepository<EmailConfirmationToken, String> {
    EmailConfirmationToken findByTokenAndReason(String token, TokenCreationReason reason);

    EmailConfirmationToken findByToken(String token);

    EmailConfirmationToken findByReferenceUserId(long referenceUserId);

    EmailConfirmationToken deleteByToken(String token);
}
