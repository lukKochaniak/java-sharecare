package com.sharecare.core.tokens.entity;

import com.sharecare.core.tokens.EmailConfirmationTokenSnapshot;
import com.sharecare.core.tokens.TokenConstants;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Entity
public class EmailConfirmationToken {

    @Id
    @NotNull
    @Column(unique = true)
    @Size(min = TokenConstants.TOKEN_MIN_LENGTH,
            max = TokenConstants.TOKEN_MAX_LENGTH)
    private String token;

    @NotNull
    private LocalDateTime expiresAt;

    @NotNull
    @Enumerated(EnumType.STRING)
    private TokenCreationReason reason;

    private long referenceUserId;

    protected EmailConfirmationToken() {
    }

    public EmailConfirmationToken(final String token,
                                  final LocalDateTime expiresAt,
                                  final TokenCreationReason reason,
                                  final long referenceUserId) {
        this.token = token;
        this.expiresAt = expiresAt;
        this.referenceUserId = referenceUserId;
        this.reason = reason;
    }

    public boolean verify(final String token, final LocalDateTime now) {
        return this.token.equals(token)
                && expiresAt.isAfter(now);
    }

    public EmailConfirmationTokenSnapshot toSnapshot() {
        return EmailConfirmationTokenSnapshot.builder()
                .reason(reason)
                .token(token)
                .expiresAt(expiresAt)
                .referenceUserId(referenceUserId)
                .build();
    }
}
