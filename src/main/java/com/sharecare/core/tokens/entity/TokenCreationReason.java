package com.sharecare.core.tokens.entity;

public enum TokenCreationReason {
    REGISTRATION,
    PASSWORD_RESET
}
