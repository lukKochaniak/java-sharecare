package com.sharecare.core.tokens;

public final class TokenConstants {

    public static final int TOKEN_MIN_LENGTH = 10;
    public static final int TOKEN_MAX_LENGTH = 255;

    public static final int TOKEN_EXPIRATION_DATE = 2;


    private TokenConstants() {
    }
}
