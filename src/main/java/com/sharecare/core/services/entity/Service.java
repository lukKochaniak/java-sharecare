package com.sharecare.core.services.entity;

import com.sharecare.core.services.ServiceSnapshot;
import com.sharecare.core.services.Status;
import com.sharecare.core.services.Title;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.util.Date;


@Entity
@Getter
@Setter
@AllArgsConstructor
public class Service {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String title;

    @NotNull
    private String description;

    @NotNull
    private long cost;

    @NotNull
    private String address;

    @NotNull
    private String city;

    @NotNull
    private String zipcode;

    private long duration;

    private Date finishDate;

    @NotNull
    private long requestedby;

    private long providedby;

    @NotNull
    private Status status;

    protected Service() {
    }

    public Service(@NotNull String title, String description, @NotNull long cost, @NotNull String address, @NotNull String city, @NotNull String zipcode, long duration, @NotNull long requestedby) {
        this.title = title;
        this.description = description;
        this.cost = cost;
        this.address = address;
        this.city = city;
        this.zipcode = zipcode;
        this.duration = duration;
        this.requestedby = requestedby;
        this.status = Status.OPEN;
    }

    public ServiceSnapshot toSnapshot() {
        return ServiceSnapshot.builder()
                .id(id)
                .title(title)
                .description(description)
                .cost(cost)
                .address(address)
                .city(city)
                .zipcode(zipcode)
                .duration(duration)
                .finishDate(finishDate)
                .requestedBy(requestedby)
                .providedBy(providedby)
                .status(status)
                .build();
    }

}

