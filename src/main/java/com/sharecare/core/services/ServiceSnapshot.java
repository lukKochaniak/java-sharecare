package com.sharecare.core.services;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.Duration;
import java.util.Date;

@Getter
@Setter
@Builder
@ToString
public class ServiceSnapshot {

    private long id;
    private String title;
    private String description;
    private long cost;
    private String address;
    private String city;
    private String zipcode;
    private Date finishDate;
    //minutes
    private long duration;
    private long requestedBy;
    private long providedBy;
    private Status status;
}
