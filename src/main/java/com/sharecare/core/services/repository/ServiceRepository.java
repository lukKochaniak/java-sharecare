package com.sharecare.core.services.repository;

import com.sharecare.core.services.entity.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Long> {
    List<Service> findByRequestedby(long requestBy);

}
