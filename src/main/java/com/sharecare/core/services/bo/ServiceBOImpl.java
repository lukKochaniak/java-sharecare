package com.sharecare.core.services.bo;

import com.sharecare.app.restapi.services.addService.ServiceAddRequest;
import com.sharecare.core.services.ServiceBO;
import com.sharecare.core.services.ServiceSnapshot;
import com.sharecare.core.services.Status;
import com.sharecare.core.services.entity.Service;
import com.sharecare.core.services.factory.ServiceFactory;
import com.sharecare.core.services.repository.ServiceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@org.springframework.stereotype.Service
@Slf4j
public class ServiceBOImpl implements ServiceBO {

    private ServiceRepository serviceRepository;
    private ServiceFactory servicefactory;

    public ServiceBOImpl(final ServiceRepository servicerepository, final ServiceFactory servicefactory) {
        this.serviceRepository = servicerepository;
        this.servicefactory = servicefactory;
    }

    @Override
    public List<ServiceSnapshot> getAllServices() {
        List<Service> services = new ArrayList<>(serviceRepository.findAll());
        return services.stream().map(Service::toSnapshot).collect(Collectors.toList());
    }

    @Override
    public ServiceSnapshot addService(ServiceAddRequest serviceAddRequest) {
        Service service = servicefactory.create(serviceAddRequest);

        service = serviceRepository.save(service);

        log.info("Added new service with id <{}>", service.getId());

        return service.toSnapshot();
    }

    @Override
    public ServiceSnapshot getService(long id) {
        Optional<Service> serviceOptional = serviceRepository.findById(id);
        return serviceOptional.map(Service::toSnapshot).orElse(null);
    }

    @Override
    public ServiceSnapshot findById(long id) {
        Optional<Service> serviceOptional = serviceRepository.findById(id);
        return serviceOptional.map(Service::toSnapshot).orElse(null);
    }

    @Override
    public ServiceSnapshot updateService(ServiceSnapshot serviceSnapshot) {
        Service service = isServicePresent(serviceSnapshot.getId());
        if (service != null) {
            service.setProvidedby(serviceSnapshot.getProvidedBy());
            BeanUtils.copyProperties(serviceSnapshot, service);
            Service savedService = serviceRepository.save(service);
            log.info("Updated service with id <{}>", service.getId());
            return savedService.toSnapshot();
        }

        return serviceSnapshot;
    }

    @Override
    public ServiceSnapshot finishService(ServiceSnapshot serviceSnapshot) {
        Service service = isServicePresent(serviceSnapshot.getId());
        if (service != null) {
            service.setStatus(Status.COMPLETED);
            Service savedService = serviceRepository.save(service);
            log.info("Finished service with id <{}>", service.getId());
            return savedService.toSnapshot();
        }

        return null;
    }

    @Override
    public ServiceSnapshot cancelService(long serviceId) {
        Service service = isServicePresent(serviceId);
        if (service != null) {
            service.setStatus(Status.OPEN);
            service.setProvidedby(0);
            Service savedService = serviceRepository.save(service);
            log.info("Canceled service with id <{}>", service.getId());
            return savedService.toSnapshot();
        }
        return null;
    }

    private Service isServicePresent(long serviceId) {
        Optional<Service> optionalService = serviceRepository.findById(serviceId);
        return optionalService.orElse(null);
    }

    @Override
    public void deleteService(long serviceId) {
        serviceRepository.deleteById(serviceId);
        log.info("Deleted service with id <{}>", serviceId);
    }

    @Override
    public List<ServiceSnapshot> findByRequestedBy(long requestedById) {
        List<Service> services = serviceRepository.findByRequestedby(requestedById);
        return services.stream()
                .map(Service::toSnapshot)
                .collect(Collectors.toList());
    }

    @Override
    public List<ServiceSnapshot> findCompletedBy(long requestedById) {
        List<Service> services = serviceRepository.findByRequestedby(requestedById);
        return services.stream()
                .filter(service -> service.getStatus() == Status.COMPLETED)
                .map(Service::toSnapshot)
                .collect(Collectors.toList());
    }

    @Override
    public List<ServiceSnapshot> getOpenServices(long requesterId) {
        List<ServiceSnapshot> services = getAllServices();
        return services.stream()
                .filter(serviceSnapshot ->
                        serviceSnapshot.getStatus() == Status.OPEN && serviceSnapshot.getRequestedBy() != requesterId)
                .collect(Collectors.toList());
    }

    @Override
    public List<ServiceSnapshot> getPendingServices(long providedBy) {
        List<ServiceSnapshot> services = getAllServices();
        return services.stream()
                .filter(serviceSnapshot ->
                        serviceSnapshot.getStatus() == Status.ACCEPTED && serviceSnapshot.getProvidedBy() == providedBy)
                .collect(Collectors.toList());
    }

    @Override
    public List<ServiceSnapshot> getServicesToStartForElderly(long requestedById) {
        List<ServiceSnapshot> services = getAllServices();
        return services.stream()
                .filter(serviceSnapshot ->
                        serviceSnapshot.getStatus() == Status.ACCEPTED && serviceSnapshot.getRequestedBy() == requestedById)
                .collect(Collectors.toList());
    }
}
