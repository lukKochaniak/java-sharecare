package com.sharecare.core.services.factory;

import com.sharecare.app.restapi.services.addService.ServiceAddRequest;
import com.sharecare.core.services.entity.Service;
import org.springframework.stereotype.Component;

@Component
public class ServiceFactory {

    public Service create(final ServiceAddRequest serviceAddRequest) {
        return new Service(serviceAddRequest.getTitle(),
                serviceAddRequest.getDescription(), serviceAddRequest.getCost(), serviceAddRequest.getAddress(),
                serviceAddRequest.getCity(), serviceAddRequest.getZipcode(), serviceAddRequest.getDuration(), serviceAddRequest.getRequestedBy());
    }
}
