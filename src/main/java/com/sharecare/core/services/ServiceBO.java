package com.sharecare.core.services;

import com.sharecare.app.restapi.services.addService.ServiceAddRequest;
import com.sharecare.core.services.entity.Service;

import java.util.List;

public interface ServiceBO {

    List<ServiceSnapshot> getAllServices();

    ServiceSnapshot addService(ServiceAddRequest serviceAddRequest) ;

    ServiceSnapshot getService(long id);

    ServiceSnapshot findById(long id);

    ServiceSnapshot updateService(ServiceSnapshot serviceSnapshot);

    ServiceSnapshot finishService(ServiceSnapshot serviceSnapshot);

    ServiceSnapshot cancelService(long serviceId) ;

    void deleteService(long serviceId);

    List<ServiceSnapshot> findByRequestedBy(long requestedById);

    List<ServiceSnapshot> findCompletedBy(long requestedById);

    List<ServiceSnapshot> getOpenServices(long requesterId);

    List<ServiceSnapshot> getPendingServices(long providedBy);

    List<ServiceSnapshot> getServicesToStartForElderly(long requestedById);
}
