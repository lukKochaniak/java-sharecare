package com.sharecare.app.restapi.blockchain;

import com.sharecare.app.restapi.ApiResponse;
import com.sharecare.app.restapi.blockchain.addContract.AddContractRequest;
import com.sharecare.core.blockchain.BlockchainWalletBO;
import com.sharecare.core.blockchain.BlockchainWalletConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("/api/transactions")
@CrossOrigin(origins = "*", maxAge = 3600)
public class BlockchainWalletController {

    private final BlockchainWalletBO blockchainWalletBO;

    public BlockchainWalletController(final BlockchainWalletBO blockchainWalletBO) {
        this.blockchainWalletBO = blockchainWalletBO;
    }

    @GetMapping("/{id}")
    public ApiResponse<Long> getUserBalance(@PathVariable long id) throws IOException {
        long balance = blockchainWalletBO.getBalance(id);
        if (balance == BlockchainWalletConstants.BALANCE_NOT_FOUND) {
            return new ApiResponse<>(200, "Couldnt find balance", BlockchainWalletConstants.BALANCE_NOT_FOUND);
        }
        return new ApiResponse<>(200, "Balance found", balance);
    }

    @PostMapping("/pay")
    public HttpEntity<Boolean> makeTransaction(@RequestBody AddContractRequest addContractRequest) throws Exception {
        boolean whetherTransactionIsSuccessful = blockchainWalletBO.performTransaction(addContractRequest);
        return ResponseEntity.ok(whetherTransactionIsSuccessful);
    }

}
