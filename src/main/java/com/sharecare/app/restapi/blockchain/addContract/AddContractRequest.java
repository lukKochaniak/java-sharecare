package com.sharecare.app.restapi.blockchain.addContract;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AddContractRequest {
    private long toId;
    private long amount;
    private long fromId;
}
