package com.sharecare.app.restapi.notifications;

import com.sharecare.core.notifications.NotificationBO;
import com.sharecare.core.notifications.NotificationSnapshot;
import com.sharecare.core.users.UserBO;
import com.sharecare.core.users.UserSnapshot;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/notifications")
@CrossOrigin(origins = "*", maxAge = 3600)
public class NotificationController {

    private final UserBO userBO;
    private final NotificationBO notificationBO;

    public NotificationController(final UserBO userBO,
                                  final NotificationBO notificationBO) {
        this.userBO = userBO;
        this.notificationBO = notificationBO;
    }

    @GetMapping("/get/{id}")
    public List<NotificationSnapshot> getNotifications(@PathVariable final long id) {
        UserSnapshot userSnapshot = userBO.findById(id);
        if (userSnapshot != null) {
            return determineFromWhoIsMessage(notificationBO.findAllForUser(id));
        }
        return null;
    }

    @GetMapping("/read/{id}")
    public NotificationSnapshot markAsRead(@PathVariable final long id) {
        notificationBO.markAsRead(id);
        return notificationBO.findById(id);
    }

    private List<NotificationSnapshot> determineFromWhoIsMessage(List<NotificationSnapshot> notificationSnapshotList) {
        notificationSnapshotList
                .forEach(notificationSnapshot -> {
                    UserSnapshot user = userBO.findById(notificationSnapshot.getFromUserId());
                    notificationSnapshot.setFromUser(user.getFirstName() + " " + user.getLastName());
                });
        return notificationSnapshotList;
    }


}
