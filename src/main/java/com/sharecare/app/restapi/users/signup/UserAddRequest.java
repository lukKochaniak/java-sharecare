package com.sharecare.app.restapi.users.signup;

import com.sharecare.core.users.Role;
import com.sharecare.core.users.SignupConstants;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@Setter
@Builder
public class UserAddRequest {

    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @NotBlank
    private String email;
    @NotBlank
    @Size(min = SignupConstants.MIN_PASSWORD_LENGTH,
            max = SignupConstants.MAX_PASSWORD_LENGTH)
    private String password;
    @NotNull
    private Date dateOfBirth;
    @NotBlank
    private String phoneNumber;
    @NotBlank
    private String address;
    @NotBlank
    private String city;
    @NotBlank
    private String zipcode;
    private Role role;

}
