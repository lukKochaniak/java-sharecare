package com.sharecare.app.restapi.users.passwordreset;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class ResetPasswordRequest {
    @NotBlank
    private String email;
}
