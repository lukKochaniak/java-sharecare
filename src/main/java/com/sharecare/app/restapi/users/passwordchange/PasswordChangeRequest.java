package com.sharecare.app.restapi.users.passwordchange;

import com.sharecare.core.users.SignupConstants;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

@Getter
@Setter
public class PasswordChangeRequest {

    @NotBlank
    private String oldPassword;

    @NotBlank
    @Size(min = SignupConstants.MIN_PASSWORD_LENGTH)
    private String newPassword;

}
