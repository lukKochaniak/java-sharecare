package com.sharecare.app.restapi.users.profileupdate;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class PasswordUpdateRequest {
    @NotBlank
    private String email;
    @NotBlank
    private String currentPassword;
    @NotBlank
    private String newPassword;
}
