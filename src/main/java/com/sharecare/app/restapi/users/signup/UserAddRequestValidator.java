package com.sharecare.app.restapi.users.signup;

import com.sharecare.app.restapi.AbstractValidator;
import com.sharecare.core.users.UserBO;
import com.sharecare.core.users.UserSnapshot;
import com.sharecare.core.users.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import javax.validation.ClockProvider;

@Component
@Slf4j
public class UserAddRequestValidator extends AbstractValidator {

    private final UserBO userBO;

    public UserAddRequestValidator(final UserBO userBO) {
        this.userBO = userBO;
    }


    @Override
    public void customValidation(Object target, Errors errors) {
        UserAddRequest userAddRequest = (UserAddRequest) target;

        validateEmail(errors, userAddRequest.getEmail());
    }

    private void validateEmail(final Errors errors, final String email) {
        UserSnapshot user = userBO.findByEmail(email);

        if (user != null) {
            errors.rejectValue("email", "userWithSpecifiedEmailAlreadyExists", "Email is already used");
        }

    }


    @Override
    public ClockProvider getClockProvider() {
        return null;
    }
}
