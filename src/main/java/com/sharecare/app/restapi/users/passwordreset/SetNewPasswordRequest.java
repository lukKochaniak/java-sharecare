package com.sharecare.app.restapi.users.passwordreset;

import com.sharecare.core.users.SignupConstants;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class SetNewPasswordRequest {
    @NotBlank
    private String token;
    @Size(min = SignupConstants.MIN_PASSWORD_LENGTH,
            max = SignupConstants.MAX_PASSWORD_LENGTH)
    private String password;
}
