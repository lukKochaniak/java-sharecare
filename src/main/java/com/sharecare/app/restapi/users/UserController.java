package com.sharecare.app.restapi.users;

import com.sharecare.app.restapi.ApiResponse;
import com.sharecare.app.restapi.users.login.LoginPayloadRequest;
import com.sharecare.app.restapi.users.passwordreset.ResetPasswordRequest;
import com.sharecare.app.restapi.users.passwordreset.SetNewPasswordRequest;
import com.sharecare.app.restapi.users.profileupdate.PasswordUpdateRequest;
import com.sharecare.app.restapi.users.profileupdate.ProfileUpdateRequest;
import com.sharecare.app.restapi.users.signup.UserAddRequest;
import com.sharecare.core.tokens.EmailConfirmationTokenBO;
import com.sharecare.core.tokens.EmailConfirmationTokenSnapshot;
import com.sharecare.core.tokens.entity.TokenCreationReason;
import com.sharecare.core.users.UserBO;
import com.sharecare.core.users.UserSnapshot;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/users")
@CrossOrigin(origins = "*", maxAge = 3600)
public class UserController {

    private final UserBO userBO;
    private final EmailConfirmationTokenBO emailConfirmationTokenBO;
    private final Validator userAddRequestValidator;
    private final Validator loginPayloadRequestValidator;
    private final Validator resetPasswordRequestValidator;
    private final Validator setNewPasswordRequestValidator;
    private final Validator profileUpdateRequestValidator;
    private final Validator passwordUpdateRequestValidator;

    public UserController(final UserBO userBO,
                          final EmailConfirmationTokenBO emailConfirmationTokenBO,
                          @Qualifier("userAddRequestValidator") final Validator userAddRequestValidator,
                          @Qualifier("loginPayloadRequestValidator") final Validator loginPayloadRequestValidator,
                          @Qualifier("resetPasswordRequestValidator") final Validator resetPasswordRequestValidator,
                          @Qualifier("setNewPasswordRequestValidator") final Validator setNewPasswordRequestValidator,
                          @Qualifier("profileUpdateRequestValidator") final Validator profileUpdateRequestValidator,
                          @Qualifier("passwordUpdateRequestValidator") final Validator passwordUpdateRequestValidator) {
        this.userBO = userBO;
        this.emailConfirmationTokenBO = emailConfirmationTokenBO;
        this.userAddRequestValidator = userAddRequestValidator;
        this.loginPayloadRequestValidator = loginPayloadRequestValidator;
        this.resetPasswordRequestValidator = resetPasswordRequestValidator;
        this.setNewPasswordRequestValidator = setNewPasswordRequestValidator;
        this.profileUpdateRequestValidator = profileUpdateRequestValidator;
        this.passwordUpdateRequestValidator = passwordUpdateRequestValidator;
    }

    @InitBinder("userAddRequest")
    protected void initUserAddRequestBinder(final WebDataBinder binder) {
        binder.setValidator(userAddRequestValidator);
    }

    @InitBinder("loginPayloadRequest")
    protected void initLoginPayloadRequesttBinder(final WebDataBinder binder) {
        binder.setValidator(loginPayloadRequestValidator);
    }

    @InitBinder("resetPasswordRequest")
    protected void initresetPasswordRequesttBinder(final WebDataBinder binder) {
        binder.setValidator(resetPasswordRequestValidator);
    }

    @InitBinder("setNewPasswordRequest")
    protected void initSetNewPasswordRequestBinder(final WebDataBinder binder) {
        binder.setValidator(setNewPasswordRequestValidator);
    }

    @InitBinder("profileUpdateRequest")
    protected void initProfileUpdateRequestBinder(final WebDataBinder binder) {
        binder.setValidator(profileUpdateRequestValidator);
    }

    @InitBinder("passwordUpdateRequest")
    protected void initPasswordUpdateRequestBinder(final WebDataBinder binder) {
        binder.setValidator(passwordUpdateRequestValidator);
    }

    @PostMapping("/add")
    public HttpEntity<UserSnapshot> addUser(@RequestBody @Valid final UserAddRequest userAddRequest) {
        UserSnapshot user = userBO.addUser(userAddRequest);
        return ResponseEntity.ok(user);
    }

    @PostMapping("/login")
    public ApiResponse<UserSnapshot> login(@RequestBody @Valid LoginPayloadRequest loginPayloadRequest) {
        boolean isMatching = userBO.isMatchingPassword(loginPayloadRequest.getEmail(), loginPayloadRequest.getPassword());

        if (isMatching) {
            UserSnapshot userSnapshot = userBO.findByEmail(loginPayloadRequest.getEmail());
            log.info("Login succesful for user <{}>", userSnapshot.getEmail());
            return new ApiResponse<>(200, "success", userSnapshot);
        }

        log.info("Login unsuccesful for user <{}>", loginPayloadRequest.getEmail());
        return new ApiResponse<>(400, "Login unsuccessful", null);
    }


    @PostMapping("/password/reset")
    public ApiResponse<UserSnapshot> resetPassword(@RequestBody @Valid final ResetPasswordRequest request) {
        if (userBO.isUserVerified(request.getEmail())) {
            UserSnapshot userSnapshot = userBO.findByEmail(request.getEmail());
            userBO.resetPassword(userSnapshot);
            return new ApiResponse<>(200, "success", userSnapshot);
        } else {
            return new ApiResponse<>(400, "User not verified!", null);
        }
    }

    @PostMapping("/password/new/set")
    public ApiResponse<UserSnapshot> setNewPassword(@RequestBody @Valid final SetNewPasswordRequest request) {
        long userId = emailConfirmationTokenBO.getTokenUserId(request.getToken());
        UserSnapshot userSnapshot = userBO.findById(userId);
        userBO.changePassword(userId, request.getPassword());
        return new ApiResponse<>(200, "success", userSnapshot);
    }

    @PostMapping("/profile/update")
    public ApiResponse<UserSnapshot> updateProfile(@RequestBody @Valid final ProfileUpdateRequest request) {
        UserSnapshot user = userBO.updateProfile(request);
        return new ApiResponse<>(200, "success", user);
    }

    @PostMapping("/password/update")
    public ApiResponse<UserSnapshot> updatePassword(@RequestBody @Valid final PasswordUpdateRequest request) {
        UserSnapshot user = userBO.findByEmail(request.getEmail());
        userBO.changePassword(user.getId(), request.getNewPassword());
        return new ApiResponse<>(200, "success", user);
    }

    @PostMapping("/profile/get/data")
    public ApiResponse<UserSnapshot> getProfileDataByEmail(@RequestBody String email) {
        UserSnapshot userSnapshot = userBO.findByEmail(email);
        return new ApiResponse<>(200, "success", userSnapshot);
    }

    @GetMapping("/elderly/get/{id}")
    public ApiResponse<UserSnapshot> getProfileDataById(@PathVariable long id) {
        UserSnapshot userSnapshot = userBO.findById(id);
        return new ApiResponse<>(200, "success", userSnapshot);
    }

    @GetMapping("/email-confirmation")
    public RedirectView confirmEmail(@RequestParam("tokens") String token) {
        Optional<EmailConfirmationTokenSnapshot> emailConfirmationTokenSnapshotOptional =
                emailConfirmationTokenBO.check(TokenCreationReason.REGISTRATION, token);
        RedirectView redirectView = new RedirectView();

        if (emailConfirmationTokenSnapshotOptional.isPresent()) {
            userBO.verifyUser(emailConfirmationTokenSnapshotOptional.get().getReferenceUserId());
            redirectView.setUrl("http://localhost:4200/success/verify");
        } else {
            redirectView.setUrl("http://localhost:4200/error/verify");
        }

        return redirectView;

    }

    @GetMapping("/email-resetting")
    public RedirectView resetPasswordEmail(@RequestParam("tokens") String token) {
        Optional<EmailConfirmationTokenSnapshot> emailConfirmationTokenSnapshotOptional =
                emailConfirmationTokenBO.check(TokenCreationReason.PASSWORD_RESET, token);

        if (emailConfirmationTokenSnapshotOptional.isPresent()) {
            RedirectView redirectView = new RedirectView();
            redirectView.setUrl("http://localhost:4200/set/password" + "?tokens=" + token);
            return redirectView;
        } else {
            return null;
        }
    }
}
