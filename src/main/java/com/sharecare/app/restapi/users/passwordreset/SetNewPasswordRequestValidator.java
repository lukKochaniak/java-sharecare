package com.sharecare.app.restapi.users.passwordreset;

import com.sharecare.app.restapi.AbstractValidator;
import com.sharecare.core.tokens.EmailConfirmationTokenBO;
import com.sharecare.core.tokens.EmailConfirmationTokenSnapshot;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import javax.validation.ClockProvider;
import java.util.Optional;

@Component
public class SetNewPasswordRequestValidator extends AbstractValidator {

    private final EmailConfirmationTokenBO emailConfirmationTokenBO;

    public SetNewPasswordRequestValidator(final EmailConfirmationTokenBO emailConfirmationTokenBO) {
        this.emailConfirmationTokenBO = emailConfirmationTokenBO;
    }

    @Override
    public void customValidation(Object target, Errors errors) {
        SetNewPasswordRequest setNewPasswordRequest = (SetNewPasswordRequest) target;

        validate(errors, setNewPasswordRequest.getToken());
    }

    private void validate(final Errors errors, final String token) {
        Optional<EmailConfirmationTokenSnapshot> optionalSnapshot = emailConfirmationTokenBO.findById(token);

        if(!optionalSnapshot.isPresent()) {
            errors.rejectValue("tokens", "noResettingPasswordRequestWithSuchToken", "There was no such request!");
        }
    }

    @Override
    public ClockProvider getClockProvider() { return null; }

}
