package com.sharecare.app.restapi.users.profileupdate;

import com.sharecare.app.restapi.AbstractValidator;
import com.sharecare.core.users.UserBO;
import com.sharecare.core.users.UserSnapshot;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import javax.validation.ClockProvider;

@Component
public class ProfileUpdateRequestValidator extends AbstractValidator {

    private final UserBO userBO;

    public ProfileUpdateRequestValidator(final UserBO userBO) {
        this.userBO = userBO;
    }

    @Override
    public void customValidation(Object target, Errors errors) {
        validate(errors, ((ProfileUpdateRequest) target).getEmail());
    }

    private void validate(final Errors errors, final String email) {
        UserSnapshot user = userBO.findByEmail(email);

        if(user == null) {
            errors.rejectValue("email", "userWithSpecifiedEmailDoesNotExist", "Email doesn't exist");
        } else {
            checkWhetherUserIsVerified(errors, user);
        }
    }

    private void checkWhetherUserIsVerified(final Errors errors, final UserSnapshot user) {
        boolean isVerified = userBO.isUserVerified(user.getEmail());
        if(!isVerified)
            errors.rejectValue("verify", "userIsNotVerified", "User is not verified!");
    }

    @Override
    public ClockProvider getClockProvider() { return null; }
}
