package com.sharecare.app.restapi.users.profileupdate;

import com.sharecare.app.restapi.AbstractValidator;
import com.sharecare.core.users.UserBO;
import com.sharecare.core.users.UserSnapshot;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import javax.validation.ClockProvider;

@Component
public class PasswordUpdateRequestValidator extends AbstractValidator {

    private final UserBO userBO;

    public PasswordUpdateRequestValidator(final UserBO userBO) { this.userBO = userBO; }

    @Override
    public void customValidation(Object target, Errors errors) {
        PasswordUpdateRequest request = (PasswordUpdateRequest) target;

        validate(errors, request.getEmail(), request.getCurrentPassword());
    }

    private void validate(final Errors errors, final String email, final String currentPassword) {
        UserSnapshot user = userBO.findByEmail(email);

        if (user == null) {
            errors.rejectValue("email", "userWithSpecifiedEmailDoesNotExist", "Email doesn't exist");
        } else {
            checkWhetherPasswordIsCorrect(errors, user, currentPassword);
        }
    }

    private void checkWhetherPasswordIsCorrect(final Errors errors, final UserSnapshot user, final String password){
        boolean matchingPassword = userBO.isMatchingPassword(user.getEmail(), password);
        if (!matchingPassword)
            errors.rejectValue("password", "passwordIsNotCorrect", "Wrong password");

    }

    @Override
    public ClockProvider getClockProvider() { return null; }
}
