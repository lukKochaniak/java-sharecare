package com.sharecare.app.restapi.users.profileupdate;

import com.sharecare.core.users.UserSnapshot;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
public class ProfileUpdateRequest {

    @NotBlank
    private String email;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @NotBlank
    private String address;
    @NotBlank
    private String city;
    @NotBlank
    private String zipcode;
    @NotNull
    private Date dateOfBirth;
}
