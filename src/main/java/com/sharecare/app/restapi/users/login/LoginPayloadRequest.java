package com.sharecare.app.restapi.users.login;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class LoginPayloadRequest {
    @NotBlank
    private String email;
    @NotBlank
    private String password;

}
