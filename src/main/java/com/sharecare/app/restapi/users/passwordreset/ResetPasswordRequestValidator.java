package com.sharecare.app.restapi.users.passwordreset;

import com.sharecare.app.restapi.AbstractValidator;
import com.sharecare.core.users.UserBO;
import com.sharecare.core.users.UserSnapshot;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.sharecare.core.users.entity.User;

import javax.validation.ClockProvider;

@Component
public class ResetPasswordRequestValidator extends AbstractValidator {

    private final UserBO userBO;

    public ResetPasswordRequestValidator(final UserBO userBO) {
        this.userBO = userBO;
    }

    @Override
    public void customValidation(Object target, Errors errors) {
        ResetPasswordRequest resetPasswordRequest = (ResetPasswordRequest) target;

        validate(errors, resetPasswordRequest.getEmail());
    }

    private void validate(final Errors errors, final String email){
        UserSnapshot user = userBO.findByEmail(email);

        if(user == null){
            errors.rejectValue("email", "userWithSpecifiedEmailDoesNotExist", "Email does not exist!");
        } else {
            boolean isVerified = userBO.isUserVerified(user.getEmail());
            if(!isVerified)
                errors.rejectValue("email", "userIsNotVerified", "User is not verified!");
        }
    }

    @Override
    public ClockProvider getClockProvider() { return null; }
}
