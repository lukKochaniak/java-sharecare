package com.sharecare.app.restapi.users.login;

import com.sharecare.app.restapi.AbstractValidator;
import com.sharecare.core.users.UserBO;
import com.sharecare.core.users.UserSnapshot;
import com.sharecare.core.users.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import javax.validation.ClockProvider;

@Component
@Slf4j
public class LoginPayloadRequestValidator extends AbstractValidator {

    private final UserBO userBO;

    public LoginPayloadRequestValidator(final UserBO userBO) {
        this.userBO = userBO;
    }

    @Override
    public void customValidation(Object target, Errors errors) {
        LoginPayloadRequest loginPayloadRequest = (LoginPayloadRequest) target;

        validate(errors, loginPayloadRequest.getEmail(), loginPayloadRequest.getPassword());
    }

    private void validate(final Errors errors, final String email, final String password) {
        UserSnapshot user = userBO.findByEmail(email);

        if (user == null) {
            errors.rejectValue("email", "userWithSpecifiedEmailDoesNotExist", "Email doesn't exist");
        } else {
            checkWhetherPasswordIsCorrect(errors, user, password);

            checkWhetherUserIsVerified(errors, user);
        }

    }

    private void checkWhetherPasswordIsCorrect(final Errors errors, final UserSnapshot user, final String password){
        boolean matchingPassword = userBO.isMatchingPassword(user.getEmail(), password);
        if (!matchingPassword)
            errors.rejectValue("password", "passwordIsNotCorrect", "Wrong password");

    }

    private void checkWhetherUserIsVerified(final Errors errors, final UserSnapshot user) {
        boolean isVerified = userBO.isUserVerified(user.getEmail());
        if(!isVerified)
            errors.rejectValue("email", "userIsNotVerified", "User is not verified!");
    }


    @Override
    public ClockProvider getClockProvider() {
        return null;
    }
}
