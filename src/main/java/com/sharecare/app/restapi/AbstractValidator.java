package com.sharecare.app.restapi;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

public abstract class AbstractValidator
        extends LocalValidatorFactoryBean
        implements Validator {

    public abstract void customValidation(Object target, Errors errors);

    @Override
    public final void validate(final Object target, final Errors errors) {
        super.validate(target, errors);
        if (errors.hasErrors()) {
            return;
        }
        customValidation(target, errors);
    }

    @Override
    public final void validate(final Object target, final Errors errors, final Object... validationHints) {
        super.validate(target, errors, validationHints);
        if (errors.hasErrors()) {
            return;
        }
        customValidation(target, errors);
    }
}
