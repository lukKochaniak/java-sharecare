package com.sharecare.app.restapi.elderlyAssignment;

import com.sharecare.app.restapi.ApiResponse;
import com.sharecare.app.restapi.elderlyAssignment.assignElderlyOffer.AssignElderlyRequest;
import com.sharecare.app.restapi.elderlyAssignment.deleteElderlyAssignmentRequest.DeleteElderlyAssignmentRequest;
import com.sharecare.app.restapi.elderlyAssignment.elderlyAssignmentRequest.ElderlyAssignmentRequest;
import com.sharecare.core.elderlyAssignment.ElderlyAssignmentBO;
import com.sharecare.core.elderlyAssignment.ElderlyAssignmentSnapshot;
import com.sharecare.core.notifications.NotificationBO;
import com.sharecare.core.notifications.NotificationSnapshot;
import com.sharecare.core.notifications.NotificationType;
import com.sharecare.core.users.Role;
import com.sharecare.core.users.UserBO;
import com.sharecare.core.users.UserSnapshot;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/assignelderly")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ElderlyAssignmentController {

    private final ElderlyAssignmentBO elderlyAssignmentBO;
    private final NotificationBO notificationBO;
    private final UserBO userBO;
    private final Validator addElderlyAssignmentRequestValidator;
    private final Validator deleteElderlyAssignmentRequestValidator;
    private final Validator notificationAssignElderlyRequestValidator;

    public ElderlyAssignmentController(final ElderlyAssignmentBO elderlyAssignmentBO,
                                       final NotificationBO notificationBO,
                                       final UserBO userBO,
                                       @Qualifier("elderlyAssignmentRequestValidator") final Validator addElderlyAssignmentRequestValidator,
                                       @Qualifier("deleteElderlyAssignmentRequestValidator") final Validator deleteElderlyAssignmentRequestValidator,
                                       @Qualifier("assignElderlyRequestValidator") final Validator notificationAssignElderlyRequestValidator) {
        this.elderlyAssignmentBO = elderlyAssignmentBO;
        this.notificationBO = notificationBO;
        this.userBO = userBO;
        this.addElderlyAssignmentRequestValidator = addElderlyAssignmentRequestValidator;
        this.deleteElderlyAssignmentRequestValidator = deleteElderlyAssignmentRequestValidator;
        this.notificationAssignElderlyRequestValidator = notificationAssignElderlyRequestValidator;
    }

    @InitBinder("addElderlyAssignmentRequestValidator")
    protected void initAddElderlyAssignmentRequestValidatorBinder(final WebDataBinder binder) {
        binder.setValidator(addElderlyAssignmentRequestValidator);
    }

    @InitBinder("deleteElderlyAssignmentRequestValidator")
    protected void initDeleteElderlyAssignmentRequestValidatorBinder(final WebDataBinder binder) {
        binder.setValidator(deleteElderlyAssignmentRequestValidator);
    }

    @InitBinder("notificationAssignElderlyRequest")
    protected void initNotificationAddRequestBinder(final WebDataBinder binder) {
        binder.setValidator(notificationAssignElderlyRequestValidator);
    }

    @PostMapping("/offer")
    public ApiResponse<NotificationSnapshot> sendAssignElderlyOffer(@RequestBody @Valid final AssignElderlyRequest request) {
        NotificationType type = NotificationType.ACCOUNT_ASSIGNMENT_OFFER;
        UserSnapshot elderlyUser = userBO.findByPhoneNumber(request.getPhoneNumber());
        UserSnapshot caregiverUser = userBO.findByEmail(request.getFromEmail());

        if(elderlyAssignmentBO.findByCaregiverAndElderlyId(caregiverUser.getId(), elderlyUser.getId()) != null){
            return new ApiResponse<>(400, "Your offer to this elderly already exists!", null);
        }

        ElderlyAssignmentSnapshot elderlyAssignmentSnapshot = elderlyAssignmentBO.add(caregiverUser.getId(), elderlyUser.getId());

        NotificationSnapshot notificationSnapshot = notificationBO.addNotification(caregiverUser.getId(),
                elderlyUser.getId(), caregiverUser.getFirstName() + " " + caregiverUser.getLastName(), type);

        if (elderlyAssignmentSnapshot != null && notificationSnapshot != null) {
            return new ApiResponse<>(200, "Notification was sent and elderly is waiting for accepting offer!", notificationSnapshot);
        }
        return new ApiResponse<>(400, "Sending notification unsuccessful", null);
    }

    @PostMapping("/accept")
    public ApiResponse<ElderlyAssignmentSnapshot> acceptAssignElderlyAssignment(@RequestBody @Valid final ElderlyAssignmentRequest request) {
        ElderlyAssignmentSnapshot elderlyAssignmentSnapshot = elderlyAssignmentBO.accept(request.getCaregiverId(), request.getElderlyId());

        if (elderlyAssignmentSnapshot != null) {
            UserSnapshot elderly = userBO.findById(request.getElderlyId());
            notificationBO.addNotification(request.getElderlyId(), request.getCaregiverId(),
                    elderly.getFirstName()+ " " + elderly.getLastName(), NotificationType.ACCOUNT_ASSIGNMENT_ACCEPTED);
            return new ApiResponse<>(200, "Caregiver and elderly were added successfully!", elderlyAssignmentSnapshot);
        }

        return new ApiResponse<>(400, "Caregiver and elderly adding was unsuccessful!", null);
    }

    @PostMapping("/decline")
    public ApiResponse<ElderlyAssignmentSnapshot> declineAssignElderlyAssignment(@RequestBody @Valid final ElderlyAssignmentRequest request) {
        ElderlyAssignmentSnapshot elderlyAssignmentSnapshot = elderlyAssignmentBO.decline(request.getCaregiverId(), request.getElderlyId());

        if (elderlyAssignmentSnapshot != null) {
            UserSnapshot elderly = userBO.findById(request.getElderlyId());
            notificationBO.addNotification(request.getElderlyId(), request.getCaregiverId(),
                    elderly.getFirstName() + " " + elderly.getLastName(), NotificationType.ACCOUNT_ASSIGNMENT_DECLINED);
            return new ApiResponse<>(200, "Offer was successfully declined!", elderlyAssignmentSnapshot);
        }

        return new ApiResponse<>(400, "Caregiver and elderly assignment was not declined!", null);
    }

    @GetMapping("/get/{id}")
    public ApiResponse<List<UserSnapshot>> getAllElderlyAssignments(@PathVariable final long id) {
        UserSnapshot user = userBO.findById(id);
        List<UserSnapshot> usersList = new ArrayList<>();

        if (user != null && user.getRole() == Role.CAREGIVER) {
            elderlyAssignmentBO.getAllElderliesAssignedToCaregiver(id).forEach(elderlyAssignmentSnapshot ->
                            usersList.add(userBO.findById(elderlyAssignmentSnapshot.getAssignedElderlyId())));
            return new ApiResponse<>(200, "List retrieved successfully!", usersList);
        } else if (user != null && user.getRole() == Role.ELDERLY) {
            elderlyAssignmentBO.getAllCaregiversAssignedToElderly(id).forEach(elderlyAssignmentSnapshot ->
                    usersList.add(userBO.findById(elderlyAssignmentSnapshot.getAssignedCaregiverId())));
            return new ApiResponse<>(200, "List retrieved successfully!", usersList);
        } else {
            return new ApiResponse<>(400, "Couldn't retrieve assigned users list!", null);
        }

    }

    @PostMapping("/delete")
    public ApiResponse<Void> deleteAssignElderlyAssignment(@RequestBody @Valid final DeleteElderlyAssignmentRequest request) {
        elderlyAssignmentBO.delete(request.getCaregiverId(), request.getElderlyId());

        UserSnapshot elderly = userBO.findById(request.getElderlyId());
        notificationBO.addNotification(request.getElderlyId(), request.getCaregiverId(),
                elderly.getFirstName() + " " + elderly.getLastName(), NotificationType.ACCOUNT_ASSIGNMENT_DELETED);

        return new ApiResponse<>(200, "Caregiver and elderly assignment was deleted successfully!", null);
    }
}
