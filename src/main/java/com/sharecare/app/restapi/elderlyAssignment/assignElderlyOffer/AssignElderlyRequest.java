package com.sharecare.app.restapi.elderlyAssignment.assignElderlyOffer;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class AssignElderlyRequest {
    @NotBlank
    private String phoneNumber;
    @NotBlank
    private String fromEmail;
}
