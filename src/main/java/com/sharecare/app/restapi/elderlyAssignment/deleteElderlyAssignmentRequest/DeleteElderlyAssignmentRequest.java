package com.sharecare.app.restapi.elderlyAssignment.deleteElderlyAssignmentRequest;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class DeleteElderlyAssignmentRequest {
    private long elderlyId;
    private long caregiverId;
}
