package com.sharecare.app.restapi.elderlyAssignment.elderlyAssignmentRequest;

import com.sharecare.app.restapi.AbstractValidator;
import com.sharecare.core.elderlyAssignment.ElderlyAssignmentBO;
import com.sharecare.core.users.UserBO;
import com.sharecare.core.users.UserSnapshot;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import javax.validation.ClockProvider;

@Component
public class ElderlyAssignmentRequestValidator extends AbstractValidator {

    private final ElderlyAssignmentBO elderlyAssignmentBO;
    private final UserBO userBO;

    public ElderlyAssignmentRequestValidator(final ElderlyAssignmentBO elderlyAssignmentBO,
                                             final UserBO userBO) {
        this.elderlyAssignmentBO = elderlyAssignmentBO;
        this.userBO = userBO;
    }

    @Override
    public void customValidation(Object target, Errors errors) {
        ElderlyAssignmentRequest request = (ElderlyAssignmentRequest) target;
        validate(request.getCaregiverId(), request.getElderlyId(), errors);
    }

    private void validate(long caregiverId, long elderlyId, Errors errors) {
        UserSnapshot caregiverUser = userBO.findById(caregiverId);
        UserSnapshot elderlyUser = userBO.findById(elderlyId);

        if (caregiverUser == null) {
            errors.rejectValue("caregiverId", "userWithSpecifiedIdDoesNotExist", "Caregiver doesn't exist!");
        } else {
            if (!caregiverUser.getRole().toString().equalsIgnoreCase("caregiver"))
                errors.rejectValue("caregiverId", "userWithSpecifiedIdIsNotCaregiver", "This user is not a caregiver!");
        }

        if (elderlyUser == null) {
            errors.rejectValue("elderlyId", "userWithSpecifiedIdDoesNotExist", "Elderly doesn't exist!");
        } else {
            if (!elderlyUser.getRole().toString().equalsIgnoreCase("caregiver"))
                errors.rejectValue("elderlyId", "userWithSpecifiedIdIsNotCaregiver", "This user is not an elderly!");
        }
    }

    @Override
    public ClockProvider getClockProvider() {
        return null;
    }
}
