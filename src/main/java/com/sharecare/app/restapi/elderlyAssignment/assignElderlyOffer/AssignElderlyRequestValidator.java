package com.sharecare.app.restapi.elderlyAssignment.assignElderlyOffer;

import com.sharecare.app.restapi.AbstractValidator;
import com.sharecare.core.elderlyAssignment.ElderlyAssignmentBO;
import com.sharecare.core.notifications.NotificationBO;
import com.sharecare.core.users.UserBO;
import com.sharecare.core.users.UserSnapshot;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import javax.validation.ClockProvider;

@Component
public class AssignElderlyRequestValidator extends AbstractValidator {

    private final UserBO userBO;

    public AssignElderlyRequestValidator(final UserBO userBO) {
        this.userBO = userBO;
    }

    @Override
    public void customValidation(Object target, Errors errors) {
        AssignElderlyRequest request = (AssignElderlyRequest) target;
        validate(request.getPhoneNumber(), request.getFromEmail(), errors);
    }

    private void validate(String phoneNumber, String from, Errors errors) {

        UserSnapshot caregiverUser = userBO.findByEmail(from);

        if(caregiverUser == null) {
            errors.rejectValue("email", "userWithSpecifiedEmailDoesNotExist", "Email doesn't exist");
        } else {
            UserSnapshot elderlyUser = userBO.findByPhoneNumber(phoneNumber);

            if(elderlyUser == null) {
                errors.rejectValue("phoneNumber", "userWithSpecifiedPhoneNumberDoesNotExist", "Phone number doesn't exist");
            } else {
                checkWhetherUserIsVerified(errors, elderlyUser);
            }
        }
    }

    private void checkWhetherUserIsVerified(final Errors errors, final UserSnapshot user) {
        boolean isVerified = userBO.isUserVerified(user.getEmail());
        if(!isVerified)
            errors.rejectValue("email", "userIsNotVerified", "User is not verified!");
    }

    @Override
    public ClockProvider getClockProvider() {
        return null;
    }
}
