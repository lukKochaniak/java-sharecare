package com.sharecare.app.restapi.services.finishService;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class FinishServiceRequest {
    private long elderlyId;
    private long caregiverId;
    private long serviceId;
}
