package com.sharecare.app.restapi.services.requestservice;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class RequestDeclineRequest {
    private long userId;
    private long serviceId;
    private String from;
}
