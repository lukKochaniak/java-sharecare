package com.sharecare.app.restapi.services.requestservice;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class RequestAcceptRequest {
    private long userId;
    private long serviceId;
    private long from;
}
