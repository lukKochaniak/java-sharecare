package com.sharecare.app.restapi.services.addService;

import com.sharecare.core.services.Status;
import com.sharecare.core.services.Title;
import lombok.Builder;
import lombok.Getter;

import java.time.Duration;

@Getter
@Builder
public class ServiceAddRequest {

    private String title;
    private String description;
    private long cost;
    private long duration;
    private long requestedBy;
    private String address;
    private String city;
    private String zipcode;
}
