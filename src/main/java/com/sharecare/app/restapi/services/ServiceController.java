package com.sharecare.app.restapi.services;

import com.sharecare.app.restapi.ApiResponse;
import com.sharecare.app.restapi.services.addService.ServiceAddRequest;
import com.sharecare.app.restapi.services.finishService.FinishServiceRequest;
import com.sharecare.app.restapi.services.requestservice.RequestAcceptRequest;
import com.sharecare.app.restapi.services.requestservice.RequestDeclineRequest;
import com.sharecare.app.restapi.services.requestservice.RequestServiceRequest;
import com.sharecare.core.blockchain.BlockchainWalletBO;
import com.sharecare.core.notifications.NotificationBO;
import com.sharecare.core.notifications.NotificationType;
import com.sharecare.core.services.ServiceBO;
import com.sharecare.core.services.ServiceSnapshot;
import com.sharecare.core.services.Status;
import com.sharecare.core.users.UserBO;
import com.sharecare.core.users.UserSnapshot;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/services")
@CrossOrigin(origins = "*", maxAge = 3600)
@Slf4j
public class ServiceController {

    private ServiceBO serviceBO;
    private NotificationBO notificationBO;
    private BlockchainWalletBO blockchainWalletBO;
    private UserBO userBO;

    public ServiceController(final ServiceBO serviceBO,
                             final NotificationBO notificationBO,
                             final UserBO userBO,
                             final BlockchainWalletBO blockchainWalletBO) {
        this.serviceBO = serviceBO;
        this.notificationBO = notificationBO;
        this.userBO = userBO;
        this.blockchainWalletBO = blockchainWalletBO;
    }

    @GetMapping("/get")
    public List<ServiceSnapshot> getAllServices() {
        return serviceBO.getAllServices();
    }

    @GetMapping("/{id}")
    public ServiceSnapshot getService(@PathVariable long id) {
        return serviceBO.getService(id);
    }

    @PostMapping("/add")
    public HttpEntity<ServiceSnapshot> addService(@RequestBody ServiceAddRequest serviceAddRequest) {
        ServiceSnapshot service = serviceBO.addService(serviceAddRequest);
        return ResponseEntity.ok(service);
    }

    @PutMapping("/update")
    public HttpEntity<ServiceSnapshot> updateService(@RequestBody ServiceSnapshot serviceSnapshot) {
        ServiceSnapshot service = serviceBO.updateService(serviceSnapshot);
        return ResponseEntity.ok(service);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<Void> deleteService(@PathVariable long id) {
        ServiceSnapshot serviceSnapshot = serviceBO.findById(id);
        UserSnapshot user = userBO.findById(serviceSnapshot.getRequestedBy());

        if (serviceSnapshot.getStatus() != Status.OPEN) {
            notificationBO.addNotification(serviceSnapshot.getRequestedBy(), serviceSnapshot.getProvidedBy(),
                    user.getFirstName() + " " + user.getLastName(), NotificationType.SERVICE_DELETED);
        }

        serviceBO.deleteService(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/servicesOf/{id}")
    public List<ServiceSnapshot> getServicesOfUser(@PathVariable long id) {
        return serviceBO.findByRequestedBy(id);
    }

    @GetMapping("/completedBy/{id}")
    public List<ServiceSnapshot> getByCompletedById(@PathVariable long id) {
        return serviceBO.findCompletedBy(id);
    }

    @GetMapping("/get/pending/services/{id}")
    public List<ServiceSnapshot> getPendingServices(@PathVariable long id) {
        return serviceBO.getPendingServices(id);
    }

    @GetMapping("/tostart/{id}")
    public List<ServiceSnapshot> getServicesToStartForElderly(@PathVariable long id) {
        return serviceBO.getServicesToStartForElderly(id);
    }

    @GetMapping("/get/open/services/{id}")
    public List<ServiceSnapshot> getOpenServices(@PathVariable long id) {
        return serviceBO.getOpenServices(id);

    }

    @GetMapping("/requestedBy/{id}")
    public List<ServiceSnapshot> getByRequestedById(@PathVariable long id) {
        return serviceBO.findCompletedBy(id);
    }

    @GetMapping("/cancel/{id}")
    public ApiResponse<ServiceSnapshot> cancelTask(@PathVariable long id) {
        ServiceSnapshot serviceSnapshot = serviceBO.getService(id);
        ServiceSnapshot canceledServiceSnapshot = serviceBO.cancelService(id);
        UserSnapshot userSnapshot = userBO.findById(serviceSnapshot.getProvidedBy());

        if (canceledServiceSnapshot != null) {
            notificationBO.addServiceNotification(serviceSnapshot.getProvidedBy(), serviceSnapshot.getRequestedBy(),
                    userSnapshot.getFirstName() + " " + userSnapshot.getLastName(), NotificationType.SERVICE_CANCELED,
                    "Task title: " + canceledServiceSnapshot.getTitle() + ", task description: " + canceledServiceSnapshot.getDescription() + ", id=" + canceledServiceSnapshot.getId());
            return new ApiResponse<>(200, "Task cancelled successfully!", serviceSnapshot);
        } else {
            return new ApiResponse<>(400, "We couldn't cancel your task!", null);
        }
    }

    @PostMapping("/request")
    public ApiResponse<ServiceSnapshot> requestService(@RequestBody RequestServiceRequest request) {

        ServiceSnapshot service = serviceBO.getService(request.getServiceId());
        UserSnapshot caregiver = userBO.findById(request.getUserId());

        if (service != null && caregiver != null) {

            UserSnapshot elderly = userBO.findById(service.getRequestedBy());

            service.setStatus(Status.REQUESTED);
            serviceBO.updateService(service);

            notificationBO.addServiceNotification(caregiver.getId(), elderly.getId(),
                    caregiver.getFirstName() + ' ' + caregiver.getLastName(), NotificationType.SERVICE_REQUESTED,
                    "Task title: " + service.getTitle() + ", task description: " + service.getDescription() + ", id=" + service.getId());

            return new ApiResponse<>(200, "Successfully requested service", service);
        } else {
            return new ApiResponse<>(400, "Something went wrong, no such service or user, please try again", null);
        }
    }

    @PostMapping("/accept/request")
    public ApiResponse<ServiceSnapshot> acceptRequest(@RequestBody RequestAcceptRequest request) {
        ServiceSnapshot service = serviceBO.getService(request.getServiceId());
        UserSnapshot caregiver = userBO.findById(request.getFrom());
        UserSnapshot elderly = userBO.findById(request.getUserId());

        service.setProvidedBy(caregiver.getId());
        service.setStatus(Status.ACCEPTED);
        serviceBO.updateService(service);

        notificationBO.addNotification(elderly.getId(), caregiver.getId(),
                elderly.getFirstName() + " " + elderly.getLastName(), NotificationType.SERVICE_REQUEST_ACCEPTED);

        return new ApiResponse<>(200, "Successfully accepted request", service);
    }

    @PostMapping("/decline/request")
    public ApiResponse<ServiceSnapshot> declineRequest(@RequestBody RequestDeclineRequest request) {
        ServiceSnapshot service = serviceBO.getService(request.getServiceId());
        UserSnapshot caregiver = userBO.findByEmail(request.getFrom());
        UserSnapshot elderly = userBO.findById(request.getUserId());

        service.setProvidedBy(-1); //TODO -1 or 0????
        service.setStatus(Status.OPEN);
        serviceBO.updateService(service);

        notificationBO.addNotification(caregiver.getId(), elderly.getId(),
                caregiver.getFirstName() + caregiver.getLastName(), NotificationType.SERVICE_REQUEST_ACCEPTED);

        return new ApiResponse<>(200, "Successfully declined request", service);
    }

    @PostMapping("/finish")
    public ApiResponse<ServiceSnapshot> finishService(@RequestBody FinishServiceRequest request) throws Exception {
        ServiceSnapshot service = serviceBO.getService(request.getServiceId());
        UserSnapshot caregiver = userBO.findById(service.getProvidedBy());
        UserSnapshot elderly = userBO.findById(service.getRequestedBy());

        if (service != null && caregiver != null && elderly != null) {
            serviceBO.finishService(service);

            boolean whetherTransactionIsSuccessful = blockchainWalletBO.performTransaction(request.getCaregiverId(), service.getCost(), request.getElderlyId());

            if (whetherTransactionIsSuccessful) {
                notificationBO.addNotification(caregiver.getId(), elderly.getId(),
                        caregiver.getFirstName() + caregiver.getLastName(), NotificationType.SERVICE_REQUEST_ACCEPTED);

                return new ApiResponse<>(200, "Task completed!", service);

            }
        }

        return new ApiResponse<>(400, "Something went wrong!", null);

    }

}
