package com.sharecare.app.restapi.services.requestservice;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Builder
public class RequestServiceRequest {
    private long serviceId;
    private long userId;
}
