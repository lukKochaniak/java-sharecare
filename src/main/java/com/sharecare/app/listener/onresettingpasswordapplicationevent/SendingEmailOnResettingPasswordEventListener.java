package com.sharecare.app.listener.onresettingpasswordapplicationevent;

import com.sharecare.app.email.user.passwordreset.PasswordResetEmailSendingService;
import com.sharecare.core.users.event.ResetPasswordEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SendingEmailOnResettingPasswordEventListener  implements ApplicationListener<ResetPasswordEvent> {

    private final PasswordResetEmailSendingService passwordResetEmailSendingService;

    public SendingEmailOnResettingPasswordEventListener(final PasswordResetEmailSendingService passwordResetEmailSendingService) {
        this.passwordResetEmailSendingService = passwordResetEmailSendingService;
    }

    @Async
    @Override
    public void onApplicationEvent(ResetPasswordEvent event) {
        log.info("Executing SendingEmailOnResettingPasswordEventListener for user email <{}>",
                event.getUserSnapshot().getEmail());
        passwordResetEmailSendingService.sendEmail(event.getUserSnapshot());
    }
}
