package com.sharecare.app.listener.onsignupevent;

import com.sharecare.app.email.user.activation.EmailConfirmationEmailSendingService;
import com.sharecare.core.users.event.SignupEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class EmailConfirmationSendingEmailOnSignupEventListener
        implements ApplicationListener<SignupEvent> {

    private final EmailConfirmationEmailSendingService emailConfirmationEmailSendingService;

    EmailConfirmationSendingEmailOnSignupEventListener(
            final EmailConfirmationEmailSendingService emailConfirmationEmailSendingService) {
        this.emailConfirmationEmailSendingService = emailConfirmationEmailSendingService;
    }

    @Async
    @Override
    public void onApplicationEvent(final SignupEvent event) {
        log.info("Executing EmailConfirmationSendingEmailOnSignupEventListener for user email <{}>",
                event.getUserSnapshot().getEmail());

        emailConfirmationEmailSendingService.sendEmail(event.getUserSnapshot());
    }
}
