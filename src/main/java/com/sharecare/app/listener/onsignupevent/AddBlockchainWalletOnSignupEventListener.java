package com.sharecare.app.listener.onsignupevent;

import com.sharecare.core.blockchain.BlockchainWalletBO;
import com.sharecare.core.users.event.SignupEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Component
public class AddBlockchainWalletOnSignupEventListener implements ApplicationListener<SignupEvent> {

    private final BlockchainWalletBO blockchainWalletBO;

    public AddBlockchainWalletOnSignupEventListener(final BlockchainWalletBO blockchainWalletBO) {
        this.blockchainWalletBO = blockchainWalletBO;
    }

    @Override
    public void onApplicationEvent(SignupEvent signupEvent) {
        log.info("Executing AddBlockchainWalletOnSignupEventListener for user email <{}>",
                signupEvent.getUserSnapshot().getEmail());

        try {
            blockchainWalletBO.addWallet(signupEvent.getUserSnapshot());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
