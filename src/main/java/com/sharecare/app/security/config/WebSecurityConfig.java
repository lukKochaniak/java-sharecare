package com.sharecare.app.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig
        extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;
    private final Environment env;
    private final UnauthorizedAuthenticationEntryPoint unauthorizedHandler;

    @Autowired
    public WebSecurityConfig(final UserDetailsService userDetailsService,
                             final Environment env,
                             final UnauthorizedAuthenticationEntryPoint unauthorizedHandler) {
        this.userDetailsService = userDetailsService;
        this.env = env;
        this.unauthorizedHandler = unauthorizedHandler;
    }


    @Override
    protected void configure(final HttpSecurity http)
            throws Exception {
        Set<String> profiles = new HashSet<>(Arrays.asList(env.getActiveProfiles()));

        boolean tests = profiles.contains("test");

        if (tests) {
            http.httpBasic().authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED));
        }

        http.authorizeRequests()
                .antMatchers("/api/users/**").permitAll()
                .antMatchers("/api/transactions/**").permitAll()
                .antMatchers("/api/services/**").permitAll()
                .antMatchers("/websocket/notification/**").permitAll()
                .antMatchers("/api/services/**").anonymous()
                .antMatchers("/socket/**").permitAll()
                .antMatchers("/api/assignelderly/**").permitAll()
                .antMatchers("/api/notifications/**").permitAll()
                .antMatchers("/api/users/password/reset/*").anonymous();

        if (tests) {
            http.authorizeRequests()
                    .antMatchers("/api/**").authenticated()
                    .anyRequest().permitAll();
        } else {
            http.authorizeRequests()
                    .anyRequest().authenticated();
            http
                    .exceptionHandling().authenticationEntryPoint(unauthorizedHandler);
        }

        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.csrf()
                .disable();

        http.cors();

    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth)
            throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationTrustResolver authenticationTrustResolver() {
        return new AuthenticationTrustResolverImpl();
    }

}
