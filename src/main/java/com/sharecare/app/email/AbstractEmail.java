package com.sharecare.app.email;

import org.springframework.core.io.Resource;

import java.util.*;

public class AbstractEmail
        implements Email {

    private final String subject;
    private final String template;
    private final Set<String> to;
    private final Set<String> cc;
    private final Set<String> bcc;
    private final Map<String, Object> variables;
    private final Locale locale;
    private final Map<String, Resource> resources;

    protected AbstractEmail(final String subject,
                            final String template,
                            final Locale locale) {
        this.subject = subject;
        this.template = template;
        this.locale = locale;
        this.to = new HashSet<>();
        this.cc = new HashSet<>();
        this.bcc = new HashSet<>();
        this.variables = new HashMap<>();
        this.resources = new HashMap<>();
    }

    @Override
    public final String getSubject() {
        return subject;
    }

    @Override
    public final String getTemplate() {
        return template;
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public final String[] getTo() {
        return to.toArray(new String[to.size()]);
    }

    @Override
    public Optional<String> getFrom() {
        return Optional.empty();
    }

    @Override
    public final String[] getCc() {
        return cc.toArray(new String[cc.size()]);
    }

    @Override
    public final String[] getBcc() {
        return bcc.toArray(new String[bcc.size()]);
    }

    @Override
    public final Map<String, Object> getVariables() {
        return Collections.unmodifiableMap(variables);
    }

    @Override
    public Map<String, Resource> getResources() {
        return Collections.unmodifiableMap(resources);
    }

    public AbstractEmail addTo(final String to) {
        this.to.add(to);
        return this;
    }

    public AbstractEmail addTo(final Collection<String> to) {
        this.to.addAll(to);
        return this;
    }

    public AbstractEmail addCc(final String cc) {
        this.cc.add(cc);
        return this;
    }

    public AbstractEmail addBcc(final String bcc) {
        this.bcc.add(bcc);
        return this;
    }

    public AbstractEmail addVariable(final String name, final Object value) {
        this.variables.put(name, value);
        return this;
    }

    public AbstractEmail addResource(final String name, final Resource resource) {
        this.resources.put(name, resource);
        return this;
    }
}
