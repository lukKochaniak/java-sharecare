package com.sharecare.app.email;

import org.springframework.core.io.Resource;

import java.util.Locale;
import java.util.Map;
import java.util.Optional;

public interface Email {

    String getSubject();

    String[] getTo();

    Locale getLocale();

    Optional<String> getFrom();

    String[] getCc();

    String[] getBcc();

    String getTemplate();

    Map<String, Object> getVariables();

    Map<String, Resource> getResources();
}
