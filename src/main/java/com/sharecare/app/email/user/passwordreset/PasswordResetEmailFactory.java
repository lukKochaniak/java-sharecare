package com.sharecare.app.email.user.passwordreset;

import com.sharecare.core.tokens.EmailConfirmationTokenSnapshot;
import com.sharecare.core.users.UserSnapshot;
import org.springframework.stereotype.Component;

import static com.sharecare.app.email.EmailConstants.APPLICATION_URL;

@Component
public class PasswordResetEmailFactory {

    public PasswordResetEmail create(final UserSnapshot userSnapshot,
                                     final EmailConfirmationTokenSnapshot emailConfirmationTokenSnapshot) {
        return new PasswordResetEmail(userSnapshot, userSnapshot.getLocale(), APPLICATION_URL + "users/email-resetting?tokens=" + emailConfirmationTokenSnapshot.getToken());
    }
}
