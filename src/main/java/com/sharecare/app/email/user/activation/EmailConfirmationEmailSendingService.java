package com.sharecare.app.email.user.activation;

import com.sharecare.app.email.EmailService;
import com.sharecare.core.users.UserSnapshot;
import com.sharecare.core.tokens.EmailConfirmationTokenBO;
import com.sharecare.core.tokens.EmailConfirmationTokenSnapshot;
import com.sharecare.core.tokens.entity.TokenCreationReason;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import static com.sharecare.app.email.EmailConstants.APPLICATION_URL;

@Service
public class EmailConfirmationEmailSendingService {

    private final EmailConfirmationTokenBO emailConfirmationTokenBO;

    private final EmailConfirmationEmailFactory emailConfirmationEmailFactory;

    private final EmailService emailService;

    public EmailConfirmationEmailSendingService(final EmailConfirmationTokenBO emailConfirmationTokenBO,
                                                final EmailConfirmationEmailFactory emailConfirmationEmailFactory,
                                                final EmailService emailService) {
        this.emailConfirmationTokenBO = emailConfirmationTokenBO;
        this.emailConfirmationEmailFactory = emailConfirmationEmailFactory;
        this.emailService = emailService;
    }

    public void sendEmail(final UserSnapshot userSnapshot) {
        EmailConfirmationTokenSnapshot emailConfirmationTokenSnapshot = emailConfirmationTokenBO.add(TokenCreationReason.REGISTRATION,
                userSnapshot.getId());

        EmailConfirmationEmail emailConfirmationEmail = emailConfirmationEmailFactory.create(userSnapshot, emailConfirmationTokenSnapshot);

        emailService.sendEmail(emailConfirmationEmail);
    }
}
