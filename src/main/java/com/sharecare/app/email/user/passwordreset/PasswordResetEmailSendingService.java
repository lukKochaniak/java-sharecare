package com.sharecare.app.email.user.passwordreset;

import com.sharecare.app.email.EmailService;
import com.sharecare.core.users.UserSnapshot;
import com.sharecare.core.tokens.EmailConfirmationTokenBO;
import com.sharecare.core.tokens.EmailConfirmationTokenSnapshot;
import com.sharecare.core.tokens.entity.TokenCreationReason;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import static com.sharecare.app.email.EmailConstants.APPLICATION_URL;

@Component
public class PasswordResetEmailSendingService {
    private final EmailConfirmationTokenBO emailConfirmationTokenBO;
    private final PasswordResetEmailFactory passwordResetEmailFactory;
    private final EmailService emailService;

    public PasswordResetEmailSendingService(final EmailConfirmationTokenBO emailConfirmationTokenBO,
                                            final PasswordResetEmailFactory passwordResetEmailFactory,
                                            final EmailService emailService) {
        this.emailConfirmationTokenBO = emailConfirmationTokenBO;
        this.passwordResetEmailFactory = passwordResetEmailFactory;
        this.emailService = emailService;
    }

    public void sendEmail(final UserSnapshot userSnapshot) {
        EmailConfirmationTokenSnapshot emailConfirmationTokenSnapshot = emailConfirmationTokenBO.add(TokenCreationReason.PASSWORD_RESET,
                userSnapshot.getId());
        PasswordResetEmail email = passwordResetEmailFactory.create(userSnapshot, emailConfirmationTokenSnapshot);

        emailService.sendEmail(email);
    }
}
