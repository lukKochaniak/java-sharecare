package com.sharecare.app.email.user.activation;

import com.sharecare.app.email.AbstractEmail;

import java.util.Locale;

public class EmailConfirmationEmail
        extends AbstractEmail {

    private static final String SUBJECT = "email.email-confirmation-email.subject";
    private static final String TEMPLATE = "email-confirmation-email";
    private static final String FIRST_NAME_KEY = "firstName";
    private static final String LAST_NAME_KEY = "lastName";
    private static final String TOKEN_HYPERLINK_KEY = "token";

    public EmailConfirmationEmail(final Locale locale,
                                  final String email,
                                  final String firstName,
                                  final String lastName,
                                  final String token) {
        super(SUBJECT, TEMPLATE, locale);
        super.addTo(email);
        super.addVariable(FIRST_NAME_KEY, firstName);
        super.addVariable(LAST_NAME_KEY, lastName);
        super.addVariable(TOKEN_HYPERLINK_KEY, token);
    }
}
