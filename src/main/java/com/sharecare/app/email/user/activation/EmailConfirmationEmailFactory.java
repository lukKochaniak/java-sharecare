package com.sharecare.app.email.user.activation;

import com.sharecare.core.users.UserSnapshot;
import com.sharecare.core.tokens.EmailConfirmationTokenSnapshot;
import org.springframework.stereotype.Component;

import static com.sharecare.app.email.EmailConstants.APPLICATION_URL;

@Component
public class EmailConfirmationEmailFactory {

    public EmailConfirmationEmail create(final UserSnapshot userSnapshot,
                                         final EmailConfirmationTokenSnapshot emailConfirmationTokenSnapshot) {
        return new EmailConfirmationEmail(userSnapshot.getLocale(),
                userSnapshot.getEmail(),
                userSnapshot.getFirstName(),
                userSnapshot.getLastName(),
                APPLICATION_URL + "users/email-confirmation?tokens=" + emailConfirmationTokenSnapshot.getToken());
    }
}
