package com.sharecare.app.email.user.passwordreset;

import com.sharecare.app.email.AbstractEmail;
import com.sharecare.core.users.UserSnapshot;

import java.util.Locale;

public class PasswordResetEmail extends AbstractEmail {
    private static final String TEMPLATE = "password-reset-email";
    private static final String SUBJECT = "email.password-reset-email.subject";
    private static final String FIRST_NAME_KEY = "firstName";
    private static final String LAST_NAME_KEY = "lastName";
    private static final String TOKEN_HYPERLINK_KEY = "token";


    public PasswordResetEmail(final UserSnapshot userSnapshot, final Locale locale, final String token) {
        super(SUBJECT, TEMPLATE, locale);
        super.addTo(userSnapshot.getEmail());
        super.addVariable(FIRST_NAME_KEY, userSnapshot.getFirstName());
        super.addVariable(LAST_NAME_KEY, userSnapshot.getLastName());
        super.addVariable(TOKEN_HYPERLINK_KEY, token);
    }
}
