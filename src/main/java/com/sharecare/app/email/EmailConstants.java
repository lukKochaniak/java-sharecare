package com.sharecare.app.email;

import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public final class EmailConstants {

    public static final Set<Locale> SUPPORTED_LANGUAGES;

    public static final String APPLICATION_URL = "http://localhost:8080/api/";

    public static final String EMAIL_TO = "emailTo";

    static {
        Set<Locale> supportedLocales = new HashSet<>();
        supportedLocales.add(Locale.ENGLISH);
        SUPPORTED_LANGUAGES = Collections.unmodifiableSet(supportedLocales);
    }

    private EmailConstants() {
    }
}
