package com.sharecare.app.email;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.Future;

@Service
@Slf4j
public class EmailServiceImpl
        implements EmailService {

    private final JavaMailSender mailSender;

    private final TemplateEngine templateEngine;

    private final MessageSource messageSource;

    @Value("classpath:mail/images/logo.png")
    private Resource logo;

    public EmailServiceImpl(final JavaMailSender mailSender,
                            final TemplateEngine templateEngine,
                            final MessageSource messageSource) {
        this.mailSender = mailSender;
        this.templateEngine = templateEngine;
        this.messageSource = messageSource;
    }

    @Async
    @Override
    public Future<Boolean> sendEmail(final Email email) {
        log.debug("Sending email <{}> to <{}>",
                email.getSubject(),
                email.getTo());

        boolean sent = true;

        try {
            deliver(email);
        } catch (MessagingException me) {
            log.error("Unable to send <{}> because of MessagingException",
                    email.getTemplate(), me);
            sent = false;
        } catch (RuntimeException re) {
            log.error("Unable to send <{}> because of RuntimeException",
                    email.getTemplate(), re);
            sent = false;
        }

        return new AsyncResult<>(sent);
    }

    private void deliver(final Email email)
            throws MessagingException {
        final Context context = new Context(email.getLocale());

        String value = Arrays.stream(email.getTo()).findFirst().get();
        context.setVariable(EmailConstants.EMAIL_TO, value);
        context.setVariables(email.getVariables());
        context.setVariable("location", "Netherlands");
        context.setVariable("signature", "http://sharehealthcare.com");

        registerResourceVariables(context, email);

        final MimeMessage mimeMessage = mailSender.createMimeMessage();
        final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
        message.setSubject(Objects.requireNonNull(this.messageSource.getMessage(email.getSubject(), null, email.getSubject(), email.getLocale())));
        message.addInline(logo.getFilename(), logo, MediaType.IMAGE_PNG_VALUE);

        String sender = email.getFrom().orElse("no-reply@sharehealthcare.com");
        message.setFrom(sender);
        message.setTo(email.getTo());

        if (email.getCc().length > 0) {
            message.setCc(email.getCc());
        }
        if (email.getBcc().length > 0) {
            message.setBcc(email.getBcc());
        }

        final String htmlContent = templateEngine.process(email.getTemplate(), context);
        message.setText(htmlContent, true);

        this.mailSender.send(mimeMessage);

        log.info("Sent email <{}> to <{}>",
                email.getSubject(),
                email.getTo());
    }

    private void registerResourceVariables(final Context context, final Email email) {
        email.getResources()
                .forEach((name, resource) -> context.setVariable(name, resource.getFilename()));
    }

}
