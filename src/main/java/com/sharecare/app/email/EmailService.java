package com.sharecare.app.email;

import java.util.concurrent.Future;

public interface EmailService {

    Future<Boolean> sendEmail(Email email);
}
